module gentypesrun

import Control.Monad.State
import Data.Either
import Data.Func
import Data.GenEq
import Gast
import StdDebug
import StdEnv
import StdOverloadedList
import System._Pointer
import Text
import Text.GenPrint

import Data.UInt
import Data.UInt.Gast
import Data.GenType.CSerialise
import mTask.Interpret.String255

import code from "cp.o"
import code from "cgentypesrun.o"

derive class Gast String255
derive bimap [!]

ppString255 :: !Int !String !*World -> *(!Pointer, !*World)
ppString255 _ _ _ = code { ccall pp_String255 "Is:p:A" }
ppUInt8 :: !Int !String !*World -> *(!Pointer, !*World)
ppUInt8 _ _ _ = code { ccall pp_UInt8 "Is:p:A" }
ppUInt16 :: !Int !String !*World -> *(!Pointer, !*World)
ppUInt16 _ _ _ = code { ccall pp_UInt16 "Is:p:A" }
ppUInt32 :: !Int !String !*World -> *(!Pointer, !*World)
ppUInt32 _ _ _ = code { ccall pp_UInt32 "Is:p:A" }
ppInt8 :: !Int !String !*World -> *(!Pointer, !*World)
ppInt8 _ _ _ = code { ccall pp_Int8 "Is:p:A" }
ppInt16 :: !Int !String !*World -> *(!Pointer, !*World)
ppInt16 _ _ _ = code { ccall pp_Int16 "Is:p:A" }
ppInt32 :: !Int !String !*World -> *(!Pointer, !*World)
ppInt32 _ _ _ = code { ccall pp_Int32 "Is:p:A" }

runTests :: !String !(Int String *World -> *(Pointer, *World)) ![!a] !*World -> *World | gCSerialise{|*|}, gCDeserialise{|*|}, Gast, gEq{|*|} a
runTests msg pp els w = foldl ffun w (take 100 [e\\e<|-els])
where
	//Needs strictness to not overflows heap
	ffun :: !*World !a -> *World | gCSerialise{|*|}, gCDeserialise{|*|}, Gast, gEq{|*|} a
	ffun w el
		# msg = toString (gCSerialise{|*|} el [])
		# (p, w) = pp (size msg) msg w
		# newmsg = derefCharArray p (size msg)
		| newmsg <> msg = abort
			(   "Unequal serialised values for: " +++ printToString el +++ "\n"
			+++ "gave: " +++ join ", " [toString (toInt i)\\i<-:msg] +++ "\n"
			+++ "got:  " +++ join ", " [toString (toInt i)\\i<-:newmsg] +++ "\n"
			)
		= case runStateT (gCDeserialise{|*|} listTop) [toInt c\\c<-:newmsg] of
			Left e = abort ("Error deserialising: " +++ toString e +++ "\n")
			Right (a, [])
				| a === el = w
				= abort
					(   "Unequal deserialised values\n"
					+++ "gave:  " +++ printToString el +++ "\n"
					+++ "got: " +++ printToString a +++ "\n"
					)
			Right _ = abort
					(   "Input exhausted: \n"
					+++ "val: " +++ printToString el +++ "\n"
					+++ "got: " +++ printToString newmsg +++ "\n"
					)

Start w = runTests "String255" ppString255 ([!fromString s \\ s<|-ggenString 255 4.0 0 255 aStream] :: [!String255])
	$ runTests "UInt8"  ppUInt8  (ggen{|*|} genState :: [!UInt8])
	$ runTests "UInt16" ppUInt16 (ggen{|*|} genState :: [!UInt16])
	$ runTests "UInt32" ppUInt32 (ggen{|*|} genState :: [!UInt32])
	$ runTests "Int8"   ppInt8   (ggen{|*|} genState :: [!Int8])
	$ runTests "Int16"  ppInt16  (ggen{|*|} genState :: [!Int16])
	$ runTests "Int32"  ppInt32  (ggen{|*|} genState :: [!Int32])
	$ w
