module TestByteCodeEncoding

import StdEnv

import Data.GenDefault

import Data.Func
import Data.Functor
import Data.Either
import Data.Tuple
import Data.List
import Data.Maybe
import Control.Applicative
import Control.GenBimap

import mTask.Interpret.Compile
import mTask.Interpret.Device
import mTask.Interpret.Specification
import mTask.Interpret.DSL => qualified :: MTask
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message
import mTask.SemVer

import Gast => qualified <., >., <=., >=.
import Gast.CommandLine
from Gast.StdProperty import For
from Gast.Gen import interleave, diagBent

import Text

eps :== 1.19E-07
reals :== [0.001, -0.001, 1.23E10, -1.23E10, 1.23E20, -1.23E20:map fromInt ints]
ints  :== [0,1,-1,42,-42,100,-100]
longs :== map Long [20000, -20000:ints]
derive class Gast Button, APin, DPin, Gesture, Pin, TaskValue
derive gPrint Long, Either
derive genShow Long

gmm mn mx = [!mn, mx, one, ~one, zero:[!inc mn..dec (dec mx)]]

ggen{|Long|}   s = [!LONG_MAX, LONG_MIN, zero, one, ~one:[!zero..dec LONG_MAX]]

int16s :: [!Int]
int16s = [!i\\(Int16 i)<|-gmm INT16_MIN INT16_MAX]

taskvalues :: [!a] -> [!TaskValue a]
taskvalues as = [|NoValue:interleave (StableValue <$> as) (UnstableValue <$> as)]

realeq :: Real Real -> Bool
realeq a b
	| a == b = True
	= abs (a-b) <= max (abs a) (abs b) * eps

realprop :: Real Real -> Property
realprop x y = check realeq x y

instance == MTDeviceSpec where (==) x y = x === y
instance == BCShareSpec where (==) x y = x === y
instance == Pin where (==) x y = x === y
instance == APin where (==) x y = x === y
instance == DPin where (==) x y = x === y
instance == MTMessageTo where (==) x y = x === y
instance == MTMessageFro where (==) x y = x === y
instance == (TaskValue a) | gEq{|*|} a where (==) x y = x === y
instance == BCTaskType where (==) x y = x === y
instance == BCPeripheral where (==) x y = x === y

testEncoding` :: (a a -> Property) a -> Property | Gast, fromByteCode{|*|}, toByteCode{|*|}, Eq a
testEncoding` pred a = q (runFBC fromByteCode{|*|} (fromString (toByteCode{|*|} a))) (Right (?Just a), [])
	/\ q (runFBC fromByteCode{|*|} (fromString (toByteCode{|*|} a) ++ ['abc'])) (Right (?Just a), ['abc'])
where
	q (Right (?Just e1), c1) (Right (?Just e2), c2) = pred e1 e2 /\ c1 =.= c2
	q e1 e2 = e1 =.= e2

testEncoding :== testEncoding` (=.=)

m l = [|e\\e<|-l]

Start w = flip (exposeProperties [Concise 99999999] [Tests 10000, Bent]) w $
		[ name "test()"          $ cast () testEncoding
		, name "testInt"         $ (testEncoding For m int16s)
		, name "testLong"        $ cast (Long 0) testEncoding
		, name "testBool"        $ cast True testEncoding
		, name "testChar"        $ (testEncoding For m (map toChar [0..255]))
		, name "testReal"        $ (testEncoding` realprop For m reals)
		, name "testButton"      $ cast NoButton testEncoding
		, name "test2TuplesInt"  $ (testEncoding For tuple <$> m int16s <*> m int16s)
		, name "test2TuplesLng"  $ cast (Long 0, Long 0) testEncoding
		, name "test3TuplesInt"  $ (testEncoding For tuple3 <$> m int16s <*> m int16s <*> m int16s)
		, name "test3TuplesLng"  $ cast (Long 0, Long 0, Long 0) testEncoding
		, name "testAPin"        $ cast A0 testEncoding
		, name "testDPin"        $ cast D0 testEncoding
		, name "testPin"         $ cast (AnalogPin A0) testEncoding
		, name "testGesture"     $ cast GNone testEncoding
		, name "testTaskVal()"   $ cast (Value () False) testEncoding
		, name "testTaskValInt"  $ (testEncoding For taskvalues int16s)
		, name "testTaskValLng"  $ cast (Value (Long 0) False) testEncoding
		, name "testTaskValTup"  $ (testEncoding For taskvalues (diagBent int16s int16s))
		]
where
	cast :: a -> (a -> Property) -> (a -> Property)
	cast _ = id
derive gDefault MTDeviceSpec
