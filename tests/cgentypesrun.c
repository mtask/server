#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>

#include "cp.h"

uint8_t rmsg[1000];
uint8_t *msg;
int idx, msglen;
char *current = "none";

uint8_t get(void)
{
	if (idx == msglen) {
		fprintf(stderr, "Input exhausted in %s\n", current);
		exit(EXIT_FAILURE);
	}
	return msg[idx++];
}

void put(uint8_t c)
{
	if (idx == msglen) {
		fprintf(stderr, "Buffer overrun in %s\n", current);
		exit(EXIT_FAILURE);
	}
	msg[idx++] = c;
}

#define pp_(type)\
uint8_t *pp_##type(int sz, uint8_t *newmsg) {\
	current = #type;\
	msg = newmsg;\
	idx = 0;\
	msglen = sz;\
	type r = parse_##type(get, malloc);\
	idx = 0;\
	msg = &rmsg[0];\
	print_##type(put, r);\
	return msg;\
}

pp_(String255)
pp_(UInt8)
pp_(UInt16)
pp_(UInt32)
pp_(Int8)
pp_(Int16)
pp_(Int32)
