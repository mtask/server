module gentypes

import Data.Either
import Data.GenType
import Data.GenType.CCode
import StdEnv
import Text

import Data.UInt
import mTask.Interpret.String255

writeToFile :: [String] String *World -> *World
writeToFile data fp w
	# (ok, f, w) = fopen fp FWriteText w
	| not ok = abort ("Couldn't open " +++ fp +++ " for writing\n")
	# f = foldl (flip fwrites) f data
	# (ok, w) = fclose f w
	| not ok = abort ("Couldn't close " +++ fp +++ "\n")
	= w

Start w = case generateCCode opts s255 of
	Left e = abort ("Error generating code: " +++ e)
	Right (h, c) = writeToFile h "cp.h" (writeToFile c "cp.c" w)
where
	opts = {zero & basename="cp", overrides=
		[ string255gType
		, uint8gType
		, uint16gType
		, uint32gType
		, int8gType
		, int16gType
		, int32gType
		]}
	s255 :: Box GType (String255, UInt8, UInt16, UInt32, Int8, Int16, Int32)
	s255 = gType{|*|}
