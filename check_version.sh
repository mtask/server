#!/bin/bash
set -e
version="$(grep -Po "(?<=^version: ).*" nitrile.yml)"
version_minor=$(<<< $version cut -d'.' -f 2)
code="CURRENT_SERVER_VERSION :== {major = UInt8 ${version%%.*}, minor = UInt8 ${version_minor}, patch = UInt8 ${version##*.}}"
grep -q "$code" -F src/clean-any/mTask/SemVer.dcl
