definition module mTask.Language.NeoPixel

/**
 * Adafruit NeoPixel integration
 */

from Data.UInt import :: UInt8

import mTask.Language

//** Datatype holding a reference to the NeoPixel. May as well be abstract.
:: NeoPixel = NeoPixel Int
//* Connection information for NeoPixel peripherals
:: NeoInfo
	= NeoInfo UInt8 Pin NeoPixelType NeoPixelFreq
instance toString NeoInfo

//* Neopixel type, usually {{NEO_GRB}}
:: NeoPixelType = NEO_GRB | NEO_RGB | NEO_RGBW
//* Neopixel frequency, usually {{NEO_KHZ800}}
:: NeoPixelFreq = NEO_KHZ800 | NEO_KHZ400

/**
 * {{NeoInfo}} for the Wemos RGB LED Shield
 *
 * @type NeoInfo
 */
neopixelWemosRGBLEDShield :== NeoInfo (UInt8 7) (DigitalPin D4) NEO_GRB NEO_KHZ800

derive class iTask NeoPixel, NeoInfo, NeoPixelType, NeoPixelFreq

class NeoPixel v where
	/**
	 * Constructor for a NeoPixel peripheral
	 *
	 * @param Connection information
	 * @param function doing something with the NeoPixel peripheral
	 * @result {{Main}} program
	 */
	neopixel :: NeoInfo ((v NeoPixel) -> Main (v b)) -> Main (v b) | type b

	/**
	 * Set the color of a single pixel
	 *
	 * @param led number
	 * @param r component (0-255)
	 * @param g component (0-255)
	 * @param b component (0-255)
	 */
	setPixelColor :: (v NeoPixel) (v Int) (v Int) (v Int) (v Int) -> MTask v ()
