definition module mTask.Language.Gesture

/**
 * Gesture sensors
 *
 * Currently supported:
 *   - PAJ7620 over I²C
 */

import mTask.Language


//** Datatype holding a reference to the AQS. May as well be abstract.
:: GestureSensor = GestureSensor Int
//** Datatype holding all possible gestures
:: Gesture = GNone | GRight | GLeft | GUp | GDown | GForward | GBackward | GClockwise | GCountClockwise

derive class iTask GestureSensor, Gesture
derive gDefault Gesture
instance toString Gesture
instance == Gesture
instance basicType Gesture

/**
 * The class representing all gesture sensors
 *
 * @var view
 */
class GestureSensor v where
	/**
	 * Constructor for the gesture sensor.
	 *
	 * @param I²C address
	 * @param function doing something with the gesture sensor
	 * @result {{Main}} program
	 */
	gestureSensor :: !I2CAddr ((v GestureSensor) -> Main (v a)) -> Main (v a) | type a

	/**
	 * Detect a gesture with a specified polling rate in {{Gesture}}.
	 *
	 * @param polling rate
	 * @param gesture sensor
	 * @result task yielding readings as an unstable value
	 */
	gesture`      :: (TimingInterval v) (v GestureSensor) -> MTask v Gesture

	/**
	 * Detect a gesture in {{Gesture}}.
	 *
	 * @param polling rate
	 * @param gesture sensor
	 * @result task yielding readings as an unstable value
	 */
	gesture       :: (v GestureSensor) -> MTask v Gesture
	gesture s = gesture` Default s
