definition module mTask.Language.Light

/**
 * Light Intensity Sensors (LISs)
 *
 * Currently supported:
 *   - BH1750 over I²C
 */

import mTask.Language


//** Datatype holding a reference to the LIS. May as well be abstract.
:: LightSensor = LightSensor Int
derive class iTask LightSensor

/**
 * The class representing all LISs
 *
 * @var view
 */
class LightSensor v where
	/**
	 * Constructor for the LIS.
	 *
	 * @param I²C address
	 * @param function doing something with the LIS
	 * @result {{Main}} program
	 */
	lightsensor :: I2CAddr ((v LightSensor) -> Main (v b)) -> Main (v b) | type b
	/**
	 * Measure the light intensity with a specified polling rate in lx.
	 *
	 * @param polling rate
	 * @param LIS
	 * @result task yielding readings as an unstable value
	 */
	light` :: (TimingInterval v) (v LightSensor) -> MTask v Real
	/**
	 * Measure the light intensity in lx.
	 *
	 * @param polling rate
	 * @param LIS
	 * @result task yielding readings as an unstable value
	 */
	light :: (v LightSensor) -> MTask v Real
	light s = light` Default s
