definition module mTask.Language.pinIO

/**
 * General Purpose Input/Output Pins (GPIO pins)
 */

import mTask.Language

/**
 * The class for interacting with the analogue pins.
 *
 * @var view
 */
class aio v where
	/**
	 * Write a value to an analogue pin
	 *
	 * @param analogue pin
	 * @param value
	 * @param task yielding the written value as a stable value after writing
	 */
	writeA :: (v APin) (v Int) -> MTask v Int
	/**
	 * Read the value of an analogue pin with a specified polling rate.
	 *
	 * @param polling rate
	 * @param analogue pin
	 * @param task yielding the read value as an unstable value
	 */
	readA` :: (TimingInterval v) (v APin) -> MTask v Int
	/**
	 * Read the value of an analogue pin.
	 *
	 * @param analogue pin
	 * @param task yielding the read value as an unstable value
	 */
	readA  :: (v APin) -> MTask v Int
	readA p = readA` Default p

/**
 * The class for interacting with the GPIO pins as if they are digital.
 *
 * @var pin type (usually instances don't specify this)
 * @var view
 */
class dio p v | pin p where
	/**
	 * Write a value to a GPIO pin
	 *
	 * @param pin
	 * @param value
	 * @param task yielding the written value as a stable value after writing
	 */
	writeD :: (v p) (v Bool) -> MTask v Bool
	/**
	 * Read the value of an GPIO pin with a specified polling rate.
	 *
	 * @param polling rate
	 * @param pin
	 * @param task yielding the read value as an unstable value
	 */
	readD` :: (TimingInterval v) (v p) -> MTask v Bool | pin p
	/**
	 * Read the value of an GPIO pin.
	 *
	 * @param pin
	 * @param task yielding the read value as an unstable value
	 */
	readD :: (v p) -> MTask v Bool | pin p
	readD p = readD` Default p

/**
 * The class for setting the pinmodes of GPIO pins
 *
 * @var view
 */
class pinMode v where
	/**
	 * Set the pinmode of a pin
	 *
	 * @param pin mode
	 * @param pin
	 * @result task yielding unit as a stable value after setting the pinmode
	 */
	pinMode :: (v PinMode) (v p) -> MTask v () | pin, type p

	/**
	 * Helper macro to globally declare a pin with a specific pinmode
	 *
	 * @param pin
	 * @param pin mode
	 * @param function doing something with this pin
	 * @result {{Main}} program
	 */
	declarePin :: p PinMode ((v p) -> Main (v a)) -> Main (v a) | pin p & type a
	declarePin p pm def :== {main=pinMode (lit pm) (lit p) >>|. unmain (def (lit p))}

//** pin modes
:: PinMode = PMInput | PMOutput | PMInputPullup
/**
 * Helper macro for lifted {{PMInput}}
 *
 * @type (v PinMode)
 */
pminput :== lit PMInput
/**
 * Helper macro for lifted {{PMOutput}}
 *
 * @type (v PinMode)
 */
pmoutput :== lit PMOutput
/**
 * Helper macro for lifted {{PMInputPullup}}
 *
 * @type (v PinMode)
 */
pminputpullup :== lit PMInputPullup
//** Type combining analogue and digital pins
:: Pin = AnalogPin APin | DigitalPin DPin
/**
 * Class for converting a pin to a {{Pin}} type
 *
 * @var view
 * @param pin-like thing
 * @param pin
 */
class pin p :: p -> Pin | type p

instance pin APin, DPin

//** analogue pins
:: APin = A0 | A1 | A2 | A3 | A4 | A5
//** digital pins
:: DPin = D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9 | D10 | D11 | D12 | D13
/**
 * Helper macro for lifted {{A0}}
 *
 * @type (v APin)
 */
a0 :== lit A0
/**
 * Helper macro for lifted {{A1}}
 *
 * @type (v APin)
 */
a1 :== lit A1
/**
 * Helper macro for lifted {{A2}}
 *
 * @type (v APin)
 */
a2 :== lit A2
/**
 * Helper macro for lifted {{A3}}
 *
 * @type (v APin)
 */
a3 :== lit A3
/**
 * Helper macro for lifted {{A4}}
 *
 * @type (v APin)
 */
a4 :== lit A4
/**
 * Helper macro for lifted {{A5}}
 *
 * @type (v APin)
 */
a5 :== lit A5
/**
 * Helper macro for lifted {{D0}}
 *
 * @type (v DPin)
 */
d0 :== lit D0
/**
 * Helper macro for lifted {{D1}}
 *
 * @type (v DPin)
 */
d1 :== lit D1
/**
 * Helper macro for lifted {{D2}}
 *
 * @type (v DPin)
 */
d2 :== lit D2
/**
 * Helper macro for lifted {{D3}}
 *
 * @type (v DPin)
 */
d3 :== lit D3
/**
 * Helper macro for lifted {{D4}}
 *
 * @type (v DPin)
 */
d4 :== lit D4
/**
 * Helper macro for lifted {{D5}}
 *
 * @type (v DPin)
 */
d5 :== lit D5
/**
 * Helper macro for lifted {{D6}}
 *
 * @type (v DPin)
 */
d6 :== lit D6
/**
 * Helper macro for lifted {{D7}}
 *
 * @type (v DPin)
 */
d7 :== lit D7
/**
 * Helper macro for lifted {{D8}}
 *
 * @type (v DPin)
 */
d8 :== lit D8
/**
 * Helper macro for lifted {{D9}}
 *
 * @type (v DPin)
 */
d9 :== lit D9
/**
 * Helper macro for lifted {{D10}}
 *
 * @type (v DPin)
 */
d10 :== lit D10
/**
 * Helper macro for lifted {{D11}}
 *
 * @type (v DPin)
 */
d11 :== lit D11
/**
 * Helper macro for lifted {{D12}}
 *
 * @type (v DPin)
 */
d12 :== lit D12
/**
 * Helper macro for lifted {{D13}}
 *
 * @type (v DPin)
 */
d13 :== lit D13

instance toString DPin, APin, PinMode, Pin
derive class iTask APin, DPin, PinMode, Pin
derive gDefault APin, DPin, PinMode, Pin

