definition module mTask.Language.LEDMatrix

/**
 * Led matrices
 *
 * Currently supported:
 *   - Wemos matrix LED shield 8x8 over I²C
 */

import mTask.Language

//** Datatype holding a reference to the LED matrix. May as well be abstract.
:: LEDMatrix = LEDMatrix Int

//** Connection information %TODO change to I²C address
:: LEDMatrixInfo =
	{ dataPin  :: Pin
	, clockPin :: Pin
	}

derive class iTask LEDMatrix, LEDMatrixInfo

/**
 * The class representing all LED matrices
 *
 * @var view
 */
class LEDMatrix v where
	/**
	 * Constructor for the LED matrix.
	 *
	 * @param connection information
	 * @param function doing something with the LED matrix
	 * @result {{Main}} program
	 */
	ledmatrix   :: LEDMatrixInfo ((v LEDMatrix) -> Main (v b)) -> Main (v b) | type b
	/**
	 * Set the state of a single LED.
	 * Remember to call {{LMDisplay}} after this to flush the changes.
	 *
	 * @param LED matrix
	 * @param x coordinate
	 * @param y coordinate
	 * @param state
	 * @result Task yielding unit as a stable value after setting the state
	 */
	LMDot       :: (v LEDMatrix) (v Int) (v Int) (v Bool) -> MTask v ()
	/**
	 * Change the intensity of the LEDs
	 * Remember to call {{LMDisplay}} after this to flush the changes.
	 *
	 * @param LED matrix
	 * @param intensity
	 * @result Task yielding unit as a stable value after setting the intensity
	 */
	LMIntensity :: (v LEDMatrix) (v Int) -> MTask v ()
	/**
	 * Clear the entire LED matrix
	 * Remember to call {{LMDisplay}} after this to flush the changes.
	 *
	 * @param LED matrix
	 * @result Task yielding unit as a stable value after clearing
	 */
	LMClear     :: (v LEDMatrix) -> MTask v ()
	/**
	 * Flush the buffer and write the actual LEDs
	 *
	 * @param LED matrix
	 * @result Task yielding unit as a stable value after flushing
	 */
	LMDisplay   :: (v LEDMatrix) -> MTask v ()
