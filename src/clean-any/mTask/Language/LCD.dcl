definition module mTask.Language.LCD

/**
 * LCD shields
 *
 * Currently supported:
 *   - None (at least in the interpreter) 
 */

import mTask.Language

//** Datatype holding a reference to the LCD screen. May as well be abstract.
:: LCD = LCDid Int

instance toString LCD
instance toString Button
derive class iTask LCD, Button
derive gDefault Button
instance == Button
instance basicType Button

//** Button id
:: Button = RightButton | UpButton | DownButton | LeftButton | SelectButton | NoButton

/**
 * Helper macro for lifted {{RightButton}}
 * 
 * @type (v Button)
 */
rightButton     :== lit RightButton
/**
 * Helper macro for lifted {{UpButton}}
 * 
 * @type (v Button)
 */
upButton        :== lit UpButton
/**
 * Helper macro for lifted {{NoButton}}
 *
 * @type (v Button)
 */
downButton      :== lit DownButton
/**
 * Helper macro for lifted {{LeftButton}}
 * 
 * @type (v Button)
 */
leftButton      :== lit LeftButton
/**
 * Helper macro for lifted {{SelectButton}}
 * 
 * @type (v Button)
 */
selectButton    :== lit SelectButton
/**
 * Helper macro for lifted {{NoButton}}
 * 
 * @type (v Button)
 */
noButton        :== lit NoButton

/**
 * The class representing all LDCs
 *
 * @var view
 */
class lcd v where
	/**
	 * Constructor for the LCD.
	 *
	 * @param Register select pin
	 * @param Read/Write pin
	 * @param Data pins
	 * @param function doing something with the AQS
	 * @result {{Main}} program
	 */
	LCD         :: Int Int [DPin] ((v LCD)->Main (v b)) -> Main (v b) | type b
	/**
	 * Print something to the LCD
	 *
	 * @param LCD
	 * @param thing to print
	 * @result Task yielding the bytes written as a stable value
	 */
	print       :: (v LCD) (v t) -> MTask v Int  | type t       // returns bytes written
	/**
	 * Set the cursor to a specific coordinate
	 *
	 * @param LCD
	 * @param x coordinate
	 * @param y coordinate
	 * @result Task yielding unit after setting the cursor
	 */
	setCursor   :: (v LCD) (v Int) (v Int) -> MTask v ()
	/**
	 * Scroll the screen one position to the left
	 *
	 * @param LCD
	 * @result Task yielding unit after scrolling
	 */
	scrollLeft  :: (v LCD) -> MTask v ()
	/**
	 * Scroll the screen one position to the right
	 *
	 * @param LCD
	 * @result Task yielding unit after scrolling
	 */
	scrollRight :: (v LCD) -> MTask v ()
	/**
	 * Query the status of a button
	 *
	 * @param Button
	 * @result Task yielding the status as an unstable value
	 */
	pressed     :: (v Button) -> MTask v Bool

/**
 * The class representing an LDC button? Deprecated
 *
 * @var view
 * @result Task yielding which button is pressed
 */
class buttonPressed v :: MTask v Button

/**
 * Helper macro to print text at a specific position
 *
 * @param LCD
 * @param x coordinate
 * @param y coordinate
 * @param thing to print
 * @type (v LCD) (v Int) (v Int) (v t) -> MTask v Int
 */
printAt lcd x y z :== setCursor lcd x y >>|. print lcd z
