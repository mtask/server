implementation module mTask.Language.AirQuality

import iTasks

import mTask.Language

derive class iTask AirQualitySensor, AirQualitySensorInfo
instance toString AirQualitySensorInfo where toString a = toSingleLineText a
