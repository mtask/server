definition module mTask.Language.DHT

/**
 * Digital Humidity and Temperature Sensors (DHTs)
 *
 * Currently supported:
 *   - DHT (all types) over OneWire
 *   - SHT30x over I²C
 */

import mTask.Language
from Data.UInt import :: UInt8

//** Datatype holding a reference to the AQS. May as well be abstract.
:: DHT = Dht Int
//** DHT connection information
:: DHTInfo
	= DHT_DHT Pin DHTtype
	//** DHT (all types) via One-Wire
	//**
	//** @var communication pin
	//** @var Type
	| DHT_SHT I2CAddr
	//** SHT30x via I²C
	//**
	//** @var I²C address
//** DHT Type
:: DHTtype = DHT11 | DHT21 | DHT22

instance toString DHTtype
derive class iTask DHTtype, DHT, DHTInfo

/**
 * The class representing all DHTs
 *
 * @var view
 */
class dht v where
	/**
	 * Constructor for the DHT.
	 *
	 * @param connection information 
	 * @param function doing something with the DHT
	 * @result {{Main}} program
	 */
	DHT          :: DHTInfo ((v DHT)->Main (v b)) -> Main (v b) | type b

	/**
	 * Measure the temperature with a specific polling rate in °C.
	 *
	 * @param polling rate 
	 * @param DHT
	 * @result task yielding readings as an unstable value
	 */
	temperature` :: (TimingInterval v) (v DHT) -> MTask v Real

	/**
	 * Measure the temperature in °C.
	 *
	 * @param polling rate
	 * @param DHT
	 * @result task yielding readings as an unstable value
	 */
	temperature  :: (v DHT) -> MTask v Real
	temperature  s = temperature` Default s

	/**
	 * Measure the relative humidity with a specific polling rate in %.
	 *
	 * @param polling rate 
	 * @param DHT
	 * @result task yielding readings as an unstable value
	 */
	humidity`    :: (TimingInterval v) (v DHT) -> MTask v Real

	/**
	 * Measure the relative humidity in %.
	 *
	 * @param DHT
	 * @result task yielding readings as an unstable value
	 */
	humidity     :: (v DHT) -> MTask v Real
	humidity s = humidity` Default s
