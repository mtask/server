definition module mTask.Language.PIR

/**
 * Passive InfraRed sensor (PIR).
 * PIRs work just like regular digital GPIOs so this file only contains convenience macros
 */

import mTask.Language

//** Type synonym a PIR
:: PIR :== DPin

/**
 * The class representing all PIRs
 *
 * @var view
 */
class PIR v | step, expr, pinMode v & dio DPin v where
	/**
	 * Constructor for the PIR.
	 * It just sets the pinmode to {{PMInput}}.
	 *
	 * @param pin
	 * @param function doing something with the AQS
	 * @result {{Main}} program
	 */
	PIR :: DPin ((v PIR) -> Main (v b)) -> Main (v b) | type b & expr, step, pinMode v
	PIR dp def :== declarePin dp PMInput def

	/**
	 * Measure the motion with a specified polling rate.
	 *
	 * @param polling rate
	 * @param PIR
	 * @result task yielding readings as an unstable value
	 */
	motion` :: (TimingInterval v) (v PIR) -> MTask v Bool
	motion` i p :== readD` i p 

	/**
	 * Measure the motion.
	 *
	 * @param PIR
	 * @result task yielding readings as an unstable value
	 */
	motion :: (v PIR) -> MTask v Bool | dio DPin v
	motion p :== readD p
