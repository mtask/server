implementation module mTask.Language.Gesture

import iTasks

import mTask.Language

derive class iTask GestureSensor, GestureSensorInfo, Gesture
derive gDefault Gesture
instance toString Gesture where toString a = toSingleLineText a
instance toString GestureSensorInfo  where toString a = toSingleLineText a
instance == Gesture where (==) l r = l === r
instance basicType Gesture where basicType = GNone
