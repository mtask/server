definition module mTask.Language.Expressions

/**
 * Expressions, functions, tuples and helper functions
 */

import mTask.Language

import StdOverloaded
import StdClass

/**
 * The class representing all expressions.
 *
 * @var view
 */
class expr v where
	/**
	 * lift a value to the mTask domain
	 *
	 * @param value
	 * @result lifted value
	 */
	lit :: t -> v t | type t
	//** Addition
	(+.) infixl 6 :: (v t) (v t) -> v t | basicType, +, zero t
	//** Subtraction
	(-.) infixl 6 :: (v t) (v t) -> v t | basicType, -, zero t
	//** Multiplication
	(*.) infixl 7 :: (v t) (v t) -> v t | basicType, *, zero, one t
	//** Division
	(/.) infixl 7 :: (v t) (v t) -> v t | basicType, /, zero t
	//** Logical and
	(&.) infixr 3 :: (v Bool) (v Bool) -> v Bool
	//** Logical or
	(|.) infixr 2 :: (v Bool) (v Bool) -> v Bool
	//** Logical negation
	Not           :: (v Bool) -> v Bool
	//** Equal
	(==.) infix 4 :: (v a) (v a) -> v Bool | Eq, basicType a
	//** Not equal
	(!=.) infix 4 :: (v a) (v a) -> v Bool | Eq, basicType a
	//** Lesser than
	(<.)  infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	//** Greater than
	(>.)  infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	//** Lesser than or equal to
	(<=.) infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	//** Greater than or equal to
	(>=.) infix 4 :: (v a) (v a) -> v Bool | Ord, basicType a
	/**
	 * ternary conditional expression
	 *
	 * @param predicate
	 * @param then expression
	 * @param else expression
	 * @result result
	 */
	If :: (v Bool) (v t) (v t) -> v t | type t

/**
 * The class representing all functions
 *
 * Usually, a single instance is given for every arity.
 *
 * ```
 * instance () SomeView where ...
 * instance (SomeView a) SomeView where ...
 * instance (SomeView a, SomeView b) SomeView where ...
 * ...
 * ```
 *
 * @var argument type
 * @var view
 * @param the function specification
 * @result the Main program
 */
class fun a v :: ((a->v s)->In (a->v s) (Main (MTask v u))) -> Main (MTask v u) | type s & type u

/**
 * The class representing 2-tuples
 *
 * @var view
 */
class tupl v where
	//** Constructor (lifted {{(,)}}/{{tuple}})
	tupl :: (v a) (v b) -> v (a, b) | type a & type b
	//** Selector for the first argument (lifted {{fst}})
	first  :: (v (a, b)) -> v a | type a & type b
	//** Selector for the second argument (lifted {{snd}})
	second :: (v (a, b)) -> v b | type a & type b

	/*
	 * Helper macro to transforms a function on v of tuple to a tuple of v's
	 *
	 * @param The function to transform
	 * @result The transformed function
	 * @type ((v a, v b) -> v c) -> ((v (a, b)) -> v c) | tupl v & type a & type b
	 */
	tupopen f :== \v->f (first v, second v)

/**
 * Helper macro for lifted {{True}}
 *
 * @type (v Bool)
 */
true  :== lit True

/**
 * Helper macro for lifted {{False}}
 *
 * @type (v Bool)
 */
false :== lit False
