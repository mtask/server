definition module mTask.Language.i2cbutton

/**
 * I²C buttons
 *
 * Currently supported:
 *   - Buttons on the v2.1.0 WEMOS OLED SHIELD
 */
import mTask.Language

instance toString ButtonStatus
derive class iTask ButtonStatus, I2CButton
instance == ButtonStatus

//** Datatype holding a reference to the AQS. May as well be abstract.
:: I2CButton =: I2CButton Int
//** Button status
:: ButtonStatus = ButtonNone | ButtonPress | ButtonLong | ButtonDouble | ButtonHold

/**
 * The class representing all I²C breakouts with two buttons
 *
 * @var view
 */
class i2cbutton v where
	/**
	 * Constructor for the DHT.
	 *
	 * @param I²C address
	 * @param function doing something with the buttons
	 * @result {{Main}} program
	 */
	i2cbutton :: !I2CAddr ((v I2CButton) -> Main (v b)) -> Main (v b) | type b

	/**
	 * Retrieve the status of the A button with a specified polling rate in {{ButtonStatus}}
	 *
	 * @param polling rate 
	 * @param button
	 * @result task immediately yielding readings
	 */
	AButton` :: (TimingInterval v) (v I2CButton) -> MTask v ButtonStatus

	/**
	 * Retrieve the status of the A button in {{ButtonStatus}}
	 *
	 * @param button
	 * @result task yielding readings as an unstable value
	 */
	AButton :: (v I2CButton) -> MTask v ButtonStatus
	AButton b = AButton` Default b

	/**
	 * Retrieve the status of the B button with a specified polling rate in {{ButtonStatus}}
	 *
	 * @param polling rate
	 * @param button
	 * @result task yielding readings as an unstable value
	 */
	BButton` :: (TimingInterval v) (v I2CButton) -> MTask v ButtonStatus

	/**
	 * Retrieve the status of the B button in {{ButtonStatus}}
	 *
	 * @param button
	 * @result task immediately yielding readings
	 */
	BButton :: (v I2CButton) -> MTask v ButtonStatus
	BButton b = BButton` Default b
