implementation module mTask.Language.Light

import iTasks

import mTask.Language

derive class iTask LightSensor, LightSensorInfo
instance toString LightSensorInfo where toString a = toSingleLineText a
