definition module mTask.Language.AirQuality

/**
 * Air Quality Sensors (AQSs)
 *
 * Currently supported:
 *   - CCS811 over I²C
 *   - SGP30 over I²C (fixed address)
 */

import mTask.Language

//* Datatype holding a reference to the AQS. May as well be abstract.
:: AirQualitySensor = AirQualitySensor Int
//* AQS connection info
:: AirQualitySensorInfo
	= AQS_CCS811 I2CAddr
	//* CCS811 sensor connected over I²C
	| AQS_SGP30
	//* SGP30 sensor connected over I²C (fixed address)
derive class iTask AirQualitySensor, AirQualitySensorInfo
instance toString AirQualitySensorInfo

/**
 * {{AirQualitySensorInfo}} for the Adafruit CCS811 breakout board
 *
 * @type AirQualitySensorInfo
 */
airqualitySensorCCS811Breakout :== AQS_CCS811 (i2c 0x5a)
/**
 * {{AirQualitySensorInfo}} for the Wemos D1 mini SGP30 shield
 *
 * @type AirQualitySensorInfo
 */
airqualitySensorWemosSGP30Shield :== AQS_SGP30

/**
 * The class representing all AQSs
 *
 * @var view
 */
class AirQualitySensor v where
	/**
	 * Constructor for the AQS.
	 *
	 * @param Connection information
	 * @param function doing something with the AQS
	 * @result {{Main}} program
	 */
	airqualitySensor :: AirQualitySensorInfo ((v AirQualitySensor) -> Main (v a)) -> Main (v a) | type a

	/**
	 * Calibrate the air quality sensor with a temperature and humidity
	 *
	 * @param AQS
	 * @param temperature
	 * @param humidity
	 * @param task yielding () as a stable value after calibration
	 */
	setEnvironmentalData :: (v AirQualitySensor) (v Real) (v Real) -> MTask v ()

	/**
	 * Measure the volatile organic components in the air with a specified polling rate in ppb.
	 *
	 * @param polling rate
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	tvoc` :: (TimingInterval v) (v AirQualitySensor) -> MTask v Long

	/**
	 * Measure the volatile organic components in the air in ppm.
	 *
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	tvoc :: (v AirQualitySensor) -> MTask v Long
	tvoc s = tvoc` Default s

	/**
	 * Measure the equivalent CO₂ levels in the air with a specified polling rate in ppm.
	 *
	 * @param polling rate
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	co2` :: (TimingInterval v) (v AirQualitySensor) -> MTask v Long

	/**
	 * Measure the equivalent CO₂ levels in the air in ppm.
	 *
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	co2 :: (v AirQualitySensor) -> MTask v Long
	co2 s = co2` Default s

	/**
	 * Utility macro to calibrate an AQS using a DHT sensor
	 *
	 * @param AQS
	 * @param {{DHT}}
	 * @result task immediately yielding unit after calibration
	 */
	setEnvFromDHT :: (v AirQualitySensor) (v DHT) -> MTask v () | tupl, .&&., dht, step, expr v
	setEnvFromDHT aqs dht :== temperature dht .&&. humidity dht
		>>~. \x->setEnvironmentalData aqs (first x) (second x)
