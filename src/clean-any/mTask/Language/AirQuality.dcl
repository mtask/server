definition module mTask.Language.AirQuality

/**
 * Air Quality Sensors (AQSs)
 *
 * Currently supported:
 *   - CCS811 over I²C
 */

import mTask.Language

//** Datatype holding a reference to the AQS. May as well be abstract.
:: AirQualitySensor = AirQualitySensor Int
derive class iTask AirQualitySensor

/**
 * The class representing all AQSs
 *
 * @var view
 */
class AirQualitySensor v where
	/**
	 * Constructor for the AQS.
	 *
	 * @param I²C address
	 * @param function doing something with the AQS
	 * @result {{Main}} program
	 */
	airqualitysensor :: I2CAddr ((v AirQualitySensor) -> Main (v a)) -> Main (v a) | type a

	/**
	 * Calibrate the air quality sensor with a temperature and humidity
	 *
	 * @param AQS
	 * @param temperature
	 * @param humidity
	 * @param task yielding () as a stable value after calibration
	 */
	setEnvironmentalData :: (v AirQualitySensor) (v Real) (v Real) -> MTask v ()

	/**
	 * Measure the volatile organic components in the air with a specified polling rate in ppb.
	 *
	 * @param polling rate
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	tvoc` :: (TimingInterval v) (v AirQualitySensor) -> MTask v Int

	/**
	 * Measure the volatile organic components in the air in ppm.
	 *
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	tvoc :: (v AirQualitySensor) -> MTask v Int
	tvoc s = tvoc` Default s

	/**
	 * Measure the equivalent CO₂ levels in the air with a specified polling rate in ppm.
	 *
	 * @param polling rate
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	co2` :: (TimingInterval v) (v AirQualitySensor) -> MTask v Int

	/**
	 * Measure the equivalent CO₂ levels in the air in ppm.
	 *
	 * @param AQS
	 * @result task yielding readings as an unstable value
	 */
	co2 :: (v AirQualitySensor) -> MTask v Int
	co2 s = co2` Default s

	/**
	 * Utility macro to calibrate an AQS using a DHT sensor
	 *
	 * @param AQS
	 * @param {{DHT}}
	 * @result task immediately yielding unit after calibration
	 */
	setEnvFromDHT :: (v AirQualitySensor) (v DHT) -> MTask v () | tupl, .&&., dht, step, expr v
	setEnvFromDHT aqs dht :== temperature dht .&&. humidity dht
		>>~. \x->setEnvironmentalData aqs (first x) (second x)
