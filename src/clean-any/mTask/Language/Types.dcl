definition module mTask.Language.Types

/**
 * The file containing many of the mTask types an helper functions
 *
 * Integers in mTask are _always_ 16-bit ints
 * Longs in mTask are _always_ 32-bit ints
 * Reals in mTask are _always_ 32-bit floats
 */

import mTask.Language

from mTask.Interpret.ByteCodeEncoding import generic toByteCode, class toByteWidth, generic fromByteCode, :: FBC

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..)
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.Tuple import instance toString ()

/**
 * Helper type for infix combination, basically an infix tuple.
 *
 * @var left-hand side
 * @var righthand side
 */
:: In a b = In infix 0 a b
/**
 * Helper type to restrict scope, may as well be abstract.
 *
 * @var thing to restrict
 */
:: Main m = {main :: m}

/**
 * Constructor for the {{Main}} type
 *
 * @param thing to restrict scope
 * @result {{Main}} object
 */
main :: a -> Main a
/**
 * Deconstructor for the {{Main}} type
 *
 * @param {{Main}} object
 * @result thing that had the scope restricted
 */
unmain :: (Main a) -> a

/**
 * Class collecting all the typeclasses needed for a type to be suitable for mTask
 *
 * @var type
 */
class type t | toString, iTask, toByteWidth, gDefault{|*|}, toByteCode{|*|}, fromByteCode{|*|} t

/**
 * Class restricting {{type}} even more, only basic types should have instances of this class.
 * It is used for arithmetic operations for example
 */
class basicType t | type t where basicType :: t
instance basicType Int, Bool, Real, Char, ()

/**
 * Convert something to an integer in mTask
 *
 * @var view
 * @param thing to convert to an integer
 * @result an integer
 */
class int v a :: (v a) -> v Int

/**
 * Convert something to a real in mTask
 *
 * @var view
 * @param thing to convert to a real
 * @result a real
 */
class real v a :: (v a) -> v Real

/**
 * Helper function to retrieve the type of a value using dynamics.
 *
 * @param value
 * @result string representation of the type
 */
type2string :: a -> String | TC a

//** An I²C address.
:: I2CAddr =: I2C UInt8
/**
 * Constructor for the {{I2CAddr}} datatype that can be used curried.
 *
 * @type I2CAddr
 */
i2c x :== I2C (fromInt x)

/**
 * Static amount of milliseconds to use in delay functions
 */
ms :== lit

/**
 * Timing interval
 *
 * @var view
 */
:: TimingInterval v
	= Default
	//** Deduct the default interval from the task type
	| BeforeMs (v Int)
	//** Ask for execution before the given number of milliseconds
	| BeforeSec (v Int)
	//** Ask for execution before the given number of seconds
	| ExactMs (v Int)
	//** Ask for execution exactly after the given number of milliseconds
	| ExactSec (v Int)
	//** Ask for execution exactly after the given number of seconds
	| RangeMs (v Int) (v Int)
	//** Ask for execution within the range of milliseconds
	| RangeSec (v Int) (v Int)
	//** Ask for execution within the range of seconds

instance toInt I2CAddr
instance toString I2CAddr
derive class iTask \ gEditor, gText I2CAddr
derive gText I2CAddr
derive gEditor I2CAddr
