implementation module mTask.Language.DHT

import Text.GenJSON

import mTask.Language

derive class iTask DHTtype, DHT, DHTInfo

instance toString DHTtype where toString t = toSingleLineText t
