definition module mTask.Language.Tasks

/**
 * Tasks and task combinators
 */

import mTask.Language

/**
 * The basic task for lifting a value as a stable value to the task domain
 *
 * @var view
 * @param the value
 * @result task yielding the value as a stable value
 */
class rtrn     v :: (v t) -> MTask v t | type t
/**
 * The basic task for lifting a value as an unstable value to the task domain
 *
 * @var view
 * @param the value
 * @result task yielding the value as an unstable value
 */
class unstable v :: (v t) -> MTask v t | type t

/**
 * Sequential task combination
 *
 * @var view
 */
class step v | expr v where
	/**
	 * The step combinator, all other sequential combinators can be derived from this one.
	 * This combinator executes the left-hand side, yielding no value.
	 * Once the task value of the left-hand side matches a continuation, the task is rewritten to the right-hand side, yielding the task value of the right-hand side.
	 *
	 * @param the left-hand side task
	 * @param step continuations
	 * @result the combined task
	 */
	(>>*.) infixl 1 :: (MTask v t) [Step v t u] -> MTask v u | type u & type t

	/**
	 * The bind combinator, uses the result (if and only if it's stable) of the left-hand side to produce the right-hand side.
	 *
	 * @param the left-hand side task
	 * @param the function producing the right-hand side task
	 * @result the combined task
	 */
	(>>=.) infixl 0 :: (MTask v t) ((v t) -> MTask v u) -> MTask v u | expr v & type u & type t
	(>>=.) ma amb = ma >>*. [IfStable (\_->lit True) amb]

	/**
	 * The sequence combinator, if the left-hand side is stable, it continues with the right-hand side.
	 *
	 * @param the left-hand side task
	 * @param the right-hand side task
	 * @result the combined task
	 */
	(>>|.) infixl 0 :: (MTask v t) (MTask v u) -> MTask v u | expr v & type u & type t
	(>>|.) ma mb  = ma >>*. [IfStable (\_->lit True) \_->mb]

	/**
	 * The unstable bind combinator, uses the result of the left-hand side to produce the right-hand side.
	 *
	 * @param the left-hand side task
	 * @param the function producing the right-hand side task
	 * @result the combined task
	 */
	(>>~.) infixl 0 :: (MTask v t) ((v t) -> MTask v u) -> MTask v u | expr v & type u & type t
	(>>~.) ma amb = ma >>*. [IfValue (\_->lit True) amb]

	/**
	 * The unstable sequence combinator, if the left-hand side has a result, it continues with the right-hand side.
	 *
	 * @param the left-hand side task
	 * @param the right-hand side task
	 * @result the combined task
	 */
	(>>..) infixl 0 :: (MTask v t) (MTask v u) -> MTask v u | expr v & type u & type t
	(>>..) ma mb  = ma >>*. [IfValue (\_->lit True) \_->mb]

/**
 * Step continuations
 *
 * @var view
 * @var argument type
 * @var result type
 */
:: Step v t u
	= IfValue    ((v t) -> v Bool) ((v t) -> MTask v u)
	//** Is called when the left-hand side task has a value
	//** @param predicate
	//** @param function producing the right-hand side task
	| IfStable   ((v t) -> v Bool) ((v t) -> MTask v u)
	//** Is called when the left-hand side has a stable value
	//** @param predicate
	//** @param function producing the right-hand side task
	| IfUnstable ((v t) -> v Bool) ((v t) -> MTask v u)
	//** Is called when the left-hand side has an unstable value
	//** @param predicate
	//** @param function producing the right-hand side task
	| IfNoValue                         (MTask v u)
	//** Is called when the left-hand side no value
	//** @param function producing the right-hand side task
	| Always                            (MTask v u)
	//** Is always called
	//** @param function producing the right-hand side task

/**
 * Repeating tasks
 *
 * @var view
 */
class rpeat v where
	/**
	 * Executes the task until it yields a stable value, after which it is restarted.
	 *
	 * @param task
	 * @param the transformed task
	 */
	rpeat :: (MTask v a) -> MTask v a | type a
	rpeat t :== rpeatEvery Default t
	/**
	 * Executes the task until it yields a stable value, after which it is restarted at most once every given timing interval.
	 *
	 * @param restarting interval
	 * @param task
	 * @param the transformed task
	 */
	rpeatEvery :: (TimingInterval v) (MTask v a) -> MTask v a | type a

/**
 * Combine two tasks in parallel and combine the results into a tuple.
 *
 * @var view
 * @param left-hand side task
 * @param right-hand side task
 * @param combined transformed task
 */
class (.&&.) infixr 4 v :: (MTask v a) (MTask v b) -> MTask v (a, b) | type a & type b
/**
 * Combine two tasks in parallel and combine the results into a single value according to the `combine` function given below.
 *
 * ```
 * combine :: (TaskValue a) (TaskValue a) -> TaskValue a
 * combine NoValue        tv           = tv
 * combine tv             NoValue      = tv
 * combine (Value ls lv) (Value rs rv)
 *     | ls = Value True lv
 *     | rs = Value True rv
 *     | otherwise = Value False lv
 * ```
 *
 * @var view
 * @param left-hand side task
 * @param right-hand side task
 * @param combined transformed task
 */
class (.||.) infixr 3 v :: (MTask v a) (MTask v a) -> MTask v a | type a

/**
 * The basic task that waits for a specified amount of time
 *
 * @var view
 * @param number of milliseconds to wait
 * @result task that yields
 *   - when the time has passed: a stable value with the number of milliseconds it overshot the waiting time
 *   - when the time has not passed: an unstable value with the number of milliseconds it still needs to wait
 */
class delay v :: (v n) -> MTask v Long | type n & long v n
