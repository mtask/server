implementation module mTask.Language.Types

import Data.Array
import StdEnv
import iTasks
import mTask.Language
from Data.Map import :: Map(..), singleton
import Data.UInt
import Data.Functor
import Data.Func
from Control.Monad import class Monad(bind), >>=, =<<

main :: a -> Main a
main a = {main=a}

unmain :: (Main a) -> a
unmain a = a.main

instance basicType Int where basicType = 42
instance basicType Bool where basicType = False
instance basicType Real where basicType = 3.1415
instance basicType Char where basicType = 'p'
instance basicType () where basicType = ()

type2string :: a -> String | TC a
type2string a = toString (typeCodeOfDynamic (dynamic a))

toHex :: Int Int -> String
toHex numb x = {#hex.[(x>>(4*i)) bitand 0xf]\\i<-reverse [0..numb-1]}
where
	hex = "0123456789abcdef"

fromHex :: String -> MaybeError [String] a | zero, fromInt, ^, *, + a
fromHex s
	# s = if (size s > 2 && s.[0] == '0' && (s.[1] == 'x' || s.[1] == 'X'))
		(s % (2, size s)) s
	| size s == 0 = Error ["Please provide a hexadecimal value"]
	| foldrArr ((||) o not o isHexDigit) False s
		= Error ["Invalid hexadecimal characters: 0-9a-fA-F"]
	= Ok (foldlArrWithKey fh zero s)
where
	fh :: !Int !a !Char -> a | fromInt, ^, *, + a
	fh idx acc c
		= fromInt
				(if (c >= '0' && c <= '9') (toInt c - toInt '0')
				(if (c >= 'a' && c <= 'f') (toInt c - toInt 'a' + 10)
				(/*if (c >= 'A' && c <= 'F')*/ (toInt c - toInt 'A' + 10))))
			+ acc * fromInt 16

instance toInt I2CAddr where toInt (I2C i) = toInt i
instance toString I2CAddr where toString i = "0x" +++ toHex 2 (toInt i)

derive class iTask \ gEditor, gText I2CAddr

gText{|I2CAddr|} _ val = gTextWithToString val
gEditor{|I2CAddr|} ViewValue
	= mapEditorWrite (const EmptyEditor)
	$ mapEditorRead toString textView
gEditor{|I2CAddr|} EditValue
	= mapEditorWrite ((=<<) $ maybeErrorToEditorReport o fmap i2c o fromHex)
	$ mapEditorRead toString
	$ textField <<@ maxlengthAttr 4 <<@ minlengthAttr 1
