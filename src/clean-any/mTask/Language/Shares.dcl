definition module mTask.Language.Shares

/**
 * Shared Data Sources (SDSs)
 */

import iTasks.SDS.Definition
import mTask.Language

//** Datatype holding a reference to the SDS. May as well be abstract.
:: Sds a = Sds Int

/**
 * The class representing access to SDSs and constructors for local SDSs
 *
 * @var view
 */
class sds v where
	/**
	 * Constructor for a local SDSs.
	 *
	 * example
	 * ```
	 * liftsds \mTaskSDS=42
	 * In {main=...}
	 * ```
	 *
	 * @param function doing something with the SDS and the initial value
	 * @result {{Main}} program
	 */
	sds     :: ((v (Sds t))->In t (Main (MTask v u))) -> Main (MTask v u) | type t & type u
	/**
	 * Write the value of an SDS.
	 *
	 * @param SDS
	 * @param value
	 * @result task yielding the value as a stable value after writing
	 */
	setSds  :: (v (Sds t)) (v t) -> MTask v t | type t
	/**
	 * Atomically update the value of an SDS.
	 *
	 * @param SDS
	 * @param update function
	 * @result task yielding the value as a stable value after updating
	 */
	updSds  :: (v (Sds t)) ((v t) -> v t) -> MTask v t | type t
	/**
	 * Watch the value of an SDS with a specified polling rate.
	 *
	 * @param polling rate
	 * @param SDS
	 * @result task yielding the value as an ustable value
	 */
	getSds` :: (TimingInterval v) (v (Sds t)) -> MTask v t | type t
	/**
	 * Watch the value of an SDS.
	 *
	 * @param SDS
	 * @result task yielding the value as an ustable value
	 */
	getSds  :: (v (Sds t)) -> MTask v t | type t
	getSds sds = getSds` Default sds

/**
 * The class representing lifted iTask SDSs
 *
 * @var view
 */
class liftsds v where
	/**
	 * Construct for an iTask SDS
	 *
	 * example:
	 *
	 * ```
	 * iTaskSDS = sharedStore "someShare" 42
	 * ...
	 * liftsds \mTaskSDS=iTaskSDS
	 * In {main=...}
	 * ```
	 *
	 * @param function doing something with the SDS and the reference to the iTask SDS
	 * @result {{Main}} program
	 */
	liftsds :: ((v (Sds t))->In (Shared sds t) (Main (MTask v u))) -> Main (MTask v u) | type t & type u & RWShared sds & TC (sds () t t)
