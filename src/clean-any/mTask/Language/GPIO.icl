implementation module mTask.Language.GPIO

import Text.GenJSON

import mTask.Language

instance pin APin where pin p = AnalogPin p
instance pin DPin where pin p = DigitalPin p

instance toString DPin where toString p = toSingleLineText p
instance toString APin where toString p = toSingleLineText p
instance toString Pin where
	toString (AnalogPin p) = toString p
	toString (DigitalPin p) = toString p
instance toString PinMode where toString p = toSingleLineText p

derive class iTask APin, DPin, PinMode, Pin
derive gDefault APin, DPin, PinMode, Pin
