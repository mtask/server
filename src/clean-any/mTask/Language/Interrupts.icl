implementation module mTask.Language.Interrupts

import mTask.Language

instance toString InterruptMode where 
  toString mode = toSingleLineText mode

derive class iTask InterruptMode
derive gDefault InterruptMode
