definition module mTask.Language.MTask

import mTask.Language
from iTasks.WF.Definition import :: TaskValue

/**
 * Helper synonymtype to represent an mTask task
 *
 * @var view
 * @var type of the task
 */
:: MTask v a :== v (TaskValue a)

instance toString (TaskValue a) | toString a
derive gDefault TaskValue

/**
 * Collection of mTask DSL classes
 *
 * A class may only have 32 members so {{mtaskfuns}} contains more classes.
 */
class mtask v
	| aio v
	& expr v
	& delay v
	& dio APin v
	& dio DPin v
	& pinMode v
	& interrupt v
	& rpeat v
	& rtrn v
	& tupl v
	& sds v
	& step v
	& unstable v
	& .&&. v
	& .||. v
	& long v Int
	& long v Long
	& long v Real
	& int v Int
	& int v Long
	& int v Real
	& real v Int
	& real v Long
	& real v Real
	& mtaskfuns v
//** Subset of all possible functions in mTask
class mtaskfuns v
	| fun () v
	& fun (v DPin) v
	& fun (v Bool) v
//	& fun (v Char) v
	& fun (v Int) v
	& fun (v Long) v
	& fun (v Real) v
//	& fun (v DPin, v DPin) v
	& fun (v DPin, v Bool) v
//	& fun (v DPin, v Char) v
	& fun (v DPin, v Int) v
//	& fun (v DPin, v Long) v
//	& fun (v DPin, v Real) v
//	& fun (v Bool, v DPin) v
//	& fun (v Bool, v Bool) v
//	& fun (v Bool, v Char) v
//	& fun (v Bool, v Int) v
//	& fun (v Bool, v Long) v
//	& fun (v Bool, v Real) v
//	& fun (v Char, v DPin) v
//	& fun (v Char, v Bool) v
//	& fun (v Char, v Char) v
//	& fun (v Char, v Int) v
//	& fun (v Char, v Long) v
//	& fun (v Char, v Real) v
//	& fun (v Int, v DPin) v
//	& fun (v Int, v Bool) v
//	& fun (v Int, v Char) v
	& fun (v Int, v Int) v
//	& fun (v Int, v Long) v
//	& fun (v Int, v Real) v
//	& fun (v Long, v DPin) v
//	& fun (v Long, v Bool) v
//	& fun (v Long, v Char) v
//	& fun (v Long, v Int) v
	& fun (v Long, v Long) v
//	& fun (v Long, v Real) v
//	& fun (v Real, v DPin) v
//	& fun (v Real, v Bool) v
//	& fun (v Real, v Char) v
//	& fun (v Real, v Int) v
//	& fun (v Real, v Long) v
//	& fun (v Real, v Real) v
//	& fun (v DPin, v DPin, v DPin) v
//	& fun (v DPin, v DPin, v Bool) v
//	& fun (v DPin, v DPin, v Char) v
//	& fun (v DPin, v DPin, v Int) v
//	& fun (v DPin, v DPin, v Long) v
//	& fun (v DPin, v DPin, v Real) v
//	& fun (v DPin, v Bool, v DPin) v
//	& fun (v DPin, v Bool, v Bool) v
//	& fun (v DPin, v Bool, v Char) v
	& fun (v DPin, v Bool, v Int) v
//	& fun (v DPin, v Bool, v Long) v
//	& fun (v DPin, v Bool, v Real) v
//	& fun (v DPin, v Char, v DPin) v
//	& fun (v DPin, v Char, v Bool) v
//	& fun (v DPin, v Char, v Char) v
//	& fun (v DPin, v Char, v Int) v
//	& fun (v DPin, v Char, v Long) v
//	& fun (v DPin, v Char, v Real) v
//	& fun (v DPin, v Int, v DPin) v
//	& fun (v DPin, v Int, v Bool) v
//	& fun (v DPin, v Int, v Char) v
//	& fun (v DPin, v Int, v Int) v
//	& fun (v DPin, v Int, v Long) v
//	& fun (v DPin, v Int, v Real) v
//	& fun (v DPin, v Long, v DPin) v
//	& fun (v DPin, v Long, v Bool) v
//	& fun (v DPin, v Long, v Char) v
//	& fun (v DPin, v Long, v Int) v
//	& fun (v DPin, v Long, v Long) v
//	& fun (v DPin, v Long, v Real) v
//	& fun (v DPin, v Real, v DPin) v
//	& fun (v DPin, v Real, v Bool) v
//	& fun (v DPin, v Real, v Char) v
//	& fun (v DPin, v Real, v Int) v
//	& fun (v DPin, v Real, v Long) v
//	& fun (v DPin, v Real, v Real) v
//	& fun (v Bool, v DPin, v DPin) v
//	& fun (v Bool, v DPin, v Bool) v
//	& fun (v Bool, v DPin, v Char) v
//	& fun (v Bool, v DPin, v Int) v
//	& fun (v Bool, v DPin, v Long) v
//	& fun (v Bool, v DPin, v Real) v
//	& fun (v Bool, v Bool, v DPin) v
//	& fun (v Bool, v Bool, v Bool) v
//	& fun (v Bool, v Bool, v Char) v
//	& fun (v Bool, v Bool, v Int) v
//	& fun (v Bool, v Bool, v Long) v
//	& fun (v Bool, v Bool, v Real) v
//	& fun (v Bool, v Char, v DPin) v
//	& fun (v Bool, v Char, v Bool) v
//	& fun (v Bool, v Char, v Char) v
//	& fun (v Bool, v Char, v Int) v
//	& fun (v Bool, v Char, v Long) v
//	& fun (v Bool, v Char, v Real) v
//	& fun (v Bool, v Int, v DPin) v
//	& fun (v Bool, v Int, v Bool) v
//	& fun (v Bool, v Int, v Char) v
//	& fun (v Bool, v Int, v Int) v
//	& fun (v Bool, v Int, v Long) v
//	& fun (v Bool, v Int, v Real) v
//	& fun (v Bool, v Long, v DPin) v
//	& fun (v Bool, v Long, v Bool) v
//	& fun (v Bool, v Long, v Char) v
//	& fun (v Bool, v Long, v Int) v
//	& fun (v Bool, v Long, v Long) v
//	& fun (v Bool, v Long, v Real) v
//	& fun (v Bool, v Real, v DPin) v
//	& fun (v Bool, v Real, v Bool) v
//	& fun (v Bool, v Real, v Char) v
//	& fun (v Bool, v Real, v Int) v
//	& fun (v Bool, v Real, v Long) v
//	& fun (v Bool, v Real, v Real) v
//	& fun (v Char, v DPin, v DPin) v
//	& fun (v Char, v DPin, v Bool) v
//	& fun (v Char, v DPin, v Char) v
//	& fun (v Char, v DPin, v Int) v
//	& fun (v Char, v DPin, v Long) v
//	& fun (v Char, v DPin, v Real) v
//	& fun (v Char, v Bool, v DPin) v
//	& fun (v Char, v Bool, v Bool) v
//	& fun (v Char, v Bool, v Char) v
//	& fun (v Char, v Bool, v Int) v
//	& fun (v Char, v Bool, v Long) v
//	& fun (v Char, v Bool, v Real) v
//	& fun (v Char, v Char, v DPin) v
//	& fun (v Char, v Char, v Bool) v
//	& fun (v Char, v Char, v Char) v
//	& fun (v Char, v Char, v Int) v
//	& fun (v Char, v Char, v Long) v
//	& fun (v Char, v Char, v Real) v
//	& fun (v Char, v Int, v DPin) v
//	& fun (v Char, v Int, v Bool) v
//	& fun (v Char, v Int, v Char) v
//	& fun (v Char, v Int, v Int) v
//	& fun (v Char, v Int, v Long) v
//	& fun (v Char, v Int, v Real) v
//	& fun (v Char, v Long, v DPin) v
//	& fun (v Char, v Long, v Bool) v
//	& fun (v Char, v Long, v Char) v
//	& fun (v Char, v Long, v Int) v
//	& fun (v Char, v Long, v Long) v
//	& fun (v Char, v Long, v Real) v
//	& fun (v Char, v Real, v DPin) v
//	& fun (v Char, v Real, v Bool) v
//	& fun (v Char, v Real, v Char) v
//	& fun (v Char, v Real, v Int) v
//	& fun (v Char, v Real, v Long) v
//	& fun (v Char, v Real, v Real) v
//	& fun (v Int, v DPin, v DPin) v
//	& fun (v Int, v DPin, v Bool) v
//	& fun (v Int, v DPin, v Char) v
//	& fun (v Int, v DPin, v Int) v
//	& fun (v Int, v DPin, v Long) v
//	& fun (v Int, v DPin, v Real) v
//	& fun (v Int, v Bool, v DPin) v
//	& fun (v Int, v Bool, v Bool) v
//	& fun (v Int, v Bool, v Char) v
//	& fun (v Int, v Bool, v Int) v
//	& fun (v Int, v Bool, v Long) v
//	& fun (v Int, v Bool, v Real) v
//	& fun (v Int, v Char, v DPin) v
//	& fun (v Int, v Char, v Bool) v
//	& fun (v Int, v Char, v Char) v
//	& fun (v Int, v Char, v Int) v
//	& fun (v Int, v Char, v Long) v
//	& fun (v Int, v Char, v Real) v
//	& fun (v Int, v Int, v DPin) v
//	& fun (v Int, v Int, v Bool) v
//	& fun (v Int, v Int, v Char) v
	& fun (v Int, v Int, v Int) v
//	& fun (v Int, v Int, v Long) v
//	& fun (v Int, v Int, v Real) v
//	& fun (v Int, v Long, v DPin) v
//	& fun (v Int, v Long, v Bool) v
//	& fun (v Int, v Long, v Char) v
//	& fun (v Int, v Long, v Int) v
//	& fun (v Int, v Long, v Long) v
//	& fun (v Int, v Long, v Real) v
//	& fun (v Int, v Real, v DPin) v
//	& fun (v Int, v Real, v Bool) v
//	& fun (v Int, v Real, v Char) v
//	& fun (v Int, v Real, v Int) v
//	& fun (v Int, v Real, v Long) v
//	& fun (v Int, v Real, v Real) v
//	& fun (v Long, v DPin, v DPin) v
//	& fun (v Long, v DPin, v Bool) v
//	& fun (v Long, v DPin, v Char) v
//	& fun (v Long, v DPin, v Int) v
//	& fun (v Long, v DPin, v Long) v
//	& fun (v Long, v DPin, v Real) v
//	& fun (v Long, v Bool, v DPin) v
//	& fun (v Long, v Bool, v Bool) v
//	& fun (v Long, v Bool, v Char) v
//	& fun (v Long, v Bool, v Int) v
//	& fun (v Long, v Bool, v Long) v
//	& fun (v Long, v Bool, v Real) v
//	& fun (v Long, v Char, v DPin) v
//	& fun (v Long, v Char, v Bool) v
//	& fun (v Long, v Char, v Char) v
//	& fun (v Long, v Char, v Int) v
//	& fun (v Long, v Char, v Long) v
//	& fun (v Long, v Char, v Real) v
//	& fun (v Long, v Int, v DPin) v
//	& fun (v Long, v Int, v Bool) v
//	& fun (v Long, v Int, v Char) v
//	& fun (v Long, v Int, v Int) v
//	& fun (v Long, v Int, v Long) v
//	& fun (v Long, v Int, v Real) v
//	& fun (v Long, v Long, v DPin) v
//	& fun (v Long, v Long, v Bool) v
//	& fun (v Long, v Long, v Char) v
//	& fun (v Long, v Long, v Int) v
//	& fun (v Long, v Long, v Long) v
//	& fun (v Long, v Long, v Real) v
//	& fun (v Long, v Real, v DPin) v
//	& fun (v Long, v Real, v Bool) v
//	& fun (v Long, v Real, v Char) v
//	& fun (v Long, v Real, v Int) v
//	& fun (v Long, v Real, v Long) v
//	& fun (v Long, v Real, v Real) v
//	& fun (v Real, v DPin, v DPin) v
//	& fun (v Real, v DPin, v Bool) v
//	& fun (v Real, v DPin, v Char) v
//	& fun (v Real, v DPin, v Int) v
//	& fun (v Real, v DPin, v Long) v
//	& fun (v Real, v DPin, v Real) v
//	& fun (v Real, v Bool, v DPin) v
//	& fun (v Real, v Bool, v Bool) v
//	& fun (v Real, v Bool, v Char) v
//	& fun (v Real, v Bool, v Int) v
//	& fun (v Real, v Bool, v Long) v
//	& fun (v Real, v Bool, v Real) v
//	& fun (v Real, v Char, v DPin) v
//	& fun (v Real, v Char, v Bool) v
//	& fun (v Real, v Char, v Char) v
//	& fun (v Real, v Char, v Int) v
//	& fun (v Real, v Char, v Long) v
//	& fun (v Real, v Char, v Real) v
//	& fun (v Real, v Int, v DPin) v
//	& fun (v Real, v Int, v Bool) v
//	& fun (v Real, v Int, v Char) v
//	& fun (v Real, v Int, v Int) v
//	& fun (v Real, v Int, v Long) v
//	& fun (v Real, v Int, v Real) v
//	& fun (v Real, v Long, v DPin) v
//	& fun (v Real, v Long, v Bool) v
//	& fun (v Real, v Long, v Char) v
//	& fun (v Real, v Long, v Int) v
//	& fun (v Real, v Long, v Long) v
//	& fun (v Real, v Long, v Real) v
//	& fun (v Real, v Real, v DPin) v
//	& fun (v Real, v Real, v Bool) v
//	& fun (v Real, v Real, v Char) v
//	& fun (v Real, v Real, v Int) v
//	& fun (v Real, v Real, v Long) v
//	& fun (v Real, v Real, v Real) v
