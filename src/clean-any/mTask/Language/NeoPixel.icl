implementation module mTask.Language.NeoPixel

import Text.GenJSON

import mTask.Language

derive class iTask NeoPixel, NeoInfo, NeoPixelType, NeoPixelFreq

instance toString NeoInfo where toString a = toSingleLineText a
