definition module mTask.Language.Sound

/**
 * Sound detector.
 * The supported sound detector work just like regular GPIOs so this file only contains convenience macros.
 *
 * Currently supported:
 *   - Sparkfun SEN-12642
 */

import mTask.Language

//** Type synonym for a sound detector
:: SoundDetector :== (DPin, APin)

/**
 * The class representing all sound detectors
 *
 * @var view
 */
class SoundDetector v | tupl, expr, pinMode v & dio DPin v
where
	/**
	 * Constructor for the sound detector.
	 * It just sets the pinmode of the sound presence pin to {{PMInput}}.
	 *
	 * @param sound presence pin
	 * @param sound level pin
	 * @param function doing something with the sound sensor
	 * @result {{Main}} program
	 */
	soundDetector :: DPin APin ((v SoundDetector) -> Main (v b)) -> Main (v b) | type b & expr, step, pinMode v
	soundDetector dp ap def :== declarePin dp PMInput \dp->def (tupl dp (lit ap))

	/**
	 * Measure the sound presence with a specified polling rate.
	 *
	 * @param polling rate
	 * @param sound detector
	 * @result task yielding readings as an unstable value
	 */
	soundPresence` :: (TimingInterval v) (v SoundDetector) -> MTask v Bool
	soundPresence` i p :== readD` i (first p)

	/**
	 * Measure the sound presence.
	 *
	 * @param sound detector
	 * @result task yielding readings as an unstable value
	 */
	soundPresence :: (v SoundDetector) -> MTask v Bool | tupl v & dio DPin v
	soundPresence p :== readD (first p)

	/**
	 * Measure the sound level with a specified polling rate.
	 *
	 * @param polling rate
	 * @param sound detector
	 * @result task yielding readings as an unstable value
	 */
	soundLevel` :: (TimingInterval v) (v SoundDetector) -> MTask v Bool | aio, tupl v
	soundLevel` i p :== readA` i (second p)

	/**
	 * Measure the sound level.
	 *
	 * @param sound detector
	 * @result task yielding readings as an unstable value
	 */
	soundLevel :: (v SoundDetector) -> MTask v Bool | tupl, aio v
	soundLevel p :== readA (second p)
