definition module mTask.Language.Interrupts

/**
 * Hardware interrupts
 */
import mTask.Language

//** The type of interrupt
:: InterruptMode
  = Change
  | Rising
  | Falling
  | Low
  | High

/**
 * Helper macro for lifted {{Change}}
 *
 * @type (v InterruptMode)
 */
change :== lit Change
/**
 * Helper macro for lifted {{Rising}}
 *
 * @type (v InterruptMode)
 */
rising :== lit Rising
/**
 * Helper macro for lifted {{Falling}}
 *
 * @type (v InterruptMode)
 */
falling :== lit Falling
/**
 * Helper macro for lifted {{Low}}
 *
 * @type (v InterruptMode)
 */
low :== lit Low
/**
 * Helper macro for lifted {{High}}
 * 
 * @type (v InterruptMode)
 */
high :== lit High

/**
 * The class representing the interrupt tasks
 *
 * @var view
 * @param interrupt mode
 * @param pin to watch for the interrupt
 * @result a task yielding no value until the interrupt fires, after which is yields a stable pin state
 * @throws {{MTEUnsupportedInterrupt}} in the interpreter if the pin doesn't support that interrupt
 */
class interrupt v :: (v InterruptMode) (v p) -> MTask v Bool | pin p

instance toString InterruptMode
derive class iTask InterruptMode
derive gDefault InterruptMode
