definition module mTask.Language.Long

/**
 * Longs represent 32-bit integers on microprocessors
 */

import mTask.Language

from StdOverloaded import class +, class -, class zero, class one, class *, class /, class <, class toInt, class ~

from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

//** Datatype for holding a Long
:: Long = Long Int

/**
 * Maximal value of a Long
 *
 * @type Long
 */
LONG_MAX :== Long  0x7fffffff
/**
 * Minimal value of a Long
 *
 * @type Long
 */
LONG_MIN :== Long -0x7fffffff

instance zero Long
instance one Long
instance ~ Long
instance + Long
instance - Long
instance * Long
instance / Long
instance < Long
instance == Long
instance toString Long
instance toInt Long
instance toReal Long
derive class iTask Long
derive gDefault Long
instance basicType Long

/**
 * Convert something to a long in mTask
 *
 * @var view
 * @param thing to convert to a long
 * @result a long
 */
class long v a :: (v a) -> v Long
