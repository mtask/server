implementation module mTask.Simulate.TraceTask

/*
	Pieter Koopman & Mart Lubbers
	Radboud University Nijmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language
import iTasks
import StdEnv, StdDebug
import Data.Func
import Data.Functor
import Data.Maybe
import Data.Either
import Data.Tuple
import mTask.Interpret.ByteCodeEncoding

import mTask.Language.LCD

fromDynamic :: Dynamic -> a | TC a
fromDynamic (x::a^) = x
fromDynamic _ = abort "fromDynamic failed"

derive class iTask TraceTask, TT, ObjectValue, MPS, Main
instance == (TaskValue a) | gEq{|*|} a where (==) x y = x === y
instance + String where (+) l r = l +++ r

// === Tooling ===

instance Functor TT where
	fmap f (TT tt) = TT (fmap (\r.{r & tskVl = f r.tskVl, nTask = fmap f (TT tt)}) tt)

(>>~-) infixl 1 :: (TT a) (a->TT b) -> TT b | iTask a & iTask b
(>>~-) (TT x) f = TT (x >>~ \r. unTT (f r.tskVl))

(>?|-) infixl 1 :: (TT a) (TT b) -> TT b | iTask a & iTask b
(>?|-) (TT x) (TT y) = TT (x >?| y)

toTT :: (Task a) -> TT a | type a
toTT t = TT (t >>~ \x.return {trace = [toString x], tskVl = x, nTask = toTT t})

// === Interface ===

simulate :: (Main (TT a)) -> Task (TraceTask a) | type a
simulate {main = task} = 
	set mps0 sharedMPS >?|
	set [] sharedObjectValue >?|
	run task task -|| mpsTask -|| handleObjects


handleObjects :: Task ()
handleObjects =
  enterChoiceWithShared [ChooseFromGrid toObjectDisplay] sharedObjectValue <<@ Title "MTask objects" >>* 
  [OnAction (Action "Edit") (hasValue \o.handleObject o >-| handleObjects)]

handleObject :: ObjectValue -> Task [ObjectValue]
handleObject obj=:{oValue=d::Real} = handleReal d obj
handleObject obj=:{oValue=d::Int}  = handleInt  d obj
handleObject obj=:{oValue=d::Bool} = handleBool d obj
handleObject obj=:{oValue=d::LCDObject}        = handleLCD  d obj
handleObject obj=:{oValue=d::DHTObject}        = handleDHT d obj
handleObject obj=:{oValue=d::AirQualityObject} = handleAQS d obj
handleObject obj=:{oValue=d::GestureObject}    = handleGes d obj
handleObject obj=:{oValue=d::LightObject}      = handleLight d obj
handleObject obj=:{oValue=(LiftedSDS d)::LiftedSDS} = handleSds d obj >?| get sharedObjectValue
handleObject obj=:{oValue=d::a->b} = viewInformation [] "no editor for functions" <<@ Title obj.oType >?| get sharedObjectValue
handleObject obj = viewInformation [] "obj" <<@ Title obj.oType >?| get sharedObjectValue

handleAQS :: AirQualityObject ObjectValue -> Task [ObjectValue]
handleAQS aqs obj
	=        updateInformation [] aqs <<@ Title (obj.oType + " " + toString obj.oId)
	>>? \new.upd (updateAt obj.oId {obj & oValue = dynamic new}) sharedObjectValue

handleGes :: GestureObject ObjectValue -> Task [ObjectValue]
handleGes ges obj
	=        updateInformation [] ges <<@ Title (obj.oType + " " + toString obj.oId)
	>>? \new.upd (updateAt obj.oId {obj & oValue = dynamic new}) sharedObjectValue

handleLight :: LightObject ObjectValue -> Task [ObjectValue]
handleLight light obj
	=        updateInformation [] light <<@ Title (obj.oType + " " + toString obj.oId)
	>>? \new.upd (updateAt obj.oId {obj & oValue = dynamic new}) sharedObjectValue

handleDHT :: DHTObject ObjectValue -> Task [ObjectValue]
handleDHT dht obj =
  updateInformation  [] dht <<@ Title (obj.oType + " " + toString obj.oId) >>? \new.
  upd (updateAt obj.oId {obj & oValue = dynamic new}) sharedObjectValue

handleLCD :: LCDObject ObjectValue -> Task [ObjectValue]
handleLCD lcd obj =
  viewInformation [] lcd.lcdtxt <<@ Title (obj.oType + " " + toString obj.oId) >>*
  [ OnAction (Action "Clear") (always (upd (updateAt obj.oId {obj & oValue = dynamic {lcd & lcdtxt = repeatn lcd.sizeH (toString (repeatn lcd.sizeW '.'))}}) sharedObjectValue))
  , OnAction (Action "Right") (always (updateAPin A0 25 >?| get sharedObjectValue))
  , OnAction (Action "Up") (always (updateAPin A0 100 >?| get sharedObjectValue))
  , OnAction (Action "Down") (always (updateAPin A0 250 >?| get sharedObjectValue))
  , OnAction (Action "Left") (always (updateAPin A0 450 >?| get sharedObjectValue))
  , OnAction (Action "Select") (always (updateAPin A0 700 >?| get sharedObjectValue))
  , OnAction (Action "No button") (always (updateAPin A0 1000 >?| get sharedObjectValue))
  ]

handleSds :: (sds () t t) ObjectValue -> Task t | RWShared sds & iTask t
handleSds sds obj =
	updateSharedInformation [] sds <<@ Title (obj.oType + " " + toString obj.oId)

handleReal :: Real ObjectValue -> Task [ObjectValue]
handleReal real obj =
  updateInformation [] real <<@ Title (obj.oType + " " + toString obj.oId) >>? \new.
  upd (updateAt obj.oId {obj & oValue = dynamic new}) sharedObjectValue

handleInt :: Int ObjectValue -> Task [ObjectValue]
handleInt int obj =
  updateInformation [] int <<@ Title (obj.oType + " " + toString obj.oId) >>? \new.
  upd (updateAt obj.oId {obj & oValue = dynamic new}) sharedObjectValue

handleBool :: Bool ObjectValue -> Task [ObjectValue]
handleBool bool obj =
  updateInformation [] bool <<@ Title (obj.oType + " " + toString obj.oId) >>? \new.
  upd (updateAt obj.oId {obj & oValue = dynamic new}) sharedObjectValue

mpsTask :: Task MPS
mpsTask =
	updateSharedInformation [] sharedMPS <<@ Title "MicroProcessor State" >>*
	[OnAction (Action "Step clock") (always (upd (\mps.{mps & clock = mps.clock + mps.interval}) sharedMPS >?| mpsTask))
	]
/*
	where
		sound {dpins, apins, interval, clock} =
			interval > 0 &&
			clock >= 0 &&
			isUnique (map fst dpins) &&
			isUnique (map fst apins) &&
			and [0 <= i && i < 1024 \\ (p,i) <- apins]
*/
run :: (TT a) (TT a) -> Task (TraceTask a) | type a
run (TT task) tt0 =
	task >>? \{trace, tskVl, nTask}.
	Title "Task: trace & result " @>> ((
	viewInformation [] trace -&&- viewInformation [ViewAs toString] tskVl) <<@ ArrangeHorizontal >>* 
	[OnAction (Action "Restart") (always (set mps0 sharedMPS >?| set [] sharedObjectValue >?| run tt0 tt0))
	,OnAction (Action "Step task")  (always (upd (\mps.{mps & clock = mps.clock + mps.interval}) sharedMPS >?| run nTask tt0))
    ])

// === MTask DSL ===

instance expr TT where
	lit x = TT (return {trace = [toString x], tskVl = x, nTask = lit x})
	(+.) x y = binopT "+." (+) x y
	(-.) x y = binopT "-." (-) x y
	(*.) x y = binopT "*." (*) x y
	(/.) x y = binopT "/." (/) x y
	(&.) x y = binopT "&." (&&) x y
	(|.) x y = binopT "|." (||) x y
	Not  x   = sinopT "Not" not x
	(==.) x y = binopT "==." (==) x y
	(!=.) x y = binopT "!=." (<>) x y
	(<.)  x y = binopT "<." (<) x y
	(>.)  x y = binopT ">." (>) x y
	(<=.) x y = binopT "<=." (<=) x y
	(>=.) x y = binopT ">=." (>=) x y
	If (TT ct) tt et =
		TT (ct >>~ \bv.
			if bv.tskVl
				(unTT tt >>~ \r. return {r & trace = trace3 "If" bv.trace r.trace ["(..)"]})
				(unTT et >>~ \r. return {r & trace = trace3 "If" bv.trace ["(..)"] r.trace})
		   )
instance tupl TT where
	first x = sinopT "fst" fst x
	second x = sinopT "snd" snd x
	tupl x y = binopT "(,)" tuple x y

//litT x = TT (return {trace = [toString x], tskVl = x, nTask = lit x})

binopT :: String (a b -> c) (TT a) (TT b) -> TT c | iTask a & iTask b & type c 
binopT name op (TT x) (TT y) =
 TT (x >>~ \x=:{tskVl = xv}. y >>~ \y=:{tskVl = yv}. return
  let v = op xv yv in
  { trace = trace2i name x.trace y.trace
  , tskVl = v
  , nTask = lit v
  } )

sinopT :: String (a -> c) (TT a) -> TT c | iTask a & type c
sinopT name op (TT x) =
 TT (x >>~ \x=:{tskVl = xv}.
   let v = op xv in
     return
       { trace = trace1 name x.trace
       , tskVl = v
       , nTask = lit v
       })

instance aio TT where
	readA` _ (TT pt) =
		TT (pt >>~ \{tskVl = p, trace = ptt}. 
			readAPin p >>~ \i. 
		    return
				{ trace = trace2 "readA" ptt ["|" + toString i + "|"]
				, tskVl = Value i False
				, nTask = readA (TT pt)
				}
		    )
	writeA (TT pt) (TT it) = 
		TT (pt >>~ \{tskVl = p, trace = ptt}.
		    it >>~ \{tskVl = i, trace = itt}.
		    upd (\mps.{mps & apins = updatePin p i mps.apins}) sharedMPS >?|
		    return
		    	{ trace = trace2 "writeA" ptt itt
		    	, tskVl = Value i True
		    	, nTask = rtrn (lit i)
		    	})

instance dio APin TT where
	readD` _ (TT pt) =
		TT (pt >>~ \{tskVl = p, trace = ptt}. 
			readAPin p >>~ \i. 
		     let b = i <> 0 in 
		     return
		     	{ trace = trace2 "readD " ptt  ["|" + toString b + "|"]
		     	, tskVl = Value b False
		     	, nTask = readD (TT pt)
		     	}
		    )
	writeD (TT pt) (TT bt) = 
		TT (pt >>~ \{tskVl = p, trace = ptt}.
		    bt >>~ \{tskVl = b, trace = btt}.
		    let i = if b 1 0 in
		    upd (\mps.{mps & apins = updatePin p i mps.apins}) sharedMPS >?|
		    return
		    	{ trace = trace2 "writeD " ptt btt
		    	, tskVl = Value b True
		    	, nTask = rtrn (lit b)
		    	})
instance dio DPin TT where
	readD` _ (TT pt) =
		TT (pt >>~ \{tskVl = p, trace = ptt}. 
			readDPin p >>~ \b. 
			return
				{ trace = trace2 "readD " ptt  ["|" + toString b + "|"]
				, tskVl = Value b False
				, nTask = readD (TT pt)
				}
		    )
	writeD (TT pt) (TT bt) = 
		TT (pt >>~ \{tskVl = p, trace = ptt}.
		    bt >>~ \{tskVl = b, trace = btt}.
		    upd (\mps.{mps & dpins = updatePin p b mps.dpins}) sharedMPS >?|
		    return
		    	{ trace = trace2 "writeD" ptt btt
		    	, tskVl = Value b True
		    	, nTask = rtrn (lit b)
		    	})
instance pinMode TT where
	pinMode (TT mode) (TT pin) =
		TT (mode >>~ \modetval->pin >>~ \pintval->return
				{ trace = trace2 "pinMode" modetval.trace pintval.trace
				, tskVl = Value () True
				, nTask = rtrn (lit ())
				})
				
instance interrupt TT where
	interrupt _ _ = abort "Interrupts not implemented"

instance rtrn TT where
	rtrn (TT vt) =
		TT (vt >>~ \{tskVl = v, trace = tr}.
			return
				{ trace = trace1 "rtrn" tr
				, tskVl = Value v True
				, nTask = rtrn (lit v)
				}
			)

instance unstable TT where
	unstable (TT vt) =
		TT (vt >>~ \{tskVl = v, trace = tr}.
			return
				{ trace = trace1 "rtrn" tr
				, tskVl = Value v False
				, nTask = unstable (lit v)
				}
			)

instance rpeat TT where
	rpeatEvery _ t = repeatTask t t

repeatTask :: (MTask TT a) (MTask TT a) -> MTask TT a | type a
repeatTask (TT t) tt =
	TT (t >>~ \v.case v.tskVl of
		Value a True = return {v & tskVl = Value a False, nTask = repeatTask tt tt}
		_ = return {v & nTask = repeatTask v.nTask tt}
	   )
//	rpeat tt = addTraceTT ["repeat "] tt >>.. rpeat tt // does not produce the correct intermediate results

instance .&&. TT where
  (.&&.) (TT xt) (TT yt) =
	TT (xt >>~ \x.
		yt >>~ \y.
		let trace = trace2i ".&&." x.trace y.trace in
		case (x.tskVl, y.tskVl) of
		  (Value xv sx, Value yv sb) =
		  	let stable = sx && sb in
			return {trace = trace,tskVl = Value (xv, yv) stable
				   ,nTask = if stable (rtrn (lit (xv, yv))) (x.nTask .&&. y.nTask)
				   }
		  _ = return {trace = trace, tskVl = NoValue, nTask = x.nTask .&&. y.nTask})


instance .||. TT where
  (.||.) (TT xt) (TT yt) =
	TT (xt >>~ \x.
		yt >>~ \y.
		let trace = trace2i ".||." x.trace y.trace in
		case x.tskVl of
		  Value xv True =
		    return {trace = trace, tskVl = x.tskVl, nTask = rtrn (lit xv)}
		  _ = case y.tskVl of
			    Value yv True =
				    return {trace = trace, tskVl = y.tskVl, nTask = rtrn (lit yv)}
			    _ = return {trace = trace, tskVl = NoValue, nTask = x.nTask .||. y.nTask})

instance step TT
where
  (>>*.) (TT xt) steps =
	TT (xt >>- \x. let result task = return {trace = indent x.trace ++ [">>=*. .."], tskVl = NoValue, nTask = task} in
	  case x.tskVl of
		Value a True = result (matchSteps x.tskVl steps [toString x.tskVl] (x.nTask >>*. steps))
		_            = result (x.nTask >>*. steps))
  (>>=.) ma=:(TT at) amb =
	TT (at >>- \av. let result task = return {trace = indent av.trace ++ [">>=. .."], tskVl = NoValue, nTask = task} in
	  case av.tskVl of
	  Value a True = result (amb (lit a))
	  _ = result (av.nTask >>=. amb))
  (>>|.) ma=:(TT at) mb =
	TT (at >>- \av. let result task = return {trace = indent av.trace ++ [">>|. .."], tskVl = NoValue, nTask = task} in
	  case av.tskVl of
	  Value a True = result mb
	  _ = result (av.nTask >>|. mb))
  (>>~.) ma=:(TT at) amb =
	TT (at >>- \av. let result task = return {trace = indent av.trace ++ [">>~. .."], tskVl = NoValue, nTask = task} in
	  case av.tskVl of
	  Value a s = result (amb (lit a))
	  _ = result (av.nTask >>~. amb))
  (>>..) ma=:(TT at) mb =
	TT (at >>- \av. let result task = return {trace = indent av.trace ++ [">>.. .."], tskVl = NoValue, nTask = task} in
	  case av.tskVl of
	  Value a True = result mb
	  _ = result (av.nTask >>.. mb))

matchSteps :: (TaskValue t) [Step TT t u] [String] (MTask TT u) -> MTask TT u | type t & type u
matchSteps val=:(Value v _) [IfValue p t:r] lTrace default = 
  TT ((unTT (p (lit v))) >>~ \{tskVl = b,trace}.
	if b	(addTrace (indent lTrace ++ trace1 ">>*. IfValue" trace) (unTT (t (lit v))))
			(unTT (matchSteps val r lTrace default)))
matchSteps val=:(Value v True) [IfStable p t:r] lTrace default = 
  TT ((unTT (p (lit v))) >>~ \{tskVl = b}.
	if b	(addTrace (indent lTrace ++ [">>*. IfStable"]) (unTT (t (lit v))))
			(unTT (matchSteps val r lTrace default)))
matchSteps val=:(Value v False) [IfUnstable p t:r] lTrace default = 
  TT ((unTT (p (lit v))) >>~ \{tskVl = b}.
	if b	(addTrace (indent lTrace ++ [">>*. IfUnstable"]) (unTT (t (lit v))))
			(unTT (matchSteps val r lTrace default)))
matchSteps NoValue [IfNoValue t:r] lTrace default = TT (addTrace (lTrace ++ [">>*. IfNoValue"]) (unTT t))
matchSteps val     [Always    t:r] lTrace default = TT (addTrace (lTrace ++ [">>*. Always"]) (unTT t))
matchSteps val     [step       :r] lTrace default = matchSteps val r lTrace default
matchSteps val     []              lTrace default = default

instance delay TT where
	delay tt =
		long tt >>~- \(Long delayValue).
		TT (get sharedMPS >>~ \mps.
			delayUntil (delayValue + mps.clock) [] delayValue)

delayUntil :: Int [String] Int -> Task (MTask TraceTask Long)
delayUntil startTime trace d
	= get sharedMPS >>~ \mps.
		let rest = Long $ mps.clock - startTime in
		if (rest < zero)
			(return {trace = trace ++ ["delayUntil " + toString startTime], tskVl = Value rest False, nTask = TT (delayUntil startTime [] d)})
		  	(return {trace = trace ++ ["delay " + toString d + " returns " + toString rest], tskVl = Value rest True, nTask = rtrn (lit rest)})

instance fun () TT where
	fun def =
		{main =
			(toTT freshOId >>~- \i.
			let (fun In body) = def (\a.addTraceTT ["fun"+toString i+" () ->"] ((fun a)))
			in  toTT (get sharedObjectValue) >>~- \objects. 
				fun () >>~- \exp.
				toTT (set (objects ++ [{oId = i, oType = "Fun ()->" + type2string exp , oValue = dynamic fun}]) sharedObjectValue) >?|-
				body.main				
		   )
		}

instance fun a TT | type a where
  fun def =
	{main =
	  let (fun In main) = def fun
	  in  toTT freshOId >>~- \i.
		  toTT (upd (\list->list ++ [{oId = i, oType = "Fun", oValue = dynamic fun}]) 
		  			sharedObjectValue ) >?|- 
		  main.main
	}

instance fun (TT a) TT | type a where
  fun def =
	{main =
	  toTT freshOId >>~- \i.
	  let (fun In main) = def \(TT a).TT (a >>~ \av.addTrace ["fun"+toString i+" "+toString av.tskVl+" ->"] (unTT (fun (TT (return av)))))
		  v = gDefault{|*|}
	  in  fun (lit v) >>~- \res.
		  toTT (upd (\list->list ++ [{oId = i, oType = "Fun " + type2string v + "->" + type2string res, oValue = dynamic fun}]) sharedObjectValue ) >?|- 
		  main.main
	}
instance fun (TT a, TT b) TT | type a & type b where
	fun def =
		{main =
			(toTT freshOId >>~- \i.
			let (fun In main) = 
					def (\(TT a,TT b).TT (a >>~ \av. b >>- \bv.
					addTrace ["fun"+toString i+" ("+toString av.tskVl+","+toString bv.tskVl+") ->"] 
					(unTT (fun (TT (return av), TT (return bv))))))
				a = gDefault{|*|}
				b = gDefault{|*|}
			in toTT (get sharedObjectValue) >>~- \objects.
				fun (lit a, lit b) >>~- \exp.
				toTT (set (objects ++ [{oId = i, oType = "Fun (" + type2string a + "," + type2string b + ")->" + type2string exp, oValue = dynamic fun}]) sharedObjectValue) >?|-
				main.main				
		   )
		}

instance fun (TT a, TT b, TT c) TT | type a & type b & type c where
	fun def =
		{main =
			(toTT freshOId >>~- \i.
			let (fun In body) =
					def (\(TT a,TT b,TT c).
						TT (a >>~ \av. b >>- \bv. c >>~ \cv.
						addTrace ["fun"+toString i+" ("+toString av.tskVl+","+toString bv.tskVl+","+toString cv.tskVl+") ->"]
						(unTT (fun (TT (return av), TT (return bv), TT (return cv))))))
				a = gDefault{|*|}
				b = gDefault{|*|}
				c = gDefault{|*|}
			in  toTT (get sharedObjectValue) >>~- \objects.
				fun (lit a, lit b, lit c) >>~- \exp.
				toTT (set (objects ++ [{oId = i, oType = "Fun (" + type2string a + "," + type2string b  + "," + type2string c + ")->" + type2string exp, oValue = dynamic fun}]) sharedObjectValue) >?|-
				body.main				
		   )
		}

k3 :: a a b -> b
k3 x y z = z

toByteCode{|ObjectValue|} _ = ""
instance toByteWidth [ObjectValue] where toByteWidth _ = 0
fromByteCode{|ObjectValue|} = fail "Not implemented"

addTraceTT :: [String] (TT a) -> TT a | type a
addTraceTT trace x = TT (unTT x >>~ \r. return {r & trace = trace ++ r.trace})

setTrace :: [String] (Task (TraceTask a)) -> (Task (TraceTask a)) | type a
setTrace trace x =  x >>~ \r.return {r & trace = trace}

addTrace :: [String] (Task (TraceTask a)) -> (Task (TraceTask a)) | type a
addTrace trace x =  x >>~ \r.return {r & trace = trace ++ r.trace}

instance toString ObjectValue where toString _ = "ObjectValue"
instance toChar ObjectValue where toChar _ = 'O'

instance liftsds TT where
	liftsds def =
		{main =
			toTT freshOId >>~- \i.
			let
				sds = TT (return {trace = [sdsName i], tskVl = Sds i, nTask = sds}) 
				(tt In e) = def sds
			in  toTT (get tt >>~ \t->upd (\l.l ++ [{oId = i, oType = "Lifted SDS " + type2string t, oValue = dynamic (LiftedSDS tt)}]) sharedObjectValue) >?|- e.main
		}

instance sds TT where
	sds def =
		{main =
			toTT freshOId >>~- \i.
			let
				sds = TT (return {trace = [sdsName i], tskVl = Sds i, nTask = sds}) 
				(t In e) = def sds
			in  toTT (upd (\l.l ++ [{oId = i, oType = "SDS " + type2string t, oValue = dynamic t}]) sharedObjectValue) >?|- e.main
		}
	getSds` _ (TT sdst) =
		TT ( sdst >>~ \sds=:{tskVl = Sds i}.
			 get sharedObjectValue >>~ \sdss.getSdsDyn sdst i (sdss !! i).oValue
		   )
	setSds (TT sdst) (TT vt) =
		TT ( sdst >>~ \{tskVl = Sds i}.
			 vt >>~ \v.
			 get sharedObjectValue >>~ \sdss.
			 setSdsDyn i v (sdss !! i).oValue
		   )
//	updSds sdst fun = getSds sdst >>=. setSds sdst
	updSds (TT sdst) fun = TT (sdst >>? \{tskVl = Sds i}.
				    get sharedObjectValue
				>>? \sdss.getSdsDyn sdst i (sdss !! i).oValue
				>>? \{tskVl = Value v _}.unTT (fun $ TT (return {trace=["x"],tskVl=v,nTask=lit v}))
				>>? \v.setTrace
					["updSds " + sdsName i + " (\\x->":v.trace ++ [")"]]
					(setSdsDyn i v (sdss !! i).oValue))

setSdsDyn :: Int (TraceTask a) Dynamic -> Task (TraceTask (TaskValue a)) | type a
setSdsDyn i v (w::a^) =
	upd (applyAt i (\sds.{sds & oValue = dynamic v.tskVl})) sharedObjectValue >?|
	return
		{ trace = ["setSds " + sdsName i: v.trace]
		, tskVl = Value v.tskVl True
		, nTask = rtrn (lit v.tskVl)
		} 
setSdsDyn i v (LiftedSDS sds::LiftedSDS) =
	setShare (dynamic v.tskVl) sds >-| return
		{ trace = ["setLiftedSds " + sdsName i:v.trace]
		, tskVl = Value v.tskVl True
		, nTask = rtrn (lit v.tskVl)
		}
where
	setShare :: Dynamic (sds () t t) -> Task t | RWShared sds & iTask t
	setShare (v::t^) sds = set v sds
	setShare _ _ = throw "setSdsDyn type mismatch"
setSdsDyn i v _ = throw "setSdsDyn doesn't match"

getSdsDyn :: (Task (TraceTask (Sds a))) Int Dynamic -> Task (TraceTask (TaskValue a)) | type a
getSdsDyn sdstask i (v::a^) =
	return
		{ trace = ["getSds " + sdsName i]
		, tskVl = Value v False
		, nTask = getSds (TT sdstask)
		} 
getSdsDyn sdstask i (LiftedSDS sds::LiftedSDS) =
	get sds >>- \v.case dynamic v of
		(v :: a^) = return
			{ trace = ["getLiftedSds " + sdsName i]
			, tskVl = Value v False
			, nTask = getSds (TT sdstask)
			}
		_ = throw "getSdsDyn type mismatch"
getSdsDyn sdstask i v = throw "getSdsDyn doesn't match"

instance dht TT where
  DHT :: DHTInfo ((TT DHT)->Main (TT b)) -> Main (TT b) | type b
  DHT addr def = 
	{main =
		toTT freshOId >>~- \i.
		toTT (upd (\l.l ++ [mkDht i]) sharedObjectValue) >?|-
		let dhtTask = TT (return {trace = [dhtName i], tskVl = Dht i, nTask = dhtTask})
		in (def dhtTask).main
	}
  temperature` _ (TT dhtTask) = 
	TT (dhtTask >>~ \{tskVl = Dht i, trace}.
		get sharedObjectValue >>~ \sdss.
		return	{ trace = trace1 "temperature" trace
			 	, tskVl = Value (fromDynamic (sdss !! i).oValue).temperature False
			 	, nTask = temperature (TT dhtTask)
			 	} 
	   )
  humidity` _ (TT dhtTask) =
	TT (dhtTask >>~ \{tskVl = Dht i,trace}.
		get sharedObjectValue >>~ \sdss.
		return	{ trace = trace1 "humidity" trace
			 	, tskVl = Value (fromDynamic (sdss !! i).oValue).humidity False
			 	, nTask = humidity (TT dhtTask)
			 	}
	   )
mkDht i = {oId = i, oType = dhtName i, oValue = dynamic dht0}
dht0 = {temperature=42.0, humidity=70.0}

instance LEDMatrix TT where
	ledmatrix _ def = abort "LEDMatrix not implemented"
	LMDot _ _ _ _ = abort "LEDMatrix not implemented"
	LMIntensity _ _ = abort "LEDMatrix not implemented"
	LMClear _ = abort "LEDMatrix not implemented"
	LMDisplay _ = abort "LEDMatrix not implemented"

// format this like dht
instance LightSensor TT where
	lightsensor addr def = 
		{ main =
			   toTT freshOId >>~- \i.
				let
					sens = LightSensor i
					sensTask = TT (return
							{ trace = ["lightSensor_" + toString i]
							, tskVl = sens
							, nTask = sensTask
							})
				in toTT (upd (\l.l ++ [
							{ oId = i
							, oType = toSingleLineText i
							, oValue = dynamic {light = 0.0}
							}]) sharedObjectValue >?|
			       		return i
			       		) >?|-
			   (def sensTask).main
		}	
	light` _ tt=:(TT task) =
		TT ( task >>~ \{tskVl = LightSensor i}.
			 get sharedObjectValue >>~ \sdss.
			 return
			 	{ trace = ["light_" + toString i]
			 	, tskVl = Value (fromDynamic (sdss !! i).oValue).light False
			 	, nTask = light tt
			 	}
		   )

instance GestureSensor TT where
	gestureSensor addr def = 
		{ main =
			   toTT freshOId >>~- \i.
				let
					ges = GestureSensor i
					gesTask = TT (return
							{ trace = ["gesture "+toString i]
							, tskVl = ges
							, nTask = gesTask
							})
				in toTT (upd (\l.l ++ [
							{ oId = i
							, oType = toSingleLineText i
							, oValue = dynamic {gesture = GNone}
							}]) sharedObjectValue >?|
			       		return i
			       		) >?|-
			   (def gesTask).main
		}			
	gesture` _ tt=:(TT task) =
		TT ( task >>~ \{tskVl = GestureSensor i}.
			 get sharedObjectValue >>~ \sdss.
			 return
			 	{ trace = ["gesture " + toString i]
			 	, tskVl = Value (fromDynamic (sdss !! i).oValue).gesture False
			 	, nTask = gesture tt
			 	}
		   )

instance AirQualitySensor TT where
	airqualitysensor addr def =
		{main =
				toTT freshOId >>~- \i.
				let
					aqs = AirQualitySensor i
					aqsTask = TT (return
						{ trace = ["aqs"+toString i]
						, tskVl = aqs
						, nTask = aqsTask
						})
				in toTT (upd (\l.l ++ [
							{ oId = i
							, oType = toSingleLineText i
							, oValue = dynamic {environment=(0.0, 0.0), co2=38, tvoc=42}
							}]) sharedObjectValue >?|
			       		return i
			       		) >?|-
			   (def aqsTask).main
		}
	setEnvironmentalData (TT aqsTask) (TT temp) (TT humid) =
		TT ( aqsTask >>~ \{tskVl = AirQualitySensor i,trace}.
			 temp >>~ \temp.
			 humid >>~ \humid.
			 get sharedObjectValue >>~ \sdss.
			 let newAQS = {fromDynamic (sdss!!i).oValue & environment=(temp.tskVl, humid.tskVl)}
			 in upd (applyAt i (\aqs.{aqs & oValue=dynamic newAQS})) sharedObjectValue >?|
			 return
				{ trace = trace3 "setEnvironment" trace temp.trace humid.trace
				, tskVl = Value () True
				, nTask = rtrn (lit ())
				}
		)
	tvoc` _ (TT aqsTask) =
		TT ( aqsTask >>~ \{tskVl = AirQualitySensor i}.
			 get sharedObjectValue >>~ \sdss.
			 return
			 	{ trace = ["TVoC " + aqsName i]
			 	, tskVl = Value (fromDynamic (sdss !! i).oValue).tvoc False
			 	, nTask = tvoc (TT aqsTask)
			 	} 
		   )
	co2` _ (TT aqsTask) =
		TT ( aqsTask >>~ \{tskVl = AirQualitySensor i}.
			 get sharedObjectValue >>~ \sdss.
			 return
			 	{ trace = ["CO2 " + aqsName i]
			 	, tskVl = Value (fromDynamic (sdss !! i).oValue).co2 False
			 	, nTask = co2 (TT aqsTask)
			 	} 
		   )

instance lcd TT where
	LCD len lines pins def =
		{main =
			toTT freshOId >>~- \i.
			let	lcdId = LCDid i
				lcdObject =
				    { lcdtxt    = repeatn lines (toString (repeatn len '.'))
				    , cursorPos = 0
				    , cursorLin = 0
				    , sizeH     = lines
				    , sizeW     = len
				    }
			    lcdTask =
			    	TT (return
			    		{ trace = ["lcd_"+toString i]
			    		, tskVl = lcdId
			    		, nTask = lcdTask
			    		})
			in toTT (upd (\l. l ++ [
							{ oId = i
							, oType = "LCD"
							, oValue = dynamic lcdObject
							}]) sharedObjectValue 
					) >?|-
			(def lcdTask).main
		}
	print (TT lcdt) (TT xt) =
		TT (lcdt >>~ \{trace = lcdTrace, tskVl = LCDid i}.
			xt >>~ \x.
			get sharedObjectValue >>~ \sdss.
			let str = toString x.tskVl
				len = size str		  
				newLCD lcd
					# nPos = min (lcd.sizeW - 1) (lcd.cursorPos + len)
					  updLine =
					  	\line.
					  		line % (0, lcd.cursorPos - 1) +
					  		str % (0, min len (nPos - lcd.cursorPos) - 1) + 
					  		line % (nPos, lcd.sizeW - 1)
					= dynamic {lcd & lcdtxt = applyAt lcd.cursorLin updLine lcd.lcdtxt, cursorPos = nPos}
			in
			upd (applyAt i (\obj.{obj & oValue = newLCD $ fromDynamic (sdss !! i).oValue})) sharedObjectValue >?|
			return
				{ trace = trace2 "print" lcdTrace [str]
				, tskVl = Value len True
				, nTask = rtrn (lit len)
				}
		   )
	setCursor (TT lcdt) (TT xt) (TT yt) =
		TT (lcdt >>~ \{trace = lcdTrace, tskVl = LCDid i}.
			xt >>~ \x.
			yt >>~ \y.
			get sharedObjectValue >>~ \sdss.
			let newLCD lcd = dynamic {lcd & cursorPos = x.tskVl, cursorLin = y.tskVl}
			in
			upd (applyAt i (\lcd.{lcd & oValue = newLCD $ fromDynamic (sdss !! i).oValue})) sharedObjectValue >?|
			return
				{ trace = trace3 "setCursor" lcdTrace x.trace y.trace
				, tskVl = Value () True
				, nTask = rtrn (lit ())
				}
		   )
	scrollLeft (TT lcd) = rtrn (lit ())
	scrollRight (TT lcd) = rtrn (lit ())
	pressed (TT bt) =
		readA a0 >>~- \(Value v _).
		TT (bt >>~ \b.
		    return
		    	{ trace = trace1 "pressed" b.trace
		    	, tskVl = Value (
		    		case b.tskVl of 
		    			RightButton		= v < 50
		    			UpButton		= 50 <= v && v < 195
		    			DownButton		= 195 <= v && v < 380
		    			LeftButton		= 380 <= v && v < 555
		    			SelectButton	= 555 <= v && v < 790
		    			NoButton		= 790 <= v)
		    		True
		    	, nTask = pressed (TT bt)
		    	}
		   )

instance buttonPressed TT where
	buttonPressed =
		readA a0 >>~- \(Value v _).
		TT (return
		    	{ trace = ["isPressed"]
		    	, tskVl = Value (
		    		if (v < 50)
		    			RightButton
		    		(if (v < 195)
		    			UpButton
		    		(if (v < 380)
		    			DownButton
		    		(if (v < 550)
		    			LeftButton
		    		(if (v < 790)
		    			SelectButton
		    			NoButton
		    		)))))
		    		True
		    	, nTask = buttonPressed
		    	}
		   )

// --- tools ---

derive class iTask Sds

unTT :: (TT t) -> Task (TraceTask t)
unTT (TT t) = t

trace1 :: String [String] -> [String]
trace1 n [x] | isMember ' ' [c \\ c <-: x] // string contains space
	= ["(" + n + " " + x + ")"]
	= [n + " " + x]
trace1 n x   = [n:indent x]

trace2 :: String [String] [String] -> [String]
trace2 n [x] [y] = ["(" + n + " " + x + " " + y + ")"]
trace2 n x   y   = [n:indent (x ++ y)]

trace3 :: String [String] [String] [String] -> [String]
trace3 n [x] [y] [z] = ["(" + n + " " + x + " " + y + " " + z + ")"]
trace3 n x   y   z   = [n:indent (x ++ y ++ z)]

trace2i :: String [String] [String] -> [String]
trace2i n [x] [y] = ["(" + x + n + y + ")"]
trace2i n x   y   = indent x ++ [n: indent y]

readAPin :: APin -> Task Int
readAPin p =
	get sharedMPS >>~ \mps. 
	case lookupPin p mps.apins of
		?Just i = return i
		?None =
			upd (\mps.{mps & apins = updatePin p 0 mps.apins}) sharedMPS >?|
			return 0

readDPin :: DPin -> Task Bool
readDPin p =
	get sharedMPS >>~ \mps. 
	case lookupPin p mps.dpins of
		?Just b = return b
		?None =
			upd (\mps.{mps & dpins = updatePin p False mps.dpins}) sharedMPS >?|
			return False

lookupPin :: p [(p,v)] -> ?v | == p
lookupPin p [] = ?None
lookupPin p [(q,w):r] | p == q
	= ?Just w
	= lookupPin p r

updatePin :: p v [(p,v)] -> [(p,v)] | == p
updatePin p v [] = [(p,v)]
updatePin p v [(q,w):r] | p == q
	= [(q,v):r]
	= [(q,w):updatePin p v r]

:: MPS =
	{ dpins    :: [(DPin, Bool)]
	, apins    :: [(APin, Int)]
	, clock    :: Int
	, interval :: Int
	}

mps0 :: MPS
mps0 =
	{ dpins    = []
	, apins    = []
	, clock    = 0
	, interval = 1
	}

sharedMPS :: SimpleSDSLens MPS
sharedMPS = sharedStore "mps" mps0

isUnique :: [a] -> Bool | Eq a
isUnique [] = True
isUnique [a:x] = not (isMember a x) && isUnique x

indent :: ([String] -> [String])
indent = map ((+) "' ")

updateAPin :: APin Int -> Task MPS
updateAPin p i = upd (\mps.{mps & apins = updatePin p i mps.apins}) sharedMPS

instance == APin where (==) x y = x === y
instance == DPin where (==) x y = x === y

:: ObjectValue =
	{ oId    :: Int
	, oType  :: String
	, oValue :: Dynamic
	}
:: ObjectDisplay =
	{ objectId	  :: Int
	, objectType  :: String
	, objectValue :: String
	}
:: DHTObject =
	{ temperature :: Real
	, humidity    :: Real
	}
:: GestureObject =
	{ gesture     :: Gesture
	}
:: LightObject =
	{ light       :: Real
	}
:: AirQualityObject =
	{ environment :: (Real, Real)
	, co2         :: Int
	, tvoc        :: Int
	}
:: LCDObject =
	{ lcdtxt    :: [String]
	, cursorPos :: Int
	, cursorLin :: Int
	, sizeW     :: Int
	, sizeH     :: Int
	}

:: LiftedSDS = E.sds t: LiftedSDS (sds () t t) & RWShared sds & iTask t

derive toByteCode DHTObject, LCD, AirQualityObject, [], GestureObject
derive fromByteCode DHTObject, LCD, AirQualityObject, [], GestureObject
derive gDefault DHTObject, AirQualityObject, GestureObject
gDefault{|ObjectValue|} = {oId = -1, oType = "?", oValue = dynamic ()}
derive class iTask DHTObject, AirQualityObject, GestureObject, ObjectDisplay, LightObject, LCDObject
toByteCode{|String|} _ = undef
fromByteCode{|String|} = undef

toObjectDisplay :: ObjectValue -> ObjectDisplay
toObjectDisplay obj = {objectId = obj.oId, objectType = obj.oType, objectValue = dynToString obj.oValue}

dynToString :: Dynamic -> String
dynToString (d::Int) = toString d
dynToString (d::Bool) = toString d
dynToString (d::String) = toString d
dynToString (d::Real) = toString d
dynToString (d::Char) = toString d
dynToString (d::LCDObject) = glue d.lcdtxt where
	glue [] = ""
	glue [a:x] = "\"" + a + "\" " + glue x
dynToString (d::DHTObject) = "Temperature = " + toString d.temperature + " ,Humidity = " + toString d.humidity
dynToString (d::GestureObject) = "GestureSensor " + toString d.gesture
dynToString (d::LightObject) = "LightObject" + toString d.light
dynToString (d::AirQualityObject) = "AirQualityObject, environment=" + toString d.environment + " co2=" + toString d.co2 + " tvoc=" + toString d.tvoc
dynToString (d::a->b) = "function"
dynToString (d::LiftedSDS) = "Lifted SDS"
dynToString _ = "..."

sharedObjectValue :: SimpleSDSLens [ObjectValue]
sharedObjectValue = sharedStore "simShare" []

freshOId :: Task Int
freshOId = get sharedObjectValue >>~ \list.return (length list)

sdsName :: Int -> String
sdsName i = "sds" + toString i

dhtName i = "dht" + toString i
aqsName i = "aqs" + toString i


applyAt :: Int (a->a) [a] -> [a]
applyAt 0 f [a:x] = [f a:x]
applyAt n f [a:x] = [a:applyAt (n-1) f x]
applyAt n f []    = []

instance int TT Int where int i = i
instance int TT Real where int i = toInt <$> i
instance int TT Long where int i = toInt <$> i
instance real TT Int where real i = toReal <$> i
instance real TT Real where real i = i
instance real TT Long where real i = toReal <$> i
instance long TT Int where long i = Long <$> i
instance long TT Real where long i = Long o toInt <$> i
instance long TT Long where long i = i
