definition module mTask.Simulate.TraceTask

/**
 * The iTask simulator view for the mTask DSL
 */

import mTask.Language

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..), :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

/**
 * The simulated task datatype
 *
 * @var type
 */
:: TraceTask a =
	{ trace :: [String]
	//** A textual trace of the task
	, tskVl :: a
	//** The current task value
	, nTask :: TT a
	//** The continuation when rewriting
	}

derive class iTask TraceTask

/**
 * The iTask simulator datatype
 * 
 * @var type
 */
:: TT a =: TT (Task (TraceTask a))

//** Simulate an mTask program
simulate :: (Main (TT a)) -> Task (TraceTask a) | type a

instance LEDMatrix TT
instance LightSensor TT
instance AirQualitySensor TT
instance GestureSensor TT
instance aio TT
instance buttonPressed TT
instance delay TT
instance dht TT
instance dio APin TT
instance dio DPin TT
instance pinMode TT
instance interrupt TT
instance expr TT
instance lcd TT
instance liftsds TT
instance rpeat TT
instance rtrn TT
instance sds TT
instance step TT
instance tupl TT
instance unstable TT
instance .&&. TT
instance .||. TT
instance fun () TT
instance fun (TT a) TT | type a
instance fun (TT a, TT b) TT | type a & type b
instance fun (TT a, TT b, TT c) TT | type a & type b & type c

instance int TT Int
instance int TT Real
instance int TT Long

instance real TT Int
instance real TT Real
instance real TT Long

instance long TT Int
instance long TT Real
instance long TT Long
