definition module mTask.Interpret

from iTasks.WF.Definition import :: Task

import mTask.Language
import mTask.SemVer

import mTask.Interpret.ByteCodeEncoding
from mTask.Interpret.Peripheral import
	:: BCPeripheral, :: BCPeripheralInfo,
	instance dht Interpret,
	instance LEDMatrix Interpret,
	instance LightSensor Interpret,
	instance AirQualitySensor Interpret,
	instance GestureSensor Interpret,
	instance NeoPixel Interpret

from mTask.Interpret.Device import
	withDevice,
	withDevice`,
	liftmTask,
	liftmTaskWithOptions,
	preloadTask,
	preloadTaskWithOptions,
	preloadTasks,
	preloadTasksWithOptions,
	mTaskSafe,
	deviceSpecification,
	class channelSync,
	:: MTDevice,
	:: MTaskBox(..),
	:: Channels, :: MTMessageTo, :: MTMessageFro
from Data.UInt import
	:: UInt8, instance toString UInt8
from mTask.Interpret.String255 import
	:: String255
from mTask.Interpret.Compile import
	:: CompileOpts
import mTask.Interpret.Message
from mTask.Interpret.Specification import
	:: MTDeviceSpec
from mTask.Interpret.Monad import
	:: Interpret
from mTask.Interpret.DSL import
	:: BCState, :: BCInstr,
	instance aio       Interpret,
	instance expr      Interpret,
	instance tupl      Interpret,
	instance delay     Interpret,
	instance dio p     Interpret,
	instance pinMode   Interpret,
	instance interrupt Interpret,
	instance rpeat     Interpret,
	instance lowerSds  Interpret,
	instance rtrn      Interpret,
	instance sds       Interpret,
	instance step      Interpret,
	instance unstable  Interpret,
	instance .&&.      Interpret,
	instance .||.      Interpret,
	instance reflect   Interpret,
	instance fun ()                         Interpret,
	instance fun (Interpret a)              Interpret,
	instance fun (Interpret a, Interpret b) Interpret,
	instance fun (Interpret a, Interpret b, Interpret c) Interpret,
	instance int  Interpret Int, instance int  Interpret Real, instance int  Interpret Long,
	instance real Interpret Int, instance real Interpret Real, instance real Interpret Long,
	instance long Interpret Int, instance long Interpret Real, instance long Interpret Long
