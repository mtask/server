definition module mTask.Interpret

/**
 * The mTask language bytecode interpreter
 */

import mTask.Language

from Control.Monad.Identity import :: Identity
from Control.Monad.State import :: StateT
from Control.Monad.Writer import :: WriterT
from Data.Either import :: Either
from iTasks.WF.Definition import :: Task

from mTask.Interpret.ByteCodeEncoding import
	generic toByteCode, generic fromByteCode, class toByteWidth,
	instance toByteWidth (a,b),
	instance toByteWidth (a,b,c),
	instance toByteWidth (TaskValue a),
	instance toByteWidth Int,
	instance toByteWidth Bool,
	instance toByteWidth Char,
	instance toByteWidth (),
	instance toByteWidth Real,
	instance toByteWidth Long,
	instance toByteWidth Button,
	instance toByteWidth APin,
	instance toByteWidth DPin,
	instance toByteWidth Gesture,
	instance toByteWidth PinMode,
	instance toByteWidth InterruptMode
from mTask.Interpret.Peripheral import
	:: BCPeripheral,
	instance dht (StateT BCState (WriterT [BCInstr] Identity)),
	instance i2cbutton (StateT BCState (WriterT [BCInstr] Identity)),
	instance LEDMatrix (StateT BCState (WriterT [BCInstr] Identity)),
	instance LightSensor (StateT BCState (WriterT [BCInstr] Identity)),
	instance AirQualitySensor (StateT BCState (WriterT [BCInstr] Identity)),
	instance GestureSensor (StateT BCState (WriterT [BCInstr] Identity))

from mTask.Interpret.Device import
	withDevice,
	withDevice`,
	liftmTask,
	liftmTaskWithOptions,
	preloadTask,
	preloadTaskWithOptions,
	preloadTasks,
	preloadTasksWithOptions,
	mTaskSafe,
	deviceSpecification,
	class channelSync,
	:: MTDevice,
	:: MTaskBox(..),
	:: Channels, :: MTMessageTo, :: MTMessageFro
from Data.UInt import
	:: UInt8, instance toString UInt8
from mTask.Interpret.String255 import
	:: String255
from mTask.Interpret.Compile import
	:: CompileOpts
from mTask.Interpret.Message import
	:: MTException(..),
	instance toString MTException
from mTask.Interpret.Specification import
	:: MTDeviceSpec
from mTask.Interpret.DSL import
	:: BCInterpret, :: BCState, :: BCInstr,
	instance aio       (StateT BCState (WriterT [BCInstr] Identity)),
	instance expr      (StateT BCState (WriterT [BCInstr] Identity)),
	instance tupl      (StateT BCState (WriterT [BCInstr] Identity)),
	instance delay     (StateT BCState (WriterT [BCInstr] Identity)),
	instance dio p     (StateT BCState (WriterT [BCInstr] Identity)),
	instance pinMode   (StateT BCState (WriterT [BCInstr] Identity)),
	instance interrupt (StateT BCState (WriterT [BCInstr] Identity)),
	instance rpeat     (StateT BCState (WriterT [BCInstr] Identity)),
	instance liftsds   (StateT BCState (WriterT [BCInstr] Identity)),
	instance rtrn      (StateT BCState (WriterT [BCInstr] Identity)),
	instance sds       (StateT BCState (WriterT [BCInstr] Identity)),
	instance step      (StateT BCState (WriterT [BCInstr] Identity)),
	instance unstable  (StateT BCState (WriterT [BCInstr] Identity)),
	instance .&&.      (StateT BCState (WriterT [BCInstr] Identity)),
	instance .||.      (StateT BCState (WriterT [BCInstr] Identity)),
	instance fun () (StateT BCState (WriterT [BCInstr] Identity)),
	instance fun (StateT BCState (WriterT [BCInstr] Identity) a)
	                (StateT BCState (WriterT [BCInstr] Identity)),
	instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b)
	                (StateT BCState (WriterT [BCInstr] Identity)),
	instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b, StateT BCState (WriterT [BCInstr] Identity) c)
                    (StateT BCState (WriterT [BCInstr] Identity)),
	instance int  (StateT BCState (WriterT [BCInstr] Identity)) Int,
	instance int  (StateT BCState (WriterT [BCInstr] Identity)) Real,
	instance int  (StateT BCState (WriterT [BCInstr] Identity)) Long,
	instance real (StateT BCState (WriterT [BCInstr] Identity)) Int,
	instance real (StateT BCState (WriterT [BCInstr] Identity)) Real,
	instance real (StateT BCState (WriterT [BCInstr] Identity)) Long,
	instance long (StateT BCState (WriterT [BCInstr] Identity)) Int,
	instance long (StateT BCState (WriterT [BCInstr] Identity)) Real,
	instance long (StateT BCState (WriterT [BCInstr] Identity)) Long
