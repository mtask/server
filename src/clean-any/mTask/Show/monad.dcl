definition module mTask.Show.monad

/**
 * The helper functions for the pretty printer datatype
 */

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Show.basic

from Control.Monad import class Monad
from Control.Applicative import class Applicative, class pure, class <*>
from Data.Functor import class Functor

//* Print a newline
nl :: Show a
//* Increase the indentation
indent :: Show a
//* Decrease the indentation
unIndent :: Show a
instance Functor Show
instance pure Show
instance <*> Show
instance Monad Show
//* Sequential composition with a more relaxed type
(>>>|) infixl 1 :: (Show a) (Show b) -> Show b
//* Wrap the pretty printer in parenthesis
par :: (Show a) -> Show a
/**
 * Capture the pretty printed output
 *
 * @param pretty printer
 * @result the original result
 * @result the printed representation
 */
censor :: (Show a) -> Show (a, [String])
