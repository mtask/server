implementation module mTask.Show.basic

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language
import StdMisc, StdList, StdTuple, StdString, StdEnum

:: ShowState =
	{ show 	 :: [String]
	, indent :: Int
	, ids    :: [String]
	}

show :: String -> Show a
show s = Show \st.(undef, {st & show = [s:st.show]})

show2String :: a -> Show b | toString a
show2String a = Show \st.(undef, {st & show = [toString a:st.show]})

instance freshId Show where
	freshId :: Show String
	freshId = Show \st=:{ids=[a:x]}.(a, {st & ids = x})

showIt :: (Show a) -> [String]
showIt (Show f) = reverse ["\n":(snd (f state0)).show]

showMain :: (Main (Show a)) -> [String] | type a
showMain {main = Show m} = reverse ["\n":(snd (m state0)).show]

state0 =
	{ show = []
	, indent = 0
	, ids = map toString [0..]
	}
