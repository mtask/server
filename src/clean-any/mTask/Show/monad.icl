implementation module mTask.Show.monad

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Show.basic
import Control.Applicative
import Control.Monad
import StdClass, StdMisc, StdInt

nl :: Show a
nl = Show \st.(undef, {st & show = ins st.indent st.show})
where
	ins n s | n > 0
		= ["  ":ins (n - 1) s]
		= ["\n":s]

indent :: Show a
indent = Show \st.(undef, {st & indent = st.indent + 1})

unIndent :: Show a
unIndent = Show \st.(undef, {st & indent = st.indent - 1})

instance Functor Show where
	fmap f (Show s1) = Show \st.let (a, st2) = s1 st in (f a, st2)
instance pure Show where
	pure x = Show \st.(x, st)
instance <*> Show where
	(<*>) (Show f) (Show x) = Show \st0.let (f1, st1) = f st0; (x1, st2) = x st1 in (f1 x1, st2)
instance Monad Show where
	bind (Show x) f = Show \s1.let (a, s2) = x s1; (Show f2) = f a in f2 s2

(>>>|) infixl 1 :: (Show a) (Show b) -> Show b
(>>>|) a b = a >>| show " " >>| b

par :: (Show a) -> Show a
par x = show "(" >>| x >>= \r. show ")" // >>| pure r

censor :: (Show a) -> Show (a, [String])
censor (Show m) = Show \st->let (a, st`) = m {st & show=[]} in ((a, st`.show), st)
