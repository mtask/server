implementation module mTask.Show.DSL

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language
import mTask.Show.basic
import mTask.Show.monad
import Control.Monad => qualified join
import Control.Applicative
import Data.Functor
import StdOverloaded, StdMisc, StdString, StdBool, StdList, StdEnum
import Text

import mTask.Interpret.ByteCodeEncoding
import iTasks.SDS.Definition

return :== pure

instance expr Show where
	lit   t   = show (toString t) // /*show "lit" >>>| */ show2String t 
	(+.)  x y = binop x "+" y
	(-.)  x y = binop x "-" y
	(*.)  x y = binop x "*" y
	(/.)  x y = binop x "/" y
	(&.)  x y = binop x "&&" y
	(|.)  x y = binop x "||" y
	Not   x   = par (show "Not" >>>| x)
	(==.) x y = binop x "==" y
	(!=.) x y = binop x "<>" y
	(<.)  x y = binop x "<" y
	(>.)  x y = binop x ">" y
	(<=.) x y = binop x "<=" y
	(>=.) x y = binop x ">=" y
	If c t e  = show "If" >>>| c >>| t >>| e
instance tupl Show where
	first s = par (show "fst" >>| s >>| return undef)
	second s = par (show "snd" >>| s >>| return undef)
	tupl a b = par (a >>| show "," >>| b >>| return undef)

binop x o y = par (x >>| show o >>| y) >>| return undef
twoArg x y = binop x ", " y
intervalArg i a = par (showTimingInterval i >>| show ", " >>| a)

showTimingInterval :: (TimingInterval Show) -> Show a
showTimingInterval Default        = show "Default"
showTimingInterval (BeforeMs t)   = show "BeforeMs " >>| t >>| return undef
showTimingInterval (BeforeSec t)  = show "BeforeSec " >>| t >>| return undef
showTimingInterval (ExactMs t)    = show "ExactMs " >>| t >>| return undef
showTimingInterval (ExactSec t)   = show "ExactSec " >>| t >>| return undef
showTimingInterval (RangeMs l u)  = show "RangeMs " >>| l >>| show " " >>| u >>| return undef
showTimingInterval (RangeSec l u) = show "RangeSec " >>| l >>| show " " >>| u >>| return undef

instance aio Show where
	readA` i p = show "readAnalog" >>| intervalArg i p >>| return NoValue
	readA  p   = show "readAnalog" >>| return NoValue
	writeA p v = show "writeAnalog" >>| twoArg p v >>| return NoValue

instance dio APin Show where
	readD` i p = show "readDigital" >>| intervalArg i p >>| return NoValue
	readD  p   = show "readDigital" >>| return NoValue
	writeD p v = show "writeDigital" >>| twoArg p v >>| return NoValue

instance dio DPin Show where
	readD` i p = show "readDigital" >>| intervalArg i p >>| return NoValue
	readD  p   = show "readDigital" >>| return NoValue
	writeD p v = show "writeDigital" >>| twoArg p v >>| return NoValue

instance pinMode Show where
	pinMode mode p = show "pinMode(" >>| par p >>| show ", " >>| par mode >>| show ")" >>| return NoValue

instance interrupt Show
where
	interrupt mode p = show "interrupt(" >>| par p >>| show ", " >>| par mode >>| show ")" >>| return NoValue

instance rtrn Show where rtrn x = par (show "rtrn" >>>| fmap (\y.Value y True) x)
instance unstable Show where unstable x = par (show "unstable" >>>| fmap (\y.Value y False) x)

instance step Show
where
	(>>*.) e l = e >>| show " >>*" >>| indent >>| nl
		>>| show "[" >>| showSteps l >>| show "]" >>| unIndent >>| nl
	(>>=.) e l = showStepMacro " >>= " e l
	(>>~.) e l = showStepMacro " >>~ " e l
	(>>|.) e l = showStepMacroForget " >>| " e l
	(>>..) e l = showStepMacroForget " >>. " e l

showStepMacro op e f = e >>| show op >>| indent >>| nl >>| fresh "a" >>= \i->show ("\\" + i + ".") >>| f (show i) >>| unIndent
showStepMacroForget op e f = e >>| show op >>| indent >>| nl >>| f >>| unIndent

showSteps :: [Step Show a b] -> Show c
showSteps [Always e:x] = show "Ever" >>>| e >>| nl // forget the rest
showSteps [IfNoValue e:x] = show "NoValueue" >>>| e >>| nl >>| show if (x=:[]) "" "," >>| showSteps [s\\s<-x | not (s =: (IfNoValue _))]
showSteps [s:x]
	= fresh "a" >>= \a.
			(case s of
			IfValue    f e = step "Value"    f (e (show a) >>| pure x)
			IfStable   f e = step "Stable"   f (e (show a) >>| pure x)
			IfUnstable f e = step "Unstable" f (e (show a) >>| pure x)
		) >>| nl >>| show if (x=:[]) "" "," >>| showSteps x
		where
		step l f e = show l >>>| fresh "a" >>= \i. par (show ("\\" + i + ".") >>| f (show i)) >>>| e
showSteps [] = nl

instance rpeat Show where
	rpeatEvery i t = par (show "rpeat" >>>| showTimingInterval i >>>| t >>| return undef)

instance delay Show where
	delay d = par (show "delay" >>>| d >>| return undef)

fresh c  :== (+) c <$> freshId

instance fun () Show
where
	fun def =
		{main = fresh "f" >>= \f.
			let (g In {main = m}) = def (\().show ("(" + f + " ())")) in 
			show ("let " + f + " () = ") >>| g () >>| nl >>|
			show "in" >>>| m // >>| nl
		}

instance fun (Show a) Show | type a
where // ((a->v s)->In (a->v s) (Main v u)) -> Main v u | type s
	fun def =
		{main = fresh "f" >>= \f. fresh "a" >>= \a. 
			let (g In {main = m}) = def (\x.par (show (f + " ") >>| x >>| return undef)) in 
			show ("let " + f + " " + a + " = ") >>| g (show a) >>| nl >>|
			show "in" >>>| m // >>| nl
		}

instance fun (Show a, Show b) Show | type a & type b
where // ((a->v s)->In (a->v s) (Main v u)) -> Main v u | type s
	fun def =
		{main = fresh "f" >>= \f. fresh "a" >>= \a. fresh "a" >>= \b.
			let (g In {main = m}) = def (\(x,y).par (show (f + " ") >>| x >>>| y >>| return undef)) in 
			show ("let " + f + " " + a + " " + b + " = ") >>| g (show a, show b) >>| nl >>|
			show "in" >>>| m // >>| nl
		}

instance fun (Show a, Show b, Show c) Show | type a & type b & type c
where // ((a->v s)->In (a->v s) (Main v u)) -> Main v u | type s
	fun def =
		{main = fresh "f" >>= \f. fresh "a" >>= \a. fresh "a" >>= \b. fresh "a" >>= \c.
			let (g In {main = m}) = def (\(x,y,z).par (show (f + " ") >>| x >>>| y >>| z >>| return undef)) in 
			show ("let " + f + " " + a + " " + b + " " + c + " = ") >>| g (show a, show b, show c) >>| nl >>|
			show "in" >>>| m // >>| nl
		}

instance sds Show where
	sds def =
		{main = fresh "s" >>= \s. 
			let (v In {main = m}) = def (show s) in 
			show ("sds " + s + "=" + toString v) >>| nl >>|
			show "in" >>>| m >>| nl
		}
	getSds` i sds = show "getSDS" >>>| showTimingInterval i >>>| sds >>| return undef
	getSds sds = show "getSDS" >>>| sds >>| return undef
	setSds sds v = show "setSDS" >>>| sds >>>| v >>| return undef
	updSds sds v = fresh "v" >>= \s->show "updSDS" >>>| sds >>>| show ("(\\" + s + "->") >>>| v (show s) >>>| show ")" >>>| return undef

instance liftsds Show where
	liftsds def =
		{main = fresh "s" >>= \s. 
			let (v In {main = m}) = def (show s) in 
			show ("liftsds " + s + "= ...") >>| nl >>|
			show "in" >>>| m >>| nl
		}

instance dht Show where
	DHT i def =
		{main = fresh "d" >>= \dht. let {main = m} = def (show dht) in
						show (toSingleLineText i +++ ";\n") >>| m}
	temperature` i dht = dht >>| show ".temperature(" >>| showTimingInterval i >>| show ")"
	temperature dht = dht >>| show ".temperature()"
	humidity` i dht = dht >>| show ".humidity(" >>| showTimingInterval i >>| show ")"
	humidity dht = dht >>| show ".humidity()"

instance LEDMatrix Show where
	ledmatrix d def = {main = fresh "lm" >>= \lm. let {main=m} = def (show lm) in
			show ("LEDMatrix " + lm + " (" + toSingleLineText d + ");\n") >>| m}
	LMDot m x y s = m >>| show ".dot(" >>| x >>| show "," >>| y >>| show "," >>| s >>| show ")"
	LMIntensity m x = m >>| show ".intensity(" >>| x >>| show ")"
	LMClear m = m >>| show ".clear()"
	LMDisplay m = m >>| show ".display()"

instance LightSensor Show where
	lightsensor addr def = {main = fresh "ls" >>= \sensor ->
		let {main = m} = def (show sensor) in
			show ("LightSensor " + sensor + " (" + toString addr + ")\n") >>| m}
	light` i ls = ls >>| show ".light(" >>| showTimingInterval i >>| show ")"
	light ls = ls >>| show ".light()"

instance AirQualitySensor Show where
	airqualitysensor addr def = {main = fresh "aqs" >>= \sensor ->
		let {main = m} = def (show sensor) in
			show ("AirQualitySensor " + sensor + " (" + toString addr + ")\n") >>| m}
	setEnvironmentalData s humid temp = s >>| show ".setEnvironmentalData(" >>| humid >>| show ", " >>| temp >>| return undef
	tvoc` i s = s >>| show ".TVOC(" >>| showTimingInterval i >>| show ")"
	tvoc s = s >>| show ".TVOC()"
	co2` i s = s >>| show ".CO2(" >>| showTimingInterval i >>| show ")"
	co2 s = s >>| show ".CO2()"

instance GestureSensor Show where
	gestureSensor addr def = {main = fresh "ges" >>= \sensor ->
		let {main = m} = def (show sensor) in
			show ("GestureSensor " + sensor + " (" + toString addr + ")\n") >>| m}
	gesture` i s = s >>| show ".gesture("  >>| showTimingInterval i >>| show ")"
	gesture s = s >>| show ".gesture()"

instance lcd Show where
	print lcd v = lcd >>| show ".print" >>>| v >>| return undef
	setCursor lcd n m = lcd >>| show ".setCursor" >>>| twoArg n m >>| return undef
	LCD x y l def = 
		{main = fresh "l" >>= \lcd. let {main = m} = def (show lcd) in
						show ("LCD " + lcd + " (" + toString x + ", " + toString y + ");\n") >>| m}
	scrollLeft lcd = lcd >>| show "scrollLeft " 
	scrollRight lcd = lcd >>| show "scrollRight "
	pressed b = show "pressed" >>>| b >>| rtrn (lit False)

instance i2cbutton Show where
	i2cbutton addr def = {main = fresh "b" >>= \but.
		let {main = m} = def (show but) in
			show ("I2CButton " + but + " (" + toString addr + ");\n") >>| m}
	AButton` i but = but >>| show ".AButton(" >>| showTimingInterval i >>| show ")"
	AButton but = but >>| show ".AButton()"
	BButton` i but = but >>| show ".BButton(" >>| showTimingInterval i >>| show ")"
	BButton but = but >>| show ".BButton()"

instance buttonPressed Show where buttonPressed = show "buttonPressed" >>| rtrn (lit NoButton)

instance .||. Show where (.||.) x y = x >>| nl >>| show ".||." >>| nl >>| y
instance .&&. Show where (.&&.) x y = x >>| nl >>| show ".&&." >>| nl >>| y >>| return undef

instance int Show Int where int i = i
instance int Show Real where int i = show "(int)" >>| i >>| return undef
instance int Show Long where int i = show "(long)" >>| i >>| return undef
instance real Show Int where real i = show "(float)" >>| i >>| return undef
instance real Show Real where real i = i
instance real Show Long where real i = show "(float)" >>| i >>| return undef
instance long Show Int where long i = show "(long)" >>| i >>| return undef
instance long Show Real where long i = show "(long)" >>| i >>| return undef
instance long Show Long where long i = i
