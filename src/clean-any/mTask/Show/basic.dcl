definition module mTask.Show.basic

/**
 * The basic functions for the pretty printing view
 */

/*
	Pieter Koopman
	Radboud University NIjmegen, The Netherlands
	pieter@cs.ru.nl
*/

import mTask.Language

//** The datatype for the pretty printer that implements the mTask classes
:: Show a = Show (ShowState -> (a, ShowState))

//** The state for the pretty printer
:: ShowState =
	{ show 	 :: [String]
	//** Currently produces text
	, indent :: Int
	//** Indentation level
	, ids    :: [String]
	//** Stream of unique identifiers
	}

/**
 * Print a string
 *
 * @param string to print
 * @result pretty printer
 */
show :: String -> Show a
/**
 * Pretty print something by converting it to a string first
 *
 * @param thing to print
 * @result pretty printer
 */
show2String :: a -> Show b | toString a
/**
 * Pretty print a {{Main}} object and execute the pretty printer
 *
 * @param {{Main}} object to print
 * @result pretty printed representation
 */
showMain :: (Main (Show a)) -> [String] | type a
/**
 * Execute a pretty printer.
 *
 * @param pretty printer
 * @result pretty printed representation
 */
showIt :: (Show a) -> [String]

/**
 * Class defining the possibility for fresh name generation
 *
 * @var view
 * @result a fresh name in the view
 */
class freshId s :: s String

instance freshId Show
