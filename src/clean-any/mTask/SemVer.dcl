definition module mTask.SemVer

from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.GenType import generic gType, :: Box, :: GType
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError, :: Either, :: CDeserialiser, :: StateT
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: EditorPurpose, :: Editor, :: EditorReport
from iTasks.WF.Definition import class iTask, :: Task

from Data.UInt import :: UInt8(..)

:: SemVer = { major :: UInt8, minor :: UInt8, patch :: UInt8 }

CURRENT_SERVER_VERSION :== {major = UInt8 0, minor = UInt8 6, patch = UInt8 0}
CURRENT_CLIENT_VERSION :== {major = UInt8 0, minor = UInt8 4, patch = UInt8 2}

derive gType SemVer
derive gCSerialise SemVer
derive gCDeserialise SemVer
derive class iTask SemVer
derive gDefault SemVer

versionSufficient :: SemVer SemVer -> Bool
serverVersionSufficient :: SemVer -> Bool
clientVersionSufficient :: SemVer -> Bool
