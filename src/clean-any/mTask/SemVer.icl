implementation module mTask.SemVer

import Data.GenDefault
import Data.GenType
import Data.GenType.CSerialise
import Data.UInt
import StdBool
import StdTuple
import Text.GenJSON
import iTasks

derive gType SemVer
derive gCSerialise SemVer
derive gCDeserialise SemVer
derive class iTask SemVer
derive gDefault SemVer

versionSufficient :: SemVer SemVer -> Bool
versionSufficient svRef svTest
	| svRef.major <> svTest.major = False
	| svRef.major == zero
		| svRef.minor <> svTest.minor = False
		| otherwise = svRef.patch <= svTest.patch
	| otherwise = svRef.minor <= svTest.minor

serverVersionSufficient :: SemVer -> Bool
serverVersionSufficient sv = versionSufficient CURRENT_SERVER_VERSION sv

clientVersionSufficient :: SemVer -> Bool
clientVersionSufficient sv = versionSufficient CURRENT_CLIENT_VERSION sv
