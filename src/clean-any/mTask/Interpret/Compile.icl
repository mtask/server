implementation module mTask.Interpret.Compile

import Data.Either
import Data.GenEq
import StdEnv
import Data.Func
import Text => qualified join
import qualified Data.Map as DM
import Control.Monad => qualified return
import Control.Applicative
import Data.UInt
import mTask.Interpret.DSL => qualified :: MTask
import mTask.Interpret.String255
import mTask.Interpret.VoidPointer
import mTask.Interpret.Message
import mTask.Interpret.Instructions
import Data.GenType.CSerialise

from iTasks.WF.Tasks.Core import return
from iTasks.WF.Tasks.SDS import get
from iTasks.SDS.Combinators.Common import mapRead
from iTasks.SDS.Definition import class RWShared, class Registrable, class Modifiable, class Readable, class Writeable, class Identifiable, :: Shared
from iTasks.Internal.SDS import
	instance Identifiable SDSLens,
	instance Readable SDSLens,
	instance Writeable SDSLens,
	instance Modifiable SDSLens,
	instance Registrable SDSLens

instance zero CompileOpts where zero = {tailcallopt=True,labelresolve=True,shorthands=True}

import StdDebug

compileOpts :: CompileOpts (Main (BCInterpret (TaskValue v))) -> (String255, [(? MTLens, Task BCShareSpec)], {BCPeripheral}, [BCInstr]) | type v
compileOpts opts task
	# st = mainBC task zero
	# funexpr = flatten
		[if opts.tailcallopt (tailcallopt v) id v.bcf_instructions
		\\(k, v)<-'DM'.toList st.bcs_functions]
	# instr = if opts.shorthands (shorthands o map stepargs) id
		//Bootstrap so that the main function potentially can be removed
		$  [BCJump st.bcs_freshlabel:funexpr]
		++ [BCLabel st.bcs_freshlabel:st.bcs_mainexpr]
		++ [BCReturn (UInt8 1) (UInt8 0)]
	# labelmap = makeLabelmap zero instr 'DM'.newMap
	| not (trace_tn (toString (length st.bcs_hardware) +++ " peripherals: " +++ toSingleLineText (st.bcs_hardware))) = undef
	| not (trace_tn (toString ('DM'.mapSize st.bcs_sdses) +++ " shares")) = undef
	=	//Return width
		( String255 (createArray (2*(toByteWidth $ unpack $ unpack $ unpack task)) '\0')
		//Shares
		, map toShareSpec $ 'DM'.toList st.bcs_sdses
		, {a\\a<-st.bcs_hardware}
		//Bytecode
			//Resolve labels
		,   if opts.labelresolve (filter (\x->not (x =: BCLabel _)) o flip labelResolve{|*|} labelmap) id
			//Rewrite stepargs
			$ instr
		)
where
	stepargs (BCStepArg i j) = BCArg j
	stepargs x = x

	toShareSpec :: (Int, Either String255 MTLens) -> (? MTLens, Task BCShareSpec)
	toShareSpec (id, Left v) = (?None, return {bcs_value=v,bcs_ident=UInt8 id,bcs_itasks=False})
	toShareSpec (id, Right sh) = (?Just sh, get $ mapRead (\v->{bcs_value=v,bcs_ident=UInt8 id,bcs_itasks=True}) sh)

	makeLabelmap :: UInt16 [BCInstr] -> (('DM'.Map JumpLabel JumpLabel) -> ('DM'.Map JumpLabel JumpLabel))
	makeLabelmap addr [] = id
	makeLabelmap addr [BCLabel i:is] = makeLabelmap addr is o 'DM'.put i (JL addr)
	makeLabelmap addr [i:is] = makeLabelmap (addr + UInt16 (bytewidth i)) is

generic labelResolve a :: a -> (('DM'.Map JumpLabel JumpLabel) -> a)
derive labelResolve BCInstr, UInt8, UInt16, BCTaskType, [], String255, PinMode, InterruptMode
labelResolve{|OBJECT|} fa (OBJECT a) = (\x->OBJECT x) o fa a
labelResolve{|EITHER|} fl _ (LEFT a) = (\x->LEFT x) o fl a
labelResolve{|EITHER|} _ fr (RIGHT a) = (\x->RIGHT x) o fr a
labelResolve{|CONS|} fa (CONS a) = (\x->CONS x) o fa a
labelResolve{|PAIR|} fl fr (PAIR a b) = \s->PAIR (fl a s) (fr b s)
labelResolve{|UNIT|} UNIT = \_->UNIT
labelResolve{|a|} a = \_->a
labelResolve{|JumpLabel|} a = 'DM'.find a

goestoreturn :: [BCInstr] -> Bool
//We can safely jump over labels
goestoreturn [BCLabel _:xs] = goestoreturn xs
//Jumps (e.g. to jump from the if to the endif) are always to the front
//If the label is not in this function, then it is not a tailcall
goestoreturn [BCJump lab:xs] = goestoreturn $ dropWhile ((=!=) (BCLabel lab)) xs
goestoreturn [BCReturn _ _:_] = True
goestoreturn _ = False

tailcallopt :: BCFunction [BCInstr] -> [BCInstr]
tailcallopt fun [BCLabel l:instr] = [BCLabel l:opt instr]
where
	opt [] = []
	opt [BCJumpSR aw lab:rest]
		| goestoreturn rest
		= [BCTailcall fun.bcf_argwidth aw lab:opt rest]
	opt [x:rest] = [x:opt rest]

shorthands :: [BCInstr] -> [BCInstr]
shorthands [] = []
//First we make ranges
shorthands [BCArg i,BCArg j:xs]
	| i == j+one = shorthands [BCArgs i j:xs]
shorthands [BCArgs i j ,BCArg k:xs]
	| j == k+one = shorthands [BCArgs i k:xs]
//Tuple shorthands
shorthands [BCArgs (UInt8 4) (UInt8 3):xs] = shorthands [BCArg43:xs]
shorthands [BCArgs (UInt8 3) (UInt8 2):xs] = shorthands [BCArg32:xs]
shorthands [BCArgs (UInt8 2) (UInt8 1):xs] = shorthands [BCArg21:xs]
shorthands [BCArgs (UInt8 1) (UInt8 0):xs] = shorthands [BCArg10:xs]
//Then we do single arguments
shorthands [BCArg (UInt8 0):xs] = shorthands [BCArg0:xs]
shorthands [BCArg (UInt8 1):xs] = shorthands [BCArg1:xs]
shorthands [BCArg (UInt8 2):xs] = shorthands [BCArg2:xs]
shorthands [BCArg (UInt8 3):xs] = shorthands [BCArg3:xs]
shorthands [BCArg (UInt8 4):xs] = shorthands [BCArg4:xs]
//Combine pops
shorthands [BCPop i,BCPop j:xs] = shorthands [BCPop (i+j):xs]
//Shorthand pops
shorthands [BCPop (UInt8 0):xs] = shorthands xs
shorthands [BCPop (UInt8 1):xs] = shorthands [BCPop1:xs]
shorthands [BCPop (UInt8 2):xs] = shorthands [BCPop2:xs]
shorthands [BCPop (UInt8 3):xs] = shorthands [BCPop3:xs]
shorthands [BCPop (UInt8 4):xs] = shorthands [BCPop4:xs]
//Combine pushes
shorthands [BCPush s1,BCPush s2:xs] = shorthands [BCPush (s1 +++ s2):xs]
//Shorthand pushes
shorthands [BCPush (String255 s):xs]
	| size s == 1 = shorthands [BCPush1 (UInt8 (toInt s.[0])):xs]
	| size s == 2 = shorthands [BCPush2 (UInt8 (toInt s.[0])) (UInt8 (toInt s.[1])):xs]
	| size s == 3 = shorthands [BCPush3 (UInt8 (toInt s.[0])) (UInt8 (toInt s.[1])) (UInt8 (toInt s.[2])):xs]
	| size s == 4 = shorthands [BCPush4 (UInt8 (toInt s.[0])) (UInt8 (toInt s.[1])) (UInt8 (toInt s.[2])) (UInt8 (toInt s.[3])):xs]
//Shorthand return instructions
shorthands [BCReturn (UInt8 1) (UInt8 0):xs] = shorthands [BCReturn1_0:xs]
shorthands [BCReturn (UInt8 1) (UInt8 1):xs] = shorthands [BCReturn1_1:xs]
shorthands [BCReturn (UInt8 1) (UInt8 2):xs] = shorthands [BCReturn1_2:xs]
shorthands [BCReturn (UInt8 1) n:xs] = shorthands [BCReturn1 n:xs]
shorthands [BCReturn (UInt8 2) (UInt8 0):xs] = shorthands [BCReturn2_0:xs]
shorthands [BCReturn (UInt8 2) (UInt8 1):xs] = shorthands [BCReturn2_1:xs]
shorthands [BCReturn (UInt8 2) (UInt8 2):xs] = shorthands [BCReturn2_2:xs]
shorthands [BCReturn (UInt8 2) n:xs] = shorthands [BCReturn2 n:xs]
shorthands [BCReturn (UInt8 3) (UInt8 0):xs] = shorthands [BCReturn3_0:xs]
shorthands [BCReturn (UInt8 3) (UInt8 1):xs] = shorthands [BCReturn3_1:xs]
shorthands [BCReturn (UInt8 3) (UInt8 2):xs] = shorthands [BCReturn3_2:xs]
shorthands [BCReturn (UInt8 3) n:xs] = shorthands [BCReturn3 n:xs]
shorthands [BCReturn n (UInt8 0):xs] = shorthands [BCReturn_0 n:xs]
shorthands [BCReturn n (UInt8 1):xs] = shorthands [BCReturn_1 n:xs]
shorthands [BCReturn n (UInt8 2):xs] = shorthands [BCReturn_2 n:xs]
//And shorthands, this occurs in step continuation functions
shorthands [BCPush2 (UInt8 0) (UInt8 1),BCAnd:xs] = shorthands xs
shorthands [BCPush2 (UInt8 0) (UInt8 0),BCOr:xs] = shorthands xs
//And the rest
shorthands [x:xs] = [x:shorthands xs]

derive gCSerialise BCShareSpec
derive gCDeserialise BCShareSpec

derive class iTask BCShareSpec
