definition module mTask.Interpret.Instructions

/**
 * Bytecode instructions
 *
 * TODO: complete documentation of all instructions
 */

from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Text.HTML import :: HtmlTag

from Data.Either import :: Either

from StdClass import class <
from Data.UInt import :: UInt8, :: UInt16
from Data.Map import :: Map
from mTask.Interpret.String255 import :: String255
from mTask.Language.pinIO import :: PinMode
from mTask.Language.Interrupts import :: InterruptMode

from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError

derive class iTask BCInstrs, BCInstr, BCTaskType, JumpLabel

derive gCSerialise BCInstrs, BCInstr, BCTaskType, JumpLabel, PinMode, InterruptMode
derive gCDeserialise BCInstrs, BCInstr, BCTaskType, JumpLabel, PinMode, InterruptMode

//** Determine the width of an instruction in bytes.
bytewidth :: BCInstr -> Int
//** Print instructions in a pretty way using columns (byte number, instruction, arguments).
debugInstructions :: [BCInstr] -> [(String, String, [String])]
//** Format debug instructions to HTML for an iTask editor (see {{debugInstructions}}).
formatDebugInstructions :: [(String, String, [String])] -> HtmlTag
//** Generically print the instructions.
printInstr :: BCInstr -> (String, [String])

//** Helper type to encode instructions more compact
:: BCInstrs =: BCIs {BCInstr}
//* Helper function for C-code generation of the {{BCInstrs}} helper type (see {{gCSerialise}})
bcinstrsgType :: (String, [String], String -> [String], String -> [String])

//** Synonym type for the width of arguments in a function
:: ArgWidth    :== UInt8
//** Synonym type for the width of the return value
:: ReturnWidth :== UInt8
//** Synonym type for the depth of rolling
:: Depth       :== UInt8
//** Synonym type for the number of rolls
:: Num         :== UInt8
//** Synonym type for an SDS id
:: SdsId       :== UInt8
//** Jump label
:: JumpLabel   =: JL UInt16
instance < JumpLabel
instance == JumpLabel
instance toString JumpLabel

//** Datatype housing all instructions
:: BCInstr
	//Return instructions
	= BCReturn1_0      | BCReturn1_1      | BCReturn1_2     | BCReturn1 ArgWidth
	| BCReturn2_0      | BCReturn2_1      | BCReturn2_2     | BCReturn2 ArgWidth
	| BCReturn3_0      | BCReturn3_1      | BCReturn3_2     | BCReturn3 ArgWidth
	| BCReturn_0 ArgWidth | BCReturn_1 ReturnWidth | BCReturn_2 ReturnWidth | BCReturn ReturnWidth ArgWidth
	//Jumping and function calls
	| BCJumpF JumpLabel | BCJump JumpLabel | BCLabel JumpLabel
	| BCJumpSR ArgWidth JumpLabel
	// Tailcall: first is argwidth of the jumper, second the jumped to
	| BCTailcall ArgWidth ArgWidth JumpLabel
	//Arguments
	| BCArg0 | BCArg1 | BCArg2 | BCArg3 | BCArg4 | BCArg ArgWidth //Single args
	| BCArg10 | BCArg21 | BCArg32 | BCArg43                       //Double args
	| BCArgs ArgWidth ArgWidth //Args from to where from > to
	| BCStepArg UInt16 UInt8
	//Task node creation
	| BCMkTask BCTaskType
	//Task node refinement
	| BCTuneRateMs | BCTuneRateSec
	//Task value ops
	| BCIsStable | BCIsUnstable | BCIsNoValue | BCIsValue
	//Stack ops
	| BCPush1 UInt8 | BCPush2 UInt8 UInt8 | BCPush3 UInt8 UInt8 UInt8 | BCPush4 UInt8 UInt8 UInt8 UInt8 | BCPush String255 | BCPushNull
	| BCPop1 | BCPop2 | BCPop3 | BCPop4 | BCPop Num
	| BCRot Depth Num | BCDup | BCPushPtrs
	//Casting
	| BCItoR | BCItoL | BCRtoI | BCRtoL | BCLtoI | BCLtoR
	//Int arith
	| BCAddI | BCSubI | BCMultI | BCDivI
	//Long arith
	| BCAddL | BCSubL | BCMultL | BCDivL
	//Real arith
	| BCAddR | BCSubR | BCMultR | BCDivR
	//Bool arith
	| BCAnd | BCOr | BCNot
	//Equality
	| BCEqI | BCNeqI | BCEqL | BCNeqL
	//Int comparison
	| BCLeI | BCGeI | BCLeqI | BCGeqI
	//Long comparison
	| BCLeL | BCGeL | BCLeqL | BCGeqL
	//Real comparison
	| BCLeR | BCGeR | BCLeqR | BCGeqR

//** Datatype housing all task types
:: BCTaskType
	= BCStable0 | BCStable1 | BCStable2 | BCStable3 | BCStable4 | BCStableNode ArgWidth
	| BCUnstable0 | BCUnstable1 | BCUnstable2 | BCUnstable3 | BCUnstable4 | BCUnstableNode ArgWidth
	// Pin io
	| BCReadD | BCWriteD | BCReadA | BCWriteA | BCPinMode
	// Interrupts
	| BCInterrupt
	// Repeat
	| BCRepeat
	// Delay
	| BCDelay
	| BCDelayUntil
	//* Only for internal use
	// Parallel
	| BCTAnd | BCTOr
	//Step
	| BCStep ArgWidth JumpLabel
	| BCStepStable ArgWidth JumpLabel | BCStepUnstable ArgWidth JumpLabel
	| BCSeqStable ArgWidth | BCSeqUnstable ArgWidth
	//Sds ops
	| BCSdsGet SdsId | BCSdsSet SdsId | BCSdsUpd SdsId JumpLabel
	// Rate limiter
	| BCRateLimit
	////Peripherals
	//DHT
	| BCDHTTemp UInt8 | BCDHTHumid UInt8
	//LEDMatrix
	| BCLEDMatrixDisplay UInt8 | BCLEDMatrixIntensity UInt8 | BCLEDMatrixDot UInt8 | BCLEDMatrixClear UInt8
	//I2CButton
	| BCAButton UInt8 | BCBButton UInt8
	//LightSensor
	| BCGetLight UInt8
	//AirQualitySensor
	| BCSetEnvironmentalData UInt8 | BCTVOC UInt8 | BCCO2 UInt8
	//Gesture
	| BCGesture UInt8
	//Internal
	| BCEvent
	//* Only for internal use (storing events in the task tree heap)
