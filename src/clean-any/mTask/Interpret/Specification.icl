implementation module mTask.Interpret.Specification

import StdEnv
import Data.GenDefault

import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.DSL
import mTask.Language

import Data.GenType.CSerialise

derive class iTask MTDeviceSpec

derive gCSerialise MTDeviceSpec
derive gCDeserialise MTDeviceSpec

derive gDefault MTDeviceSpec
instance zero MTDeviceSpec where zero = gDefault{|*|}
