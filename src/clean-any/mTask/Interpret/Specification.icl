implementation module mTask.Interpret.Specification

import Data.GenDefault
import Data.GenType.CSerialise
import StdEnv
import Text.GenJSON

import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.DSL
import mTask.Language
import mTask.SemVer

derive class iTask MTDeviceSpec

derive gCSerialise MTDeviceSpec
derive gCDeserialise MTDeviceSpec

derive gDefault MTDeviceSpec
instance zero MTDeviceSpec where zero = gDefault{|*|}
