implementation module mTask.Interpret.Device.Serial

import Control.Monad.State
import Data.Either
import Data.GenType.CSerialise
import Data.Maybe
import StdEnv
import System.Time
import iTasks
import iTasks.Extensions.Serial

import mTask.Interpret => qualified :: MTask
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message

instance channelSync TTYSettings where
	 channelSync settings channels
		= syncSerialChannel
			{tv_sec=0,tv_nsec=100*1000000}
			settings
			(\a->{#toChar c\\c<-gCSerialise{|*|} a []})
			write
			channels

write s = case runStateT (deserialiseMultiple listTop []) [toInt s\\s<-:s] of
	Left e = (Left (toString e), s)
	Right (a, c) = (Right a, toString c)
