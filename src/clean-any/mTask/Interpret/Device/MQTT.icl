implementation module mTask.Interpret.Device.MQTT

import iTasks
import StdFunctions
import StdArray

import Data.Func
import Data.Either
import System._Pointer
import Text

import mTask.Interpret.Device
import Data.GenType.CSerialise

import iTasks.Extensions.MQTT
import iTasks.Extensions.MQTT.Client
import iTasks.Extensions.MQTT.Util

import StdDebug

derive class iTask MQTTSettings

instance channelSync MQTTSettings where
	channelSync settings channels = mqttConnect (transformSettings settings) (mqttHandler channels settings)

mqttHandler :: (sds () Channels Channels) MQTTSettings (SimpleSDSLens MQTTClient) -> Task () | RWShared sds
mqttHandler channels settings client = subscribe >-| anyTask [upstream, downstream]
  where upstream = watch channels >>*
											[ OnValue $ ifValue (not o isEmpty o snd3) \(_,msgs,_).
													sequence (map (sendMessage client settings) msgs) >-|
													upd (\(f,_,d). (f,[],d)) channels >-|
													upstream
											, OnValue $ ifValue thd3 \_. mqttDisconnect client
											]
        downstream = watch received >>*
											[ OnValue $ ifValue (not o isEmpty) \msgs.
													upd (\(f,s,d). ((map processMessage msgs) ++ f, s, d)) channels >-|
													set [] received >-|
													downstream
											]
        received = createReceiveLens client
        subscribe = upd (\c. { c & subscribe=[("up/" +++ settings.mcuId, 1)] }) client

processMessage :: MQTTMsg -> MTMessageFro 
processMessage msg
  # (MQTTMsg _ p _) = msg
  = case deserialiseMultiple listTop [] (fromString p) of
			Left error = abort (toString error)
			Right ([m], []) = m
			Right ([], rest) = abort $ "Message not complete, non-parsed data: " +++ join "," [toString r\\r<-rest]
			Right (_, _) = abort $ "Multiple messages in one MQTT packet"
	
sendMessage :: (SimpleSDSLens MQTTClient) MQTTSettings MTMessageTo -> Task ()
sendMessage client set msg = mqttSend (MQTTMsg ("down/" +++  set.mcuId) (toString (gCSerialise{|*|} msg [])) { qos=1, retain=False }) client

// Settings
transformSettings :: MQTTSettings -> MQTTConnectionSettings
transformSettings {MQTTSettings|host,port,serverId,auth,mcuId}= 
	{ MQTTConnectionSettings
	| host         = host
	, port         = port
	, clientId     = serverId
	, keepAlive    = 60
	, cleanSession = True
	, auth         = auth
	, lwt          = ?None
	}
	
// Generate server id
generateServerId :: Task String
generateServerId = sequence (repeatn 22 (get randomInt)) >>- \r. return $ "s" +++ (toString [ toChar alphabet.[(abs c) rem 62] \\ c <- r])
	where alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
