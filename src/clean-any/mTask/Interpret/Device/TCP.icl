implementation module mTask.Interpret.Device.TCP

import StdArray, StdMisc
import iTasks
import iTasks.SDS.Definition
import System.Time
import Text
import Data.Either
import Data.GenType.CSerialise
import mTask.Interpret.Device
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message

derive class iTask TCPSettings

getTCPSettings :: Task TCPSettings
getTCPSettings = enterInformation [] <<@ Title "Settings"

instance channelSync TCPSettings where
	channelSync {TCPSettings|host,port,pingTimeout} channels = get currentTimestamp
		>>- \ts->tcpconnect host port (?Just 2000) share
			{ ConnectionHandlers
			| onConnect     = \cid _->onShareChange (ts, [])
			, onData        = onData
			, onShareChange = onShareChange
			, onDisconnect  = \_ _->(Ok (Timestamp 0, []), ?Just ([], [], True))
			, onDestroy     = \_->(Ok (Timestamp 0, []), [toString (gCSerialise{|*|} MTTShutdown [])])
			} <<@ NoUserInterface @! ()
	where
		share :: SDSParallel () (Channels, Timestamp) Channels
		share = SDSParallelWriteLeft channels currentTimestamp
			{ SDSParallelOptions
			| id     = createSDSIdentity ("|channelSync")
				(?Just (sdsIdentity channels))
				(?Just (sdsIdentity currentTimestamp))
			, name   = "channelSync"
			, param  = \()->((), ())
			, read   = id
			, writel = SDSWriteConst \_ w->Ok (?Just w)
			, writer = SDSWriteConst \_ _->Ok ?None
			}

		onData :: String (Timestamp, [Int]) (Channels, Timestamp) -> (MaybeErrorString (Timestamp, [Int]), ?Channels, [String], Bool)
		onData newdata (lastPing, acc) ((msgs,send,ss), ts)
			# acc = acc ++ fromString newdata
			= case deserialiseMultiple listTop [] acc of
				Left error = (Error (toString error), ?None, [], True)
				Right ([], newacc)
					= (Ok (lastPing, newacc), ?None, [], ss)
				Right (newmsgs, newacc)
					# lastPing = last [lastPing:[ts\\MTFPing<-newmsgs]]
					| isJust pingTimeout && lastPing <> Timestamp 0 && toInt ts - toInt lastPing > fromJust pingTimeout
						= (Error "Ping timeout", ?None, [], True)
					= (Ok (lastPing, newacc), ?Just (msgs++newmsgs, send, ss), [], ss)
		
		onShareChange :: (Timestamp, [Int]) (Channels, Timestamp) -> (MaybeErrorString (Timestamp, [Int]), ?Channels, [String], Bool)
		onShareChange (lastPing, _) (_, ts)
			| isJust pingTimeout && lastPing <> Timestamp 0 && toInt ts - toInt lastPing > fromJust pingTimeout
				= (Error "Ping timeout", ?None, [], True)
		// Nothing to send
		onShareChange lpacc ((msgs, [], ss), ts) = (Ok lpacc, ?None, [], ss)
		// Something to send
		onShareChange lpacc ((msgs, send, ss), ts) = (Ok lpacc, ?Just (msgs,[],ss), [toString (gCSerialise{|*|} s [])\\s<-send], ss)
