implementation module mTask.Interpret.Device.TCP

import Control.Monad.State
import Data.Either
import Data.GenType.CSerialise
import StdArray, StdMisc
import System.Time
import Text
import iTasks
import iTasks.SDS.Definition

import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Device
import mTask.Interpret.Message

derive class iTask TCPSettings

getTCPSettings :: Task TCPSettings
getTCPSettings = enterInformation [] <<@ Title "Settings"

instance channelSync TCPSettings where
	channelSync {TCPSettings|host,port,pingTimeout} channels
		= tcpconnect host port share handlers
		 <<@ NoUserInterface @! ()
	where
		share :: SDSParallel () (Channels, Timestamp) Channels
		share = SDSParallelWriteLeft channels currentTimestamp
			{ SDSParallelOptions
			| id     = createSDSIdentity ("|channelSync")
				(?Just (sdsIdentity channels))
				(?Just (sdsIdentity currentTimestamp))
			, name   = "channelSync"
			, param  = \()->((), ())
			, read   = id
			, writel = SDSWriteConst \_ w->Ok (?Just w)
			, writer = SDSWriteConst \_ _->Ok ?None
			}

		handlers :: ConnectionHandlers (Timestamp, [Int]) (Channels, Timestamp) Channels
		handlers = { ConnectionHandlers
			| onConnect     = onConnect
			, onData        = onData
			, onShareChange = onShareChange
			, onDisconnect  = onDisconnect
			, onDestroy     = onDestroy
			}
		where
			onConnect :: !IOHandle !String !(Channels, Timestamp) -> (!MaybeErrorString (Timestamp, [Int]), !?Channels, ![String], !Bool)
			onConnect _ addr (ch, ts) = postMessages (ts, []) ch

			postMessages :: !a !Channels -> (!MaybeErrorString a, ?Channels, [String], Bool)
			postMessages res (msgs, send, stop) =
				( Ok res
				, if (send =: []) ?None (?Just (msgs, [], stop))
				, [toString (gCSerialise{|*|} s [])\\s<-send]
				, stop
				)

			onData :: !IOHandle !String !(Timestamp, [Int]) !(Channels, Timestamp) -> (MaybeErrorString (?(Timestamp, [Int])), ?Channels, [String], Bool)
			onData _ newdata (lastPing, acc) ((msgs,send,ss), ts)
				# acc = acc ++ fromString newdata
				= case runStateT (deserialiseMultiple listTop []) acc of
					Left error = (Error (toString error), ?None, [], True)
					Right ([], newacc)
						= (Ok (?Just (lastPing, newacc)), ?None, [], ss)
					Right (newmsgs, newacc)
						# lastPing = last [lastPing:[ts\\MTFPing<-newmsgs]]
						| isJust pingTimeout && lastPing <> Timestamp 0 && toInt ts - toInt lastPing > fromJust pingTimeout
							= (Error "Ping timeout", ?None, [], True)
						= (Ok (?Just (lastPing, newacc)), ?Just (msgs++newmsgs, send, ss), [], ss)

			onShareChange :: !(Timestamp, [Int]) !(Channels, Timestamp) -> (!MaybeErrorString (?(Timestamp, [Int])), !?Channels, ![String], !Bool)
			onShareChange (lastPing, _) (ch, ts)
				| isJust pingTimeout && lastPing <> Timestamp 0 && toInt ts - toInt lastPing > fromJust pingTimeout
					= (Error "Ping timeout", ?None, [], True)
				| otherwise = postMessages ?None ch

			onDisconnect :: !IOHandle !(Timestamp, [Int]) !(Channels, Timestamp) -> (!MaybeErrorString (?(Timestamp, [Int])), ?Channels)
			onDisconnect _ _ _ = (Ok (?Just (Timestamp 0, [])), ?Just ([], [], True))

			onDestroy :: !(Timestamp, [Int]) -> (!MaybeErrorString (?(Timestamp, [Int])), ![String])
			onDestroy _ = (Ok (?Just (Timestamp 0, [])), [toString (gCSerialise{|*|} MTTShutdown [])])
