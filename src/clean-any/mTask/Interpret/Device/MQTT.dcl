definition module mTask.Interpret.Device.MQTT

/**
 * Communication specification for MQTT
 */

from mTask.Interpret.Device import class channelSync
from iTasks.WF.Definition import class iTask, :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Extensions.MQTT import :: MQTTAuth

//** MQTT communication specifications
:: MQTTSettings = 
	{ host  :: String
	//** Host name
	, port  :: Int
	//** Port number
	, mcuId :: String
	//** Identifier for the device
	, serverId :: String
	//** Identifier for the server
	, auth :: MQTTAuth
	//** Authentication type
	}
derive class iTask MQTTSettings

//** Generate a fresh server ID
generateServerId :: Task String

instance channelSync MQTTSettings
