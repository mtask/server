definition module mTask.Interpret.Device.Serial

/**
 * Communication specification for TTY devices (serial port communication)
 */

from mTask.Interpret.Device import class channelSync
from StdOverloaded import class zero
from System.Serial import getTTYDevices, :: TTYSettings(..), :: Parity(..), :: BaudRate(..), :: ByteSize(..), instance zero TTYSettings
import iTasks.Extensions.Serial

instance channelSync TTYSettings
