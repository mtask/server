definition module mTask.Interpret.Device.Serial

/**
 * Communication specification for TTY devices (serial port communication)
 */

from StdOverloaded import class zero
from System.Serial import getTTYDevices, :: TTYSettings(..), :: Parity(..), :: BaudRate(..), :: ByteSize(..), instance zero TTYSettings
import iTasks.Extensions.Serial

from mTask.Interpret.Device import class channelSync

instance channelSync TTYSettings
