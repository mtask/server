definition module mTask.Interpret.Device.TCP

/**
 * Communication specification for TCP devices
 */

from mTask.Interpret.Device import class channelSync
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

/**
 * TCP communication specification
 */
:: TCPSettings =
	{ host :: String
	//** host name
	, port :: Int
	//** port number
	, pingTimeout :: ?Int
	//** Require a ping signal every so many seconds
	}
derive class iTask TCPSettings

instance channelSync TCPSettings
