definition module mTask.Interpret.Device.TCP

/**
 * Communication specification for TCP devices
 */

from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState
from iTasks import :: Port
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.WF.Definition import class iTask

from mTask.Interpret.Device import class channelSync

/**
 * TCP communication specification
 */
:: TCPSettings =
	{ host :: String
	//** host name
	, port :: Port
	//** port number
	, pingTimeout :: ?Int
	//** Require a ping signal every so many seconds
	}
derive class iTask TCPSettings

instance channelSync TCPSettings
