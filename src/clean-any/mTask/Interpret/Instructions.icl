implementation module mTask.Interpret.Instructions

import Control.Applicative
import Control.GenBimap
import Control.Monad => qualified join
import Data.Array
import Data.Either
import Data.Func
import Data.Functor
import Data.UInt
import StdEnv
import Text
import Text.HTML
import iTasks => qualified return, forever, sequence

import mTask.Language
import mTask.Interpret.String255

import Data.GenType
import Data.GenType.CSerialise

derive class iTask BCInstrs, BCInstr, BCTaskType, JumpLabel

instance < JumpLabel where (<) (JL i) (JL j) = i < j
instance == JumpLabel where (==) (JL i) (JL j) = i == j

derive gType BCInstrs, BCInstr, BCTaskType, JumpLabel, PinMode, InterruptMode
derive gCSerialise BCInstr, BCTaskType, JumpLabel, PinMode, InterruptMode
derive gCDeserialise BCInstr, BCTaskType, JumpLabel, PinMode, InterruptMode

gCSerialise{|BCInstrs|} (BCIs s)
	# r = foldrArr gCSerialise{|*|} [] s
	= \c->gCSerialise{|*|} (UInt16 (length r)) (r ++ c)
gCDeserialise{|BCInstrs|} top = \st->
	case deserialiseN (UInt16 0) gCDeserialise{|*|} gCDeserialise{|*|} top st of
		Left e = Left e
		Right (cs, st) = case deserialiseMultiple listTop (tl [BCReturn1_0]) (map fromChar cs) of
			Left e = Left e
			Right (is, []) = Right (BCIs {i\\i<-is}, st)
			Right (is, e) = Left (CDUser "Error parsing bytecode instructions, input not exhausted")

bcinstrsgType :: (String, [String], String -> [String], String -> [String])
bcinstrsgType =
	( gTypeName (gTypeForValue (BCIs {}))
	, ["typedef struct BCInstrs { uint16_t size; uint8_t *elements; } BCInstrs;"]
	, \r->
		[ r +++ ".size = get()<<8;\n"
		, r +++ ".size += get();\n"
		, r +++ ".elements = alloc(" +++ r +++ ".size);\n"
		, "for (uint16_t i = 0; i<" +++ r +++ ".size; i++) {\n"
		, "\t" +++ r +++ ".elements[i] = get();\n"
		, "}\n"]
	, \r->
		[ "put(" +++ r +++ ".size >> 8);\n"
		, "put(" +++ r +++ ".size);\n"
		, "for (uint16_t i = 0; i<" +++ r +++ ".size; i++) {\n"
		, "\tput(" +++ r +++ ".elements[i]);\n"
		, "}\n"])

bytewidth :: BCInstr -> Int
bytewidth i = length (gCSerialise{|*|} i [])

debugInstructions :: [BCInstr] -> [(String, String, [String])]
debugInstructions d = pprint 0 d
where
	pprint :: Int [BCInstr] -> [(String, String, [String])]
	pprint i [] = []
	pprint i [BCLabel l:is] = [("","",[]),("  ","BCLabel", [toString l]):pprint i is]
	pprint i [ins:is] =
		[let (c,a) = printInstr ins in (toString i +++ ": ", c,a)
		:pprint (i+bytewidth ins) is]

formatDebugInstructions :: [(String, String, [String])] -> HtmlTag
formatDebugInstructions is
	= TableTag []
		[ TbodyTag []
			[TrTag []
				[ TdTag  [] [Text n]
				, TdTag  [] [PreTag [] [Text i]]
				: [TdTag [] [PreTag [] [Text a]]\\a<-args]
				]
			\\(n, i, args)<-is
			]
		]

instance toString JumpLabel where toString (JL i) = toString i
generic gPrintInstr a :: a [String] -> [String]
derive gPrintInstr BCInstr, UInt8, UInt16, JumpLabel, BCTaskType, PinMode, InterruptMode
gPrintInstr{|OBJECT|} f (OBJECT a) c = f a c
gPrintInstr{|EITHER|} f _ (LEFT a) c = f a c
gPrintInstr{|EITHER|} _ f (RIGHT a) c = f a c
gPrintInstr{|FIELD|} f (FIELD a) c = f a c
gPrintInstr{|RECORD|} f (RECORD a) c = f a c
gPrintInstr{|UNIT|} _ c = c
gPrintInstr{|CONS of {gcd_name}|} f (CONS a) c = [gcd_name:f a c]
gPrintInstr{|PAIR|} l r (PAIR x y) c = l x (r y c)
gPrintInstr{|Int|} i c = [toString i:c]
gPrintInstr{|Real|} r c = [toString r:c]
gPrintInstr{|Bool|} i c = [toString i:c]
gPrintInstr{|String|} i c = [i:c]
gPrintInstr{|String255|} (String255 i) c = [join " " [toString (toInt i)\\i<-:i]:c]

printInstr :: BCInstr -> (String, [String])
printInstr c
	# [c:as] = gPrintInstr{|*|} c []
	= (c, as)
