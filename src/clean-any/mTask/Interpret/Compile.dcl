definition module mTask.Interpret.Compile

/**
 * Functions regarding the actual compilation
 */

from StdOverloaded import class zero
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either
from iTasks.WF.Definition import class iTask, :: TaskValue, :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from Data.GenDefault import generic gDefault

from mTask.Language import :: Main, class type
from mTask.Interpret.DSL import :: BCInterpret, :: MTLens, :: BCState, :: SDSLens, :: StateT, :: WriterT, :: Identity
from mTask.Interpret.Peripheral import :: BCPeripheral
from mTask.Interpret.ByteCodeEncoding import generic fromByteCode, generic toByteCode, class toByteWidth, :: FBC
from mTask.Interpret.Instructions import :: BCInstr

from mTask.Interpret.String255 import :: String255
from Data.UInt import :: UInt8

//** Compilation options
:: CompileOpts =
	{ tailcallopt  :: Bool
	//** Optimise tailcalls
	, labelresolve :: Bool
	//** Rewrite jumps to labels to jumps to locations
	, shorthands   :: Bool
	//** Use shorthand instructions, shortening the bytecode
	}

instance zero CompileOpts

/**
 * Compile the given mTask task using the compilation options provided
 *
 * @param compilation options
 * @param mTask task
 * @result return value
 * @result shared data sources
 * @result peripherals
 * @result instructions
 */
compileOpts :: CompileOpts (Main (BCInterpret (TaskValue v))) -> (String255, [(? MTLens, Task BCShareSpec)], {BCPeripheral}, [BCInstr]) | type v

//** Internal structure for an SDS used in the RTS
:: BCShareSpec =
	{ bcs_ident  :: UInt8
	//** sds id
	, bcs_itasks :: Bool
	//** is it lifted?
	, bcs_value  :: String255
	//** current value
	}

derive gCSerialise BCShareSpec
derive gCDeserialise BCShareSpec
derive class iTask BCShareSpec
