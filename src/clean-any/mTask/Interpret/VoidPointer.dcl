definition module mTask.Interpret.VoidPointer

/**
 * Voidpointer type for C code generation.
 */

from Data.Either import :: Either
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.WF.Definition import class iTask
from Data.GenType import generic gType, :: Box, :: GType
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either

//** Datatype, it does not contain data, it only represents a void pointer in the generated C code
:: VoidPointer =: VoidPointer ()
/**
 * Constructor of the newtype that can be used curried
 *
 * @type VoidPointer
 */
vp :== VoidPointer ()
derive gType VoidPointer
derive gCSerialise VoidPointer
derive gCDeserialise VoidPointer
derive class iTask VoidPointer

//* Helper function for C-code generation of void pointers (see {{gCSerialise}})
voidPointergType :: (String, [String], String -> [String], String -> [String])
