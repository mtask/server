implementation module mTask.Interpret.DSL

import StdEnv

import qualified Data.Map as DM

import Data.Error
import Data.Either
import Data.Func
import Data.Functor
import Data.Functor.Identity
import Data.List
import Data.Tuple
import Data.Maybe
import Control.Applicative
import Control.Monad
import Control.Monad.Writer
import Control.Monad.State
import Control.Monad.Trans
import System.Time

import mTask.Language
import mTask.Interpret.Instructions
import mTask.Interpret.ByteCodeEncoding
import Data.UInt
import mTask.Interpret.Peripheral
import mTask.Interpret.String255

from iTasks.SDS.Combinators.Common import mapReadWriteError
from iTasks.SDS.Definition import :: SDSReducer, :: Shared
from iTasks.Internal.SDS import
	instance Identifiable SDSLens,
	instance Readable SDSLens,
	instance Writeable SDSLens,
	instance Modifiable SDSLens,
	instance Registrable SDSLens

cast1 :: (m a) -> a
cast1 _ = undef

mainBC :: (Main (BCInterpret v)) BCState -> BCState
mainBC {main} st
	# ((a, st), w) = runIdentity $ runWriterT $ runStateT main st
	= {st & bcs_mainexpr=w}

instance zero BCState
where
	zero =
		{ BCState
		| bcs_infun      = (JL zero)
		, bcs_mainexpr   = []
		, bcs_context    = []
		, bcs_functions  = 'DM'.newMap
		, bcs_freshlabel = (JL one)
		, bcs_sdses      = 'DM'.newMap
		, bcs_hardware   = []
		}

tell` :: [BCInstr] -> BCInterpret a
tell` w = liftT (tell w) >>| pure undef

binop :: (v a) BCInstr BCInstr BCInstr -> BCInstr | type a
binop a i l r = if (toByteWidth (unpack a) == 1) i
	(if (isReal (cast1 a)) r l)

isReal :: a -> Bool | TC a
isReal a = case dynamic a of
	(a :: Real) = True
	_ = False

freshlabel :: BCInterpret JumpLabel
freshlabel = getState >>= \s=:{bcs_freshlabel=(JL i)}->
	put {s & bcs_freshlabel=JL (i+one)} >>| pure (JL i)

freshsds :: BCInterpret Int
freshsds = gets \s->'DM'.mapSize s.bcs_sdses

setRate :: Bool (TimingInterval (StateT BCState (WriterT [BCInstr] Identity))) -> BCInterpret a
setRate rl Default        = pure undef
setRate rl (BeforeMs u)   = tell` [BCPush $ fromString $ toByteCode{|*|} 0] >>| u >>| tell` [BCTuneRateMs]
setRate rl (BeforeSec u)  = tell` [BCPush $ fromString $ toByteCode{|*|} 0] >>| u >>| tell` [BCTuneRateSec]
setRate rl (ExactMs t)    = tell` if rl [BCMkTask BCRateLimit] [] >>| t >>| t >>| tell` [BCTuneRateMs]
setRate rl (ExactSec t)   = tell` if rl [BCMkTask BCRateLimit] [] >>| t >>| t >>| tell` [BCTuneRateSec]
setRate rl (RangeMs u l)  = tell` if rl [BCMkTask BCRateLimit] [] >>| u >>| l >>| tell` [BCTuneRateMs]
setRate rl (RangeSec u l) = tell` if rl [BCMkTask BCRateLimit] [] >>| u >>| l >>| tell` [BCTuneRateSec]

instance aio (StateT BCState (WriterT [BCInstr] Identity))
where
	readA` i p = p >>| tell` [BCMkTask BCReadA] >>| setRate True i
	writeA p v = p >>| v >>| tell` [BCMkTask BCWriteA]

instance expr (StateT BCState (WriterT [BCInstr] Identity))
where
	lit   t   = tell` [BCPush $ fromString $ toByteCode{|*|} t]
	(+.)  a b = a >>| b >>| tell` [binop a BCAddI BCAddL BCAddR]
	(-.)  a b = a >>| b >>| tell` [binop a BCSubI BCSubL BCSubR]
	(*.)  a b = a >>| b >>| tell` [binop a BCMultI BCMultL BCMultR]
	(/.)  a b = a >>| b >>| tell` [binop a BCDivI BCDivL BCDivR]
	(&.)  a b = a >>| b >>| tell` [BCAnd]
	(|.)  a b = a >>| b >>| tell` [BCOr]
	Not   a   = a >>|       tell` [BCNot]
	(==.) a b = a >>| b >>| tell` [binop a BCEqI  BCEqL BCEqL]   // there is no more meaningful equality for reals than the long equality
	(!=.) a b = a >>| b >>| tell` [binop a BCNeqI BCNeqL BCNeqL] // there is no more meaningful equality for reals than the long equality
	(<.)  a b = a >>| b >>| tell` [binop a BCLeI  BCLeL BCLeR]
	(>.)  a b = a >>| b >>| tell` [binop a BCGeI  BCGeL BCGeR]
	(<=.) a b = a >>| b >>| tell` [binop a BCLeqI BCLeqL BCLeqR]
	(>=.) a b = a >>| b >>| tell` [binop a BCGeqI BCGeqL BCGeqR]
	If c t e = freshlabel >>= \elselabel->freshlabel >>= \endiflabel->
		c >>| tell` [BCJumpF elselabel] >>|
		t >>| tell` [BCJump endiflabel,BCLabel elselabel] >>|
		e >>| tell` [BCLabel endiflabel]

instance tupl (StateT BCState (WriterT [BCInstr] Identity))
where
	first t  = censorListen t >>= \(_, is)->tell` if (onlyArg is)
		(take (toByteWidth $ fst $ unpack t) is)
		(is ++ [ BCPop $ UInt8 (toByteWidth $ snd $ unpack $ t)])
	second t = censorListen t >>= \(_, is)->tell` if (onlyArg is)
		(drop (toByteWidth $ fst $ unpack t) is)
		(is ++ [ BCRot (UInt8 (toByteWidth $ unpack t)) $ UInt8 (toByteWidth $ snd $ unpack t)
		       , BCPop $ UInt8 (toByteWidth $ fst $ unpack t)])
	tupl a b = liftM2 tuple a b

instance dio v (StateT BCState (WriterT [BCInstr] Identity))
where
	readD` i p = p >>| tell` [BCMkTask BCReadD] >>| setRate True i
	readD p = p >>| tell` [BCMkTask BCReadD]
	writeD p v = p >>| v >>| tell` [BCMkTask BCWriteD]

instance pinMode  (StateT BCState (WriterT [BCInstr] Identity))
where
	pinMode mode pin = mode >>| pin >>| tell` [BCMkTask BCPinMode]

instance interrupt (StateT BCState (WriterT [BCInstr] Identity))
where
	interrupt mode pin = mode >>| pin >>| tell` [BCMkTask BCInterrupt]

instance delay (StateT BCState (WriterT [BCInstr] Identity))
where
	delay m = m >>| tell` [BCMkTask BCDelay]

instance rpeat (StateT BCState (WriterT [BCInstr] Identity))
where
	rpeatEvery i m = m >>| tell` [BCMkTask BCRepeat] >>| setRate False i

instance rtrn (StateT BCState (WriterT [BCInstr] Identity))
where
	rtrn m = m >>| tell` (map BCMkTask $ bcstable $ toByteWidth $ unpack m)

instance sds (StateT BCState (WriterT [BCInstr] Identity))
where
	sds def = {main = freshsds
			>>= \sdsi->
				let sds = modify (addSdsIfNotExist sdsi (Left $ String255 (toByteCode{|*|} t)))
						>>| pure (Sds sdsi)
				    (t In e) = def sds
				in e.main
		}
	getSds` ti f 
		= f
		>>= \(Sds i)-> tell` [BCMkTask (BCSdsGet (fromInt i))]
		>>| setRate True ti
	setSds f v = f >>= \(Sds i)->v >>| tell`
		(  map BCMkTask (bcstable $ toByteWidth $ unpack v)
		++ [BCMkTask (BCSdsSet (fromInt i))])
	updSds f v
		=   f >>= \(Sds i)->freshlabel >>= \funlab->gets (\s->s.bcs_context)
		>>= \ctx->let argwidth = casta v (abort "updSds(1) casta undef") in
		//Possibly add the context
		    tell` (if (ctx =: [])
			(map BCMkTask $ bcstable 0)
			//The context is just the arguments up till now in reverse
			(  [BCArg (UInt8 i)\\i<-reverse (indexList ctx)]
			++ map BCMkTask (bcstable $ length ctx)
			)
		)
		>>| addToCtx funlab zero argwidth
		>>| liftFunction funlab (argwidth + fromInt (length ctx))
			(rtrn $ v $ retrieveArgs funlab zero argwidth)
			//Always returns a task tree for saving
			(?Just one)
		>>| modify (\s->{s & bcs_context=ctx})
		>>| tell` [BCMkTask (BCSdsUpd (fromInt i) funlab)]
	where
		casta :: ((m a) -> b) a -> UInt8 | toByteWidth a
		casta _ a = UInt8 (toByteWidth a)

instance liftsds  (StateT BCState (WriterT [BCInstr] Identity))
where
	liftsds def = {main = freshsds
			>>= \sdsi->
				let sds = modify (addSdsIfNotExist sdsi (Right $ lens t))
						>>| pure (Sds sdsi)
				    (t In e) = def sds
				in e.main
		}
	where
		lens :: ((Shared sds a) -> MTLens) | type, iTask a & RWShared sds
		lens = mapReadWriteError
			( Ok o fromString o toByteCode{|*|}
			, \w r-> ?Just <$> iTasksDecode (toString w)
			) ?None

addSdsIfNotExist :: Int (Either String255 MTLens) BCState -> BCState
addSdsIfNotExist sdsid sds s = {s & bcs_sdses=addMapIfNotExist sdsid sds s.bcs_sdses}

addMapIfNotExist :: k v ('DM'.Map k v) -> 'DM'.Map k v | < k
addMapIfNotExist k v m = if ('DM'.member k m) m ('DM'.put k v m)

import StdDebug, Text.GenPrint
derive gPrint BCInstr, UInt8, UInt16, BCTaskType, JumpLabel, String255, PinMode, InterruptMode

addToCtx :: JumpLabel UInt8 UInt8 -> BCInterpret ()
addToCtx (JL sl) fro to = modify \s->
	{s & bcs_context=s.bcs_context ++ [BCStepArg sl i\\i<-[fro..to-one]]}

clearCtx :: BCInterpret ()
clearCtx = modify \s->{s & bcs_context=[]}

/*
 * LHS of the step must be already executed
 *
 * @param width of the lhs
 * @param width of the rhs
 * @param Task type instruction constructor
 * @param Function creating the continuation function
 */
makeStep :: UInt8 UInt8 (UInt8 JumpLabel -> BCTaskType) (JumpLabel UInt8 -> BCInterpret b)
	-> BCInterpret c | toByteWidth b
makeStep lhswidth rhswidth instr contfun
	//Fetch a fresh label
	=   freshlabel
	//Fetch the context
	>>= \funlab->gets (\s->s.bcs_context)
	//Possibly add the context
	>>= \ctx->tell` (if (ctx =: [])
			[]
			//The context is just the arguments up till now in reverse
			(  [BCArg (UInt8 i)\\i<-reverse (indexList ctx)]
			++ map BCMkTask (bcstable $ length ctx)
			++ [BCMkTask BCTAnd]
			)
		)
	//Increase the context
	>>| addToCtx funlab zero lhswidth
	//Lift the step function
	>>| liftFunction
			//Fresh label
			funlab
			//Width of the arguments is the width of the lhs plus the
			//stability plus the context
			(one + lhswidth + (UInt8 (length ctx)))
			//Body     label  ctx width            continuations
			(contfun funlab (UInt8 (length ctx)))
			//(toContFun funlab (UInt8 (length ctx)) cont)
			//Return width (always 1, a task pointer)
			(?Just one)
	>>| modify (\s->{s & bcs_context=ctx})
	>>| tell` [BCMkTask $ instr rhswidth funlab]

instance step (StateT BCState (WriterT [BCInstr] Identity))
where
	(>>*.) lhs cont = lhs >>| makeStep lhswidth rhswidth BCStep toContFun
	where
		lhswidth = UInt8 (toByteWidth $ unpack $ unpack lhs)
		rhswidth = UInt8 (toByteWidth $ unpack $ unpack cont)

		rhsval :: [Step v a b] b -> b | toByteWidth b
		rhsval _ i = i

		toContFun steplabel contextwidth
			= foldr tcf (tell` [BCPush $ fromString $ toByteCode{|*|} MT_NULL]) cont
		where
			tcf (IfStable f t)
				= If ((stability >>| tell` [BCIsStable]) &. f val)
					(t val >>| tell` [])
			tcf (IfUnstable f t)
				= If ((stability >>| tell` [BCIsUnstable]) &. f val)
					(t val >>| tell` [])
			tcf (IfNoValue t)
				= If (stability >>| tell` [BCIsNoValue])
					(t >>| tell` [])
			tcf (IfValue f t)
				= If ((stability >>| tell` [BCIsValue]) &. f val)
					(t val >>| tell` [])
			tcf (Always f) = const f

			stability = tell` [BCArg $ lhswidth + contextwidth]
			val = retrieveArgs steplabel zero lhswidth

	(>>=.) lhs rhs = lhs >>| makeStep lhswidth rhswidth BCStepStable toContFun
	where
		lhswidth = UInt8 (toByteWidth $ unpack $ unpack lhs)
		rhswidth = UInt8 (toByteWidth $ unpack $ unpack $ rhs undef)

		toContFun funlab ctxwidth = rhs (retrieveArgs funlab zero lhswidth)

	(>>~.) lhs rhs = lhs >>| makeStep lhswidth rhswidth BCStepUnstable toContFun
	where
		lhswidth = UInt8 (toByteWidth $ unpack $ unpack lhs)
		rhswidth = UInt8 (toByteWidth $ unpack $ unpack $ rhs undef)

		toContFun funlab ctxwidth = rhs (retrieveArgs funlab zero lhswidth)

	// Only take the short route if there is no recursive call
	(>>|.) lhs rhs
		=         lhs
		>>|       getState
		>>= \bcs->censorListen rhs
		>>= \(_, is)
			//Recursive call so we must make the step lazy
			| any (calls bcs.bcs_infun) is
				= makeStep lhswidth rhswidth BCStepStable \_ _->rhs
			= rhs >>| tell` [BCMkTask $ BCSeqStable rhswidth]
	where
		lhswidth = UInt8 (toByteWidth $ unpack $ unpack lhs)
		rhswidth = UInt8 (toByteWidth $ unpack rhs)

	(>>..) lhs rhs
		=         lhs
		>>|       getState
		>>= \bcs->censorListen rhs
		>>= \(_, is)
			//Recursive call so we must make the step lazy
			| any (calls bcs.bcs_infun) is
				= makeStep lhswidth rhswidth BCStepUnstable \_ _->rhs
			= rhs >>| tell` [BCMkTask $ BCSeqUnstable rhswidth]
	where
		lhswidth = UInt8 (toByteWidth $ unpack $ unpack lhs)
		rhswidth = UInt8 (toByteWidth $ unpack rhs)

calls :: JumpLabel BCInstr -> Bool
calls jl (BCTailcall _ _ jl`) = jl == jl`
calls jl (BCJumpSR _ jl`) = jl == jl`
calls jl (BCJumpF jl`) = jl == jl`
calls jl (BCJump jl`) = jl == jl`
calls jl _ = False

/*
 * Function to get an argument getter
 *
 * @param label of where the abstraction took place
 * @param first argument
 * @param last argument
 */
retrieveArgs :: JumpLabel UInt8 UInt8 -> BCInterpret a
retrieveArgs (JL sl) fro to = gets (\s->s.bcs_context)
	>>= \ctx->tell` [BCArg (findarg i ctx zero)\\i<-reverse [fro..to-one]]
where
	findarg :: UInt8 [BCInstr] UInt8 -> UInt8
	findarg ntharg [] acc = abort "steparg: unknown"
	findarg ntharg [BCStepArg steplabel a:xs] acc
		| steplabel == sl && a == ntharg = acc
		= findarg ntharg xs (inc acc)
	findarg ntharg [x:_] acc = abort ("steparg: malformed: " +++ printToString x)

onlyArg :: [BCInstr] -> Bool
onlyArg [] = True
onlyArg [BCArg _:xs] = onlyArg xs
onlyArg _ = False

instance unstable (StateT BCState (WriterT [BCInstr] Identity))
where
	unstable m = m >>| tell` (map BCMkTask $ bcunstable $ toByteWidth $ unpack m) 

instance .&&. (StateT BCState (WriterT [BCInstr] Identity))
where
	(.&&.) lhs rhs = lhs >>| rhs >>| tell` [BCMkTask BCTAnd] 

instance .||. (StateT BCState (WriterT [BCInstr] Identity))
where
	(.||.) lhs rhs = lhs >>| rhs >>| tell` [BCMkTask BCTOr]

infun :: JumpLabel (BCInterpret a) -> BCInterpret a
infun jl m = getState >>= \os->
	put {os & bcs_infun=jl} *> m <* modify (\b->{b & bcs_infun=os.bcs_infun})

/*
 * Creating a function:
 *
 * - fetching a fresh label
 * - give the definition a function call argument with the label
 * - from the return value we can deduce the argument width
 * - add the arguments to the context
 * - lift the function (add it to the state)
 * - execute the body
 */
instance fun () (StateT BCState (WriterT [BCInstr] Identity))
where
	fun def = {main
			=   freshlabel >>= \funlabel->
			let (g In m) = def \()->callFunction funlabel zero []
			in infun funlabel
				(liftFunction funlabel zero (g ()) ?None)
			>>| clearCtx >>| m.main
		}

instance fun
	(StateT BCState (WriterT [BCInstr] Identity) a)
	(StateT BCState (WriterT [BCInstr] Identity)) | type a
where
	fun def = {main=freshlabel >>= \funlabel->
			let (g In m) = def \a->callFunction funlabel (UInt8 (toByteWidth $ unpack a)) [a]
			    argwidth = casta g (abort "fun(1) casta  undef")
			in  addToCtx funlabel zero argwidth
			>>| infun funlabel
				(liftFunction funlabel argwidth
					(g (retrieveArgs funlabel zero argwidth)
					) ?None)
			>>| clearCtx >>| m.main
		}
	where
		casta :: ((m a) -> b) a -> UInt8 | toByteWidth a
		casta _ a = UInt8 (toByteWidth a)

instance fun
	( StateT BCState (WriterT [BCInstr] Identity) a
	, StateT BCState (WriterT [BCInstr] Identity) b
	) (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b
where
	fun def = {main=freshlabel >>= \funlabel->
			let (g In m)  = def \(a, b)->
					callFunction funlabel
						(UInt8 (toByteWidth (unpack a) + toByteWidth (unpack b)))
						[a, b >>| tell` []]
			    arg1width = casta g (abort "fun(2) casta undef")
			    arg2width = casta (\(a, b)->g (b, a)) (abort "fun(2) castb undef")
			in  addToCtx funlabel zero (arg1width + arg2width)
			>>| infun funlabel
				(liftFunction funlabel (arg1width + arg2width)
					(g ( retrieveArgs funlabel zero arg1width
					   , retrieveArgs funlabel arg1width (arg1width+arg2width)
					)) ?None)
			>>| clearCtx >>| m.main
		}
	where
		casta :: ((m a, b) -> c) a -> UInt8 | toByteWidth a
		casta _ a = UInt8 (toByteWidth a)

instance fun
	( StateT BCState (WriterT [BCInstr] Identity) a
	, StateT BCState (WriterT [BCInstr] Identity) b
	, StateT BCState (WriterT [BCInstr] Identity) c
	) (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b & type c
where
	fun def = {main=freshlabel >>= \funlabel->
			let (g In m)  = def \(a, b, c)->
					callFunction funlabel
						(UInt8 (toByteWidth (unpack a) + toByteWidth (unpack b) + toByteWidth (unpack c)))
						[a, b >>| tell` [], c >>| tell` []]
				arg1width = casta g (abort "fun(3) casta undef")
				arg2width = casta (\(a,b,c)->g (b,a,c)) (abort "fun(3) castb undef")
				arg3width = casta (\(a,b,c)->g (c,a,b)) (abort "fun(3) castc undef")
			in  addToCtx funlabel zero (arg1width + arg2width + arg3width)
			>>| infun funlabel
				(liftFunction funlabel (arg1width + arg2width + arg3width)
					(g ( retrieveArgs funlabel zero arg1width
					   , retrieveArgs funlabel arg1width (arg1width+arg2width)
					   , retrieveArgs funlabel
							(arg1width+arg2width) (arg1width+arg2width+arg3width)
					)) ?None)
			>>| clearCtx >>| m.main
		}
	where
		casta :: ((m a, b, c) -> d) a -> UInt8 | toByteWidth a
		casta _ a = UInt8 (toByteWidth a)

callFunction :: JumpLabel UInt8 [BCInterpret b] -> BCInterpret c
callFunction label arity args
	=   tell` [BCPushPtrs]
	>>| sequence (reverse args)
	>>| tell` [BCJumpSR arity label]

liftFunction :: JumpLabel UInt8 (BCInterpret a) (?UInt8) -> BCInterpret () | toByteWidth a
liftFunction label awidth m mw
# rwidth = maybe (UInt8 (toByteWidth $ unpack m)) id mw
= censorListen m                >>= \(_, funinstructions)->
	getState                    >>= \s=:{bcs_functions}->
	put {s & bcs_functions='DM'.put label
		{ bcf_instructions =  [BCLabel label:funinstructions]
		                   ++ [BCReturn rwidth awidth]
		, bcf_argwidth     = awidth
		, bcf_returnwidth  = rwidth
		} s.bcs_functions}

//Write in terms of monadtrans etc
censorListen :: ((BCInterpret a) -> BCInterpret (a, [BCInstr]))
censorListen = mapStateT
	(fmap (\((a, s), w)->((a, w), s)) o censor (const []) o listen)

bcstable :: Int -> [BCTaskType]
bcstable i = bcfork i BCStableNode
	[BCStable0, BCStable1, BCStable2, BCStable3, BCStable4]

bcunstable :: Int -> [BCTaskType]
bcunstable i = bcfork i BCUnstableNode
	[BCUnstable0, BCUnstable1, BCUnstable2, BCUnstable3, BCUnstable4]

bcfork :: Int (UInt8 -> BCTaskType) [BCTaskType] -> [BCTaskType]
bcfork n node tt
	| n <= 4 = [tt !! n]
	= bcfork (n - 2) node tt ++ [node $ fromInt $ n - 2]

instance int  (StateT BCState (WriterT [BCInstr] Identity)) Int
where int i = i
instance int  (StateT BCState (WriterT [BCInstr] Identity)) Real
where int i = i >>| tell` [BCRtoI]
instance int  (StateT BCState (WriterT [BCInstr] Identity)) Long
where int i = i >>| tell` [BCLtoI]

instance real  (StateT BCState (WriterT [BCInstr] Identity)) Int
where real i = i >>| tell` [BCItoR]
instance real  (StateT BCState (WriterT [BCInstr] Identity)) Real
where real i = i
instance real  (StateT BCState (WriterT [BCInstr] Identity)) Long
where real i = i >>| tell` [BCLtoR]

instance long  (StateT BCState (WriterT [BCInstr] Identity)) Int
where long i = i >>| tell` [BCItoL]
instance long  (StateT BCState (WriterT [BCInstr] Identity)) Real
where long i = i >>| tell` [BCRtoL]
instance long  (StateT BCState (WriterT [BCInstr] Identity)) Long
where long i = i
