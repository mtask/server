implementation module mTask.Interpret.Devices

import StdEnv
import Data.Func
import iTasks
import mTask.Interpret.Device
import mTask.Interpret.Device.Serial
import mTask.Interpret.Device.TCP
import mTask.Interpret.Device.MQTT
import iTasks.Extensions.MQTT

derive class iTask MTD
instance channelSync MTD where
	channelSync (MQTT t) ch = channelSync t ch
	channelSync (TCP t) ch = channelSync t ch
	channelSync (Serial t) ch = channelSync t ch
instance zero MTD where zero = TCP {TCPSettings|port=8123,host="localhost",pingTimeout= ?None}

enterDevice :: Task MTD
enterDevice
	=   generateServerId >>- \si -> tune (Title "Enter Device")
	$   tune ArrangeHorizontal
	$   let preset = presets si in updateChoice [ChooseFromList fst] preset (hd preset) <<@ Title "Select a preset" 
	>&> flip whileUnchanged (tune (Title "Update the preset") o updateInformation []) o mapRead (maybe zero snd)
where
	presets serverId =
		[("Demo device Windows Bluetooth (HC-06)", Serial {zero & devicePath="COM5", baudrate=B9600})
		,("Demo device Linux Bluetooth (HC-06)", Serial {zero & devicePath="/dev/rfcomm0", baudrate=B9600})
		,("Arduino UNO", Serial {zero & devicePath="/dev/ttyACM0", baudrate=B38400})
		,("Chinese UNO", Serial {zero & devicePath="/dev/ttyUSB0", baudrate=B38400})
		,("Arduino UNO with Bluetooth (Itead)", Serial {zero & devicePath="/dev/rfcomm", baudrate=B38400})
		,("WEMOS LOLIN D1 Mini", TCP {TCPSettings | host="192.168.1.1xx", port=8123, pingTimeout = ?Just 3})
		,("Local client", TCP {TCPSettings | host="localhost", port=8123, pingTimeout= ?None})
		,("Generic TCP client", TCP {TCPSettings | host="", port=8123, pingTimeout= ?None})
		,("Generic MQTT client", MQTT {MQTTSettings | host="localhost", port=1883, mcuId="mcu", serverId=serverId, auth=NoAuth})
		]
