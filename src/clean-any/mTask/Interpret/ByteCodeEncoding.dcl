definition module mTask.Interpret.ByteCodeEncoding

/**
 * Encoding values to bytecode to make them usable in the interpreter
 */

import StdGeneric
from Control.Applicative import class Applicative, class pure, class <*>, class <*, class *>, class Alternative
from Control.Monad import class Monad
from Data.Either import :: Either
from Data.Error import :: MaybeError
from Data.Functor import class Functor
from Data.GenCons import generic conses
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from StdOverloaded import class toString
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.WF.Definition import :: TaskException, :: TaskValue, class iTask

from mTask.Language import :: Gesture, :: Pin, :: APin, :: DPin, :: Button, :: ButtonStatus, :: Long, class type, :: PinMode, :: InterruptMode
from Data.UInt import :: UInt8, :: UInt16, :: UInt32, :: Int8, :: Int16, :: Int32
from mTask.Interpret.String255 import :: String255
from mTask.Interpret.ByteCodeEncoding._FBC import :: FBC

/**
 * Convert a value to bytecode
 *
 * @var value
 * @param value
 * @result bytecode
 */
generic toByteCode a :: a -> String

derive toByteCode OBJECT, CONS of {gcd_index}, PAIR, EITHER, UNIT, RECORD, FIELD
derive toByteCode (), (,), (,,), TaskValue, Int, Bool, Char, Real, Long, Button, ButtonStatus, APin, DPin, Pin, UInt8, UInt16, UInt32, Int8, Int16, Int32, Gesture, PinMode, InterruptMode

/**
 * Retrieve the bytecode parser for a given type
 *
 * @var type
 * @result bytecode parser (see {{runFBC}})
 */
generic fromByteCode a *! :: FBC a

// Successful: (Right (Just a), String)
// Not enough bytes: (Right Nothing, String)
// Error: (Left String, String) (E.g. not existing constructor number)
/**
 * run a bytecode parser
 *
 * @param parser
 * @param input bytes
 * @result Either an error message or in case of succes a ?Just value, or in case of an exhausted input a ?None.
 * @result the remaining bytes
 */
runFBC :: (FBC a) -> ([Char] -> (Either String (? a), [Char]))
instance Functor FBC
instance pure FBC
instance <*> FBC
instance *> FBC
instance <* FBC
instance Alternative FBC
instance Monad FBC

/**
 * Helper function for lifting a value to an {{FBC}}
 *
 * @param value
 */
pure2 :: (Either String (? a), [Char]) -> FBC a

/**
 * Class to determine the width of a value in stack cells (16-bit)
 *
 * @var type
 * @param type
 * @result width in stack cells
 */
class toByteWidth a :: a -> Int
instance toByteWidth (a,b) | toByteWidth a & toByteWidth b
instance toByteWidth (a,b,c) | toByteWidth a & toByteWidth b & toByteWidth c
instance toByteWidth (TaskValue a) | toByteWidth a
instance toByteWidth Int, Bool, Char, (), Real, Long, Button, ButtonStatus, APin, DPin, Pin, UInt8, UInt16, Gesture, PinMode, InterruptMode

derive fromByteCode OBJECT, CONS of {gcd_index,gcd_name}, PAIR, EITHER, UNIT, RECORD, FIELD
derive fromByteCode (), (,), (,,), TaskValue, Int, Bool, Char, Real, Long, Button, ButtonStatus, APin, DPin, Pin, UInt8, UInt16, UInt32, Int8, Int16, Int32, Gesture, PinMode, InterruptMode

//** pack a real as a float in an integer
convert_real_to_float_in_int :: !Real -> Int
//** Unpack a float in an integer as a real
convert_float_in_int_to_real :: !Int -> Real

//** The bytecode parser that parses an enumeration type
fromBCADT :: FBC a | conses{|*|} a
//** The fail bytecode parser
fail :: String -> (FBC a)
//** The bytecode parser that returs the next character but doesn't pop it
peek :: FBC (? Char)
//** The bytecode parser that gobbles newlines
skipNL :: FBC ()
//** The bytecode parser that returns the next character
top :: FBC Char
//** The bytecode parser that returns the next N characters
ntop :: Int -> FBC String

/** The helper function that strips away one layer of types
 *
 * @param value (is not touched)
 * @result undef (correctly typed that is)
 */
unpack :: (m a) -> a

//* Run the bytecode parser for that type and yield the result or transform the error into an iTask exception 
iTasksDecode :: String -> MaybeError TaskException a | type a

//** Helper function to use {{gCSerialise{|*|}} as an instance for {{toByteCode}} TODO: use Clean's new default instances from generics for this
fromCSerialiseToToBytecode :: a -> String | gCSerialise{|*|} a
//** Helper function to use {{gCDeserialise{|*|}} as an instance for {{fromByteCode}} TODO: use Clean's new default instances from generics for this
fromCDeserialiseToFromBytecode :: FBC a | gCDeserialise{|*|} a
