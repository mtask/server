definition module mTask.Interpret.Peripheral

/**
 * Peripheral definitions
 *
 * This will be split up eventually
 */

from Data.Either import :: Either
from Data.GenEq import generic gEq
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.UInt import :: UInt8
from Data.VoidPointer import :: VoidPointer
from StdOverloaded import class toString
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.WF.Definition import class iTask

import mTask.Interpret.DSL
import mTask.Language

//** Peripheral options
:: BCPeripheral = { perinfo :: BCPeripheralInfo, st :: VoidPointer }
:: BCPeripheralInfo
	= BCDHT DHTInfo
	//** DHT
	| BCLEDMatrix LEDMatrixInfo
	//** LED Matrix
	| BCLightSensor LightSensorInfo
	//** Light sensor
	| BCAirQualitySensor AirQualitySensorInfo
	//** Air quality sensor
	| BCGestureSensor GestureSensorInfo
	//** Gesture sensor
    | BCNeoPixel NeoInfo
    //** Neopixel led strip

//= E.p: BCPeripheral p & iTask p & fromByteCode p & toByteCode p

derive class iTask BCPeripheral, BCPeripheralInfo
derive gCSerialise BCPeripheral, BCPeripheralInfo
derive gCDeserialise BCPeripheral, BCPeripheralInfo

instance dht Interpret
instance LEDMatrix Interpret
instance LightSensor Interpret
instance AirQualitySensor Interpret
instance GestureSensor Interpret
instance NeoPixel Interpret
