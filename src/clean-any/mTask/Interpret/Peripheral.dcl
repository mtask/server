definition module mTask.Interpret.Peripheral

/**
 * Peripheral definitions
 *
 * This will be split up eventually
 */

from Data.UInt import :: UInt8
from StdOverloaded import class toString
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from mTask.Interpret.VoidPointer import :: VoidPointer
import mTask.Interpret.DSL
import mTask.Language

//** Peripheral options
:: BCPeripheral
	= BCDHT DHTInfo
	//** DHT
	| BCLEDMatrix LEDMatrixInfo
	//** LED Matrix
	| BCI2CButton I2CAddr
	//** LED I²C buttons
	| BCLightSensor I2CAddr
	//** Light sensor
	| BCAirQualitySensor I2CAddr
	//** Air quality sensor
	| BCGestureSensor I2CAddr
	//** Gesture sensor
	| BCInitialised VoidPointer
	//** Special case that, in the C-code contains a pointer to the C structure, marking that the sensor has been initialised

//= E.p: BCPeripheral p & iTask p & fromByteCode p & toByteCode p

derive class iTask BCPeripheral
derive gCSerialise BCPeripheral
derive gCDeserialise BCPeripheral

instance dht (StateT BCState (WriterT [BCInstr] Identity))
instance LEDMatrix (StateT BCState (WriterT [BCInstr] Identity))
instance i2cbutton (StateT BCState (WriterT [BCInstr] Identity))
instance LightSensor (StateT BCState (WriterT [BCInstr] Identity))
instance AirQualitySensor (StateT BCState (WriterT [BCInstr] Identity))
instance GestureSensor (StateT BCState (WriterT [BCInstr] Identity))
