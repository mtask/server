definition module mTask.Interpret.String255

/**
 * A helper type representing bounded length index strings
 */

from Data.Either import :: Either
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.GenType import generic gType, :: Box, :: GType
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from StdOverloaded import class +++, class %, class fromString, class ==
from Text import class Text
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.WF.Definition import class iTask

//** String, that is serialisable and contains at most 255 characters
:: String255 =: String255 String

instance +++ String255
instance % String255
instance fromString String255
instance toString String255
instance == String255
instance Text String255

derive class iTask String255
derive gCDeserialise String255
derive gCSerialise String255
derive gType String255

//** Print a {{String255}} safely, i.e. translate all non-printable characters to escapes.
safePrint :: String255 -> String

//* Helper function for C-code generation of bounded strings (see {{gCSerialise}}).
string255gType :: (String, [String], String -> [String], String -> [String])
