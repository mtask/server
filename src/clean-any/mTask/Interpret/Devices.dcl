definition module mTask.Interpret.Devices

from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: EditorPurpose, :: Editor, :: EditorReport
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.WF.Definition import class iTask, :: Task
from mTask.Interpret.Device import class channelSync
import mTask.Interpret.Device.Serial
import mTask.Interpret.Device.TCP
import mTask.Interpret.Device.MQTT

/**
 * Datatype housing the different communication options for easy editors
 */
:: MTD = TCP TCPSettings | Serial TTYSettings | MQTT MQTTSettings

derive class iTask MTD
instance channelSync MTD

/**
 * Editor for a device with some presets
 *
 * @result task
 */
enterDevice :: Task MTD
