implementation module mTask.Interpret.VoidPointer

import Data.Either
import iTasks
import Data.GenType
import Data.GenType.CSerialise

derive gType VoidPointer
gCSerialise{|VoidPointer|} _ = id
gCDeserialise{|VoidPointer|} top = \st->Right (VoidPointer (), st)
derive class iTask VoidPointer

voidPointergType :: (String, [String], String -> [String], String -> [String])
voidPointergType =
	( gTypeName (gTypeForValue (VoidPointer ()))
	, ["typedef void *VoidPointer;\n"]
	, \r->[r, " = NULL;\n", "(void)get;\n"]
	, \r->["(void)", r, ";\n", "(void)put;\n"])
