implementation module mTask.Interpret.Message

import Data.GenType.CSerialise
import Data.UInt
import Data.VoidPointer
import StdEnv
import iTasks => qualified get, amend

import mTask.Interpret => qualified :: MTask
import mTask.Interpret.Compile
import mTask.Interpret.Instructions
import mTask.Interpret.Peripheral
import mTask.Interpret.Specification
import mTask.Interpret.String255

derive class iTask MTMessageFro, MTMessageTo, MTException, MTaskMeta, MTaskValueState, MTaskEvalStatus, MTaskPrepData, VoidPointer
derive gCSerialise MTMessageFro, MTMessageTo, MTException, MTaskMeta, TaskValue, MTaskValueState, MTaskEvalStatus, MTaskPrepData
derive gCDeserialise MTMessageFro, MTMessageTo, MTException, MTaskMeta, TaskValue, MTaskValueState, MTaskEvalStatus, MTaskPrepData
instance toString MTException where toString e = toSingleLineText e
