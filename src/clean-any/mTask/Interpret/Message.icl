implementation module mTask.Interpret.Message

import StdEnv
import iTasks
import Data.GenType.CSerialise
import Data.UInt
import mTask.Interpret => qualified :: MTask
import mTask.Interpret.Instructions
import mTask.Interpret.Compile
import mTask.Interpret.VoidPointer

derive class iTask MTMessageFro, MTMessageTo, MTException, MTaskMeta, MTaskValueState, MTaskEvalStatus, MTaskPrepData
derive gCSerialise MTMessageFro, MTMessageTo, MTException, MTaskMeta, TaskValue, MTaskValueState, MTaskEvalStatus, MTaskPrepData
derive gCDeserialise MTMessageFro, MTMessageTo, MTException, MTaskMeta, TaskValue, MTaskValueState, MTaskEvalStatus, MTaskPrepData
instance toString MTException where toString e = toSingleLineText e
