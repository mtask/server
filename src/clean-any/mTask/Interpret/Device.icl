implementation module mTask.Interpret.Device

import StdEnv

import Control.Applicative
import Data.Either
import Data.Func
import Data.Functor
import Data.GenHash
import Data.List => qualified group
import Data.Map => qualified union, difference, find, updateAt, get
import Data.Map.GenJSON
import Data.Tuple
import Text
import Text.HTML
import System.Time

import iTasks
import iTasks.Extensions.DateTime

import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Message
import mTask.Interpret.DSL
import mTask.Interpret.Compile
import mTask.Interpret.String255
import mTask.Interpret.VoidPointer
import mTask.Interpret
import Data.UInt

derive class iTask MTDeviceData, MTaskStatus

:: Channels   :== ([MTMessageFro], [MTMessageTo], Bool)
:: MTDevice     = MTDevice
	//* device data
	(SimpleSDSLens MTDeviceData)
	//* sds update queue
	(SimpleSDSLens (Map UInt8 [(UInt8, String255)]))
	//* channels
	(SimpleSDSLens Channels)
:: MTDeviceData =
	{ deviceShutdown :: Bool
	, deviceTasks :: Map UInt8 MTaskStatus
	, deviceSpec  :: ? MTDeviceSpec
	, deviceIds   :: [UInt8]
	}
:: MTaskStatus  = MTSInit | MTSPrepack | MTSAcked | MTSValue (TaskValue String255) | MTSException MTException

instance zero MTDeviceData where
	zero =
		{ deviceShutdown = False
		, deviceTasks    = newMap
		, deviceSpec     = ?None
		, deviceIds      = [zero..UInt8 250]
		}

sendMessage :: (MTMessageTo (SimpleSDSLens Channels) -> Task Channels)
sendMessage = upd o appSnd3 o flip (++) o pure

withDevice :: (a (MTDevice -> Task b) -> Task b) | iTask b & channelSync, iTask a
withDevice = withDevice` True

withDevice` :: Bool a (MTDevice -> Task b) -> Task b | iTask b & channelSync, iTask a
withDevice` verbose device devfun =
	withShared newMap \sdsupdates->
	withShared ([], [MTTSpecRequest], False) \channels->
	withShared zero \dev->
		parallel
			[(Embedded, \_->watch (channels >*< dev)
				>>* [OnValue $ ifValue (\(ch, dev)->not dev.deviceShutdown && thd3 ch) \_->throw MTEUnexpectedDisconnect])
			,(Embedded
				,   \stl->  appendTask Embedded (\_->catchAll
						(channelSync device channels <<@ NoUserInterface)
						(throw o MTESyncException o fromString) @! ?None) stl
				>>- \cstid->appendTask Embedded (\_->processChannels dev sdsupdates channels <<@ NoUserInterface @! ?None) stl
				>>- \pctid->watch dev
				>>* [OnValue $ ifValue (\s->isJust s.deviceSpec)
						\_->appendTask Embedded (\_-> ?Just <$> devfun (MTDevice dev sdsupdates channels)) stl]
				>>- \dftid->watch (sdsFocus (Right dftid) $ taskListItemValue stl)
				>>* [OnValue $ ifValue (\tv->tv =: (Value _ True))
						\_->upd (\d->{d & deviceShutdown=True}) dev
						>-| upd (\(m, s, ss)->(m, s++[MTTShutdown], True)) channels
						>-| watch channels
						>>* [OnValue $ ifValue (\s->s=:(_, [], _)) \_->return ?None]
			])] []
		@? \tv->case tv of
			NoValue = NoValue
			//At least one task was evaluated
			Value vs s = case [v\\(_, v=:(Value (?Just _) _))<-vs] of
				[] = NoValue
				//The device function has a value
				[Value (?Just a) s:_]
					//Its stability has to be postponed until after cleanup
					= Value a (s && length [()\\(_, Value _ True)<-vs] >= 2)
where
	traceIfVerbose msg
		| verbose = traceValue msg
		= return msg

	processChannels :: (SimpleSDSLens MTDeviceData) (SimpleSDSLens (Map UInt8 [(UInt8, String255)])) (SimpleSDSLens Channels) -> Task [MTDeviceData]
	processChannels dev sdsupdates channels = forever
		$   watch channels
		>>* [OnValue $ ifValue (not o isEmpty o fst3)
		    $   \(r,s,ss)->upd (\(rr,s,ss)->(drop (length r) rr,s,ss)) channels
		    >>- \_->sequence (map process r)
		    ]
	where
		process :: MTMessageFro -> Task MTDeviceData
		process (MTFSpec c)
			= traceIfVerbose ("Received spec: " +++ toSingleLineText c)
			>-| upd (\s->{s & deviceSpec= ?Just c}) dev
		process (MTFTaskDelAck i)
			= traceIfVerbose ("Task " +++ toString i +++ " deleted")
			>-| upd (\s->{s & deviceIds=s.deviceIds ++ [i], deviceTasks=del i s.deviceTasks}) dev
		process (MTFTaskAck i)
			= traceIfVerbose ("Task acked: " +++ toSingleLineText i)
			>-| upd (\s->{s & deviceTasks=put i MTSAcked s.deviceTasks}) dev
		process (MTFTaskPrepAck i)
			= traceIfVerbose ("Taskprep acked: " +++ toSingleLineText i)
			>-| upd (\s->{s & deviceTasks=put i MTSPrepack s.deviceTasks}) dev
		process (MTFTaskReturn taskid mv)
			= traceIfVerbose ("Task " +++ toSingleLineText taskid +++ " returned: " +++ toSingleLineText (safePrint <$> mv))
			>-| upd (\s->{s & deviceTasks=put taskid (MTSValue mv) s.deviceTasks}) dev
		process (MTFSdsUpdate taskid sdsid value)
			# value = concat [String255 (toByteCode{|*|} i)\\i<-:value]
			= traceIfVerbose ("Received an update for sds " +++ toString sdsid +++ " from task " +++ toString taskid +++ " with value: " +++ safePrint value)
			>-| upd (alter (fmap \l->l ++ [(sdsid, value)]) taskid) sdsupdates
			>-| get dev
		process (MTFException exc)
			= traceIfVerbose ("Received an exception: ", toSingleLineText exc)
			>-| case exc of
				//We ignore sdsunknown exceptions because they can happen due to timing sometimes
				MTESdsUnknown _ _ = traceValue ("Warning: the server sent an update to an SDS unknown to the device: " +++ toSingleLineText exc)
					>-|get dev
				//General error, just rethrow
				MTERTSError = throw MTERTSError
				//Errors only affecting the current task
				e=:(MTEUnsupportedPeripheral taskid _) = 
					upd (\s->{s & deviceTasks=put taskid (MTSException e) s.deviceTasks}) dev
				//Errors affecting all tasks
				e = upd (\s->{s & deviceTasks=MTSException e <$ s.deviceTasks}) dev
		process (MTFDebug s)
			= traceIfVerbose ("MSG: " +++ toString s +++ "\n") >>- \_->get dev
		process MTFPing = get dev
		process x = traceIfVerbose ("Not implemented: " +++ toSingleLineText x)
			>-| get dev

mTaskSafe :: (Task u) -> Task (Either MTException u) | type u
mTaskSafe t = try (Right <$> t) \e->case e of
	//General errors
	e=:MTERTSError = throw e
	//Task specific errors
	e = return (Left e)

preloadTasks :: [MTaskBox] -> Task String
preloadTasks ts = preloadTasksWithOptions zero ts

preloadTasksWithOptions :: CompileOpts [MTaskBox] -> Task String
preloadTasksWithOptions opts tasks
	= get applicationName
	>>- \aname->get currentDateTime
	>>- \now->header now aname <$> ptwo [zero..] tasks footer
	>>- viewInformation [ViewAs \a->PreTag [] [Text a]] o join "\n"
where
	ptwo :: [UInt8] [MTaskBox] [String] -> Task [String]
	ptwo _ [] c = return c
	ptwo [n:ns] [(MTB t):ts] c = mkMessage n opts t \_ td
		# td & status = MTNeedsInit
		# bytes = join ", " [toString c\\c<-gCSerialise{|*|} td []]
		= ptwo ns ts
			[ concat5 "#define PRELOADED_HASH" (toString n) " " (toString $ mtaskmetatohash td) "ll"
			, concat5 "static const uint8_t preloaded_task" (toString n) "[] PROGMEM = {" bytes "};"
			: c]
	footer =
		[ concat3 "static const uint8_t *const preloaded_tasks[PRELOADED_TASKS_NUM] PROGMEM = {" (join ", " ["preloaded_task" +++ toString n\\n<-[0..length tasks-1]]) "};"
		, concat3 "static const int64_t preloaded_hashes[PRELOADED_TASKS_NUM] PROGMEM = {" (join ", " ["PRELOADED_HASH" +++ toString n\\n<-[0..length tasks-1]]) "};"
		, ""
		, "#ifdef __cplusplus"
		, "}"
		, "#endif /* __cplusplus */"
		, ""
		, "#endif /* !PRELOADED_TASKS_H */"
		, ""
		]
		
	header now aname s =
		[ "/** @file preloaded_tasks.h"
		, " *"
		, " * Contains the binary data for the preloaded tasks"
		, " * Generated on " +++ toString now +++ " by " +++ aname
		, " */"
		, "#ifndef PRELOADED_TASKS_H"
		, "#define PRELOADED_TASKS_H"
		, ""
		, "#include \"preload.h\""
		, ""
		, "#ifdef __cplusplus"
		, "extern \"C\" {"
		, "#endif /* __cplusplus */"
		, ""
		, "#define PRELOADED_TASKS_NUM " +++ toString (length tasks)
		, ""
		: s]

mtaskmetatohash :: MTaskMeta -> Int
mtaskmetatohash mm = gHash{|*|} $ gCSerialise{|*|} mm []

preloadTask :: (Main (BCInterpret (TaskValue u))) -> Task String | type u
preloadTask a = preloadTaskWithOptions zero a

preloadTaskWithOptions :: CompileOpts (Main (BCInterpret (TaskValue u))) -> Task String | type u
preloadTaskWithOptions opts task = preloadTasksWithOptions opts [MTB task]

mkMessage :: UInt8 CompileOpts (Main (BCInterpret (TaskValue u)))
	([?MTLens] MTaskMeta -> Task b)
	-> Task b | type u
mkMessage sid opts task f
	# (returnvalue, shares, hardware, instructions) = compileOpts opts task
	# (mayberefs, shares) = unzip shares
	= sequence shares >>- \shares->f mayberefs
		{ taskid         = sid
		, tree           = VoidPointer ()
		, stability      = MTNoValue
		, value          = returnvalue
		, peripherals    = hardware
		, shares         = {i\\i<-shares}
		, instructions   = BCIs {i\\i<-instructions}
		, status         = MTNeedsInit
		, execution_min  = UInt32 0
		, execution_max  = UInt32 0
		, lastrun        = UInt32 0
		}

liftmTask :: (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u
liftmTask a b = liftmTaskWithOptions zero a b

taskAccepted MTSPrepack = True
taskAccepted MTSAcked = True
taskAccepted (MTSValue _) = True
taskAccepted (MTSException _) = True
taskAccepted _ = False

liftmTaskWithOptions :: CompileOpts (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u
liftmTaskWithOptions opts task (MTDevice dev sdsupdates channels)
	=   get dev
	//Compile
	>>- \st=:{deviceIds=[sid:rest]}->upd (\s->{s & deviceTasks=put sid MTSInit s.deviceTasks, deviceIds=rest}) dev
	//Make sure the task is deleted even if we are destroyed (e.g. from a step)
	>-| let taskView = sdsFocus sid $ mapLens "taskView" taskshare ?None
		in  withCleanupHook (sendMessage (MTTTaskDel sid) channels)
		//Resolve shares
		$   mkMessage sid opts task \mayberefs msg->
		//Add task to sdsupdates map
		    upd (put sid []) sdsupdates
		//Send the prep
		>-| sendMessage (MTTTaskPrep {taskid=sid, hash=mtaskmetatohash msg}) channels
		//Wait for the prepack
		>-| watch taskView
		>>* [OnValue $ ifValue taskAccepted \t->case t of
			//If it was loaded from cache we don't need to send anything
			MTSAcked -> return ()
			MTSValue _ -> return ()
			MTSException _ -> return ()
			_ -> sendMessage (MTTTask msg) channels @! ()
		]
		//Wait for task ack
		>-| watch taskView
		>>* [OnValue $ ifValue (\t->t =: MTSAcked || t =: (MTSValue _) || t =: (MTSException _))
			\_->    waitForReturn taskView
				-|| allTasks [watchShareUpstream sid ish sh\\(?Just ish)<-mayberefs & sh<-:msg.shares]
				-|| watchSharesDownstream mayberefs [i\\i<-:msg.shares] sid
		] @? \tv->case tv of
			NoValue = NoValue
			Value v _ = v
where
	taskshare = mapReadWrite
		(\  s->s.deviceTasks
		,\t s-> ?Just {s & deviceTasks=t}
		) ?None dev
	//waitForReturn :: UInt8 -> Task u^
	waitForReturn taskView  = watch $ flip mapReadError taskView \s->case s of
			MTSException e = Error (exception e)
			MTSValue NoValue = Ok NoValue
			MTSValue (Value a s) = case iTasksDecode (toString a) of
				Error e = Error e
				Ok v = Ok (Value v s)
			_ = Ok NoValue

	//If a share changes from the server
	watchShareUpstream :: UInt8 MTLens BCShareSpec -> Task ()
	watchShareUpstream taskid ref share = watch ref
		>>* [OnValue $ ifValue ((<>)share.bcs_value) $ const
			$ traceValue "Watching the upstream share" >-| whileUnchanged ref
			\v->sendMessage (MTTSdsUpdate taskid share.bcs_ident v) channels
				@? const NoValue
			]

	//If a share changes from the client
	watchSharesDownstream :: [?MTLens] [BCShareSpec] UInt8 -> Task ()
	watchSharesDownstream mayberefs shares taskid
		# myupdates = sdsFocus taskid $ mapLens "sdsupdates" sdsupdates (?Just [])
		= forever
		$   watch myupdates
		>>* [OnValue $ ifValue (not o isEmpty)
			$   \shup->upd (drop $ length shup) myupdates
			>>- \_   ->sequence (map (uncurry applyDownstreamUpdate) shup)
			@! ()]
	where
		applyDownstreamUpdate :: UInt8 String255 -> Task ()
		applyDownstreamUpdate sdsid value = case find (\(_, sh)->sh.bcs_ident==sdsid) (zip2 mayberefs shares) of
			?None = traceValue "Huh? I got a share update for an unknown share" @! ()
			?Just (?None, _) = traceValue "Huh? I got a share update for a non iTasks share" @! ()
			?Just (?Just ref, _) = traceValue "Found the sds, updating" >>- \_->upd (const value) ref @! ()

deviceSpecification :: MTDevice -> Task MTDeviceSpec
deviceSpecification (MTDevice dev _ _)
	= watch dev >>* [OnValue $ ifValue (\d->isJust d.deviceSpec) \d->return (fromJust d.deviceSpec)]

viewDevice :: MTDevice -> Task ()
viewDevice (MTDevice dev sdsupdates channels) = allTasks
	[ viewSharedInformation [ViewAs $ toList] sdsupdates
		<<@ Title "Sdsupdates" @! ()
	, viewSharedInformation [] channels
		<<@ Title "Communication Channels" @! ()
	, viewSharedInformation [ViewAs $ toData`] dev
		<<@ Title "Device Data" @! ()
	] <<@ ArrangeWithTabs False @? const NoValue
where
	toData` d = {deviceTasks`=toList d.deviceTasks, deviceSpec`=d.deviceSpec}//, deviceIds`=d.deviceIds}

:: MTDeviceData` =
	{ deviceTasks` :: [(UInt8, MTaskStatus)]
	, deviceSpec`  :: ? MTDeviceSpec
//	, deviceIds`   :: [UInt8]
	}

derive class iTask MTDeviceData`
