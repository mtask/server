definition module mTask.Interpret.ByteCodeEncoding._FBC

/**
 * Module for the FBC type so that it may remain abstract
 */

from Data.Either import :: Either
from StdGeneric import generic binumap

derive binumap FBC
/**
 * bytecode parser
 *
 * @var type
 */
:: FBC a = FBC ([Char] -> (Either String (? a), [Char]))
