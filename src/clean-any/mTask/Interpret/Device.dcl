definition module mTask.Interpret.Device

/**
 * The integration with iTask
 */

from Control.Monad.State import :: StateT
from Control.Monad.Writer import :: WriterT
from Data.Either import :: Either
from Data.Functor.Identity import :: Identity
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.SDS.Definition import class RWShared, class Readable, class Writeable, class Modifiable, class Registrable, class Identifiable
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.WF.Definition import :: Task, :: TaskValue
from iTasks.WF.Definition import class iTask

from mTask.Interpret.ByteCodeEncoding import generic fromByteCode, generic toByteCode, class toByteWidth, :: FBC
from mTask.Interpret.Compile import :: CompileOpts
from mTask.Interpret.DSL import :: BCInterpret, :: BCState

from mTask.Interpret.Instructions import :: BCInstr
from mTask.Interpret.Message import :: MTMessageFro, :: MTMessageTo, :: MTException
from mTask.Interpret.Specification import :: MTDeviceSpec
from mTask.Language import :: Main, class type

//** Helper type for a communication channel, (inqueue, outqueue, stopflag)
:: Channels :== ([MTMessageFro], [MTMessageTo], Bool)
//** A box helper to pack mTask programs of various types in an untyped container
:: MTaskBox = E.u: MTB (Main (BCInterpret (TaskValue u))) & type u
//** Abstract type representing a reference to a connected device
:: MTDevice

/**
 * The communication channel, for a given communication specification, the task synchronises the channels
 *
 * @var communication specification
 * @param communication specification
 * @param SDS to synchronise
 * @result a task that synchronises the SDS
 */
class channelSync a :: a (sds () Channels Channels) -> Task () | RWShared sds

/**
 * Connect to a device
 *
 * @param communication specification
 * @param function producing a task doing something with the device
 * @result resulting task
 */
withDevice :: (a (MTDevice -> Task b) -> Task b) | iTask b & channelSync, iTask a

/**
 * Connects a device and specifies the verbosity
 *
 * @param verbosity
 * @param communication specification
 * @param function producing a task doing something with the device
 * @result resulting task
 */
withDevice` :: Bool a (MTDevice -> Task b) -> Task b | iTask b & channelSync, iTask a

/**
 * Lift the mTask task to an itasks task, i.e. run it on the given device
 *
 * @param mTask task
 * @param device
 * @param resulting task
 */
liftmTask :: (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u

/**
 * Lift the mTask task to an itasks task given the options, i.e. run it on the given device
 *
 * @param compilation options
 * @param mTask task
 * @param device
 * @result resulting task
 */
liftmTaskWithOptions :: CompileOpts (Main (BCInterpret (TaskValue u))) MTDevice -> Task u | type u

/**
 * Prepare a task for preloading.
 *
 * @param mTask task
 * @result the code to put in preloaded_task.h
 */
preloadTask :: (Main (BCInterpret (TaskValue u))) -> Task String | type u

/**
 * Prepare a task for preloading and use the compilation options provided
 *
 * @param compilation options
 * @param mTask task
 * @result the code to put in preloaded_task.h
 */
preloadTaskWithOptions :: CompileOpts (Main (BCInterpret (TaskValue u))) -> Task String | type u

/**
 * Prepare a list of boxed tasks for preloading.
 *
 * @param mTask tasks boxed in MTaskBox types
 * @result the code to put in preloaded_task.h
 */
preloadTasks :: [MTaskBox] -> Task String

/**
 * Prepare a list of boxed tasks for preloading and use the compilation options
 * provided
 *
 * @param mTask tasks boxed in MTaskBox types
 * @result the code to put in preloaded_task.h
 */
preloadTasksWithOptions :: CompileOpts [MTaskBox] -> Task String

/**
 * Wraps a task and catches mTask exceptions when possible
 *
 * @param task
 * @result resulting task
 */
mTaskSafe :: (Task u) -> Task (Either MTException u) | type u

/**
 * Retrieves the device specification from a device handle
 *
 * @param device
 * @result device specification
 */
deviceSpecification :: MTDevice -> Task MTDeviceSpec
