implementation module mTask.Interpret.Peripheral

import Control.Applicative
import Control.Monad
import Data.Either
import Data.Func
import Data.Functor
import Data.Functor.Identity
import Data.GenType.CSerialise
import Data.List
import qualified Data.Map as DM
import Data.Monoid
import Data.VoidPointer
import StdEnv
import Text => qualified join
import Text.GenJSON

import Data.UInt
import mTask.Interpret.DSL
import mTask.Language

derive class iTask BCPeripheral, BCPeripheralInfo, VoidPointer
derive gCSerialise BCPeripheral, DHTtype, DHTInfo, Pin, APin, DPin, LEDMatrixInfo, I2CAddr, BCPeripheralInfo, AirQualitySensorInfo, LightSensorInfo, GestureSensorInfo
derive gCDeserialise BCPeripheral, DHTtype, DHTInfo, Pin, APin, DPin, LEDMatrixInfo, I2CAddr, BCPeripheralInfo, AirQualitySensorInfo, LightSensorInfo, GestureSensorInfo
derive gCDeserialise NeoInfo, NeoPixelType, NeoPixelFreq
derive gCSerialise NeoInfo, NeoPixelType, NeoPixelFreq

nextPeripheral st = length st.bcs_peripherals

instance == BCPeripheralInfo where (==) l r = l === r

addPerIfNotExist :: BCPeripheralInfo -> Interpret Int
addPerIfNotExist per = amend \s=:{bcs_peripherals}->case findIndex ((==)per) bcs_peripherals of
		?None = (length bcs_peripherals, {s & bcs_peripherals=bcs_peripherals ++ [per]})
		?Just i = (i, s)

instance dht Interpret where
	dht dhtinfo def = {main
		= addPerIfNotExist (BCDHT dhtinfo)
		>>= \pid->unmain (def (pure (Dht pid)))
		}
	temperature` ti dht
		= dht
		>>= \(Dht i)->tell [BCMkTask $ BCDHTTemp $ fromInt i]
		>>| setRate True ti
	humidity` ti dht
		= dht
		>>= \(Dht i)->tell [BCMkTask $ BCDHTHumid $ fromInt i]
		>>| setRate True ti

instance LEDMatrix Interpret where
	ledmatrix i def = {main
		= addPerIfNotExist (BCLEDMatrix i)
		>>= \pid->unmain (def (pure (LEDMatrix pid)))
		}
	LMDot m x y s = m >>= \(LEDMatrix i)->x >>| y >>| s >>| tell [BCMkTask $ BCLEDMatrixDot $ fromInt i]
	LMIntensity m x = m >>= \(LEDMatrix i)->x >>| tell [BCMkTask $ BCLEDMatrixIntensity $ fromInt i]
	LMClear m = m >>= \(LEDMatrix i)->tell [BCMkTask $ BCLEDMatrixClear $ fromInt i]
	LMDisplay m = m >>= \(LEDMatrix i)->tell [BCMkTask $ BCLEDMatrixDisplay $ fromInt i]

instance LightSensor Interpret
where
	lightSensor addr def = {main
		= addPerIfNotExist (BCLightSensor addr)
		>>= \pid->unmain (def (pure (LightSensor pid)))
		}
	light` ti ls
		= ls
		>>= \(LightSensor i) -> tell [BCMkTask (BCGetLight (fromInt i))]
		>>| setRate True ti

instance AirQualitySensor Interpret
where
	airqualitySensor addr def = {main
		= addPerIfNotExist (BCAirQualitySensor addr)
		>>= \pid->unmain (def (pure (AirQualitySensor pid)))
		}
	setEnvironmentalData aqs humid temp = humid >>| temp >>| aqs >>= \(AirQualitySensor i)->tell [BCMkTask (BCSetEnvironmentalData (fromInt i))]
	tvoc` i aqs
		= aqs
		>>= \(AirQualitySensor s) -> tell [BCMkTask (BCTVOC (fromInt s))]
		>>| setRate True i
	co2` i aqs
		= aqs
		>>= \(AirQualitySensor s) -> tell [BCMkTask (BCCO2 (fromInt s))]
		>>| setRate True i

instance GestureSensor Interpret
where
	gestureSensor addr def = {main
		=   addPerIfNotExist (BCGestureSensor addr)
		>>= \pid->unmain (def (pure (GestureSensor pid)))
		}
	gesture` i ges
		= ges
		>>= \(GestureSensor s) -> tell [BCMkTask (BCGesture (fromInt s))]
		>>| setRate True i

instance NeoPixel Interpret
where
	neopixel neoinfo def = {main
		= addPerIfNotExist (BCNeoPixel neoinfo)
		>>= \pid -> unmain (def (pure (NeoPixel pid)))
		}
	setPixelColor vneo n r g b
		= vneo >>= \(NeoPixel neo) -> n >>| r >>| g >>| b >>| 
		  tell [BCMkTask (BCNeoSetPixelColor (fromInt neo))]
