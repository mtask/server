implementation module mTask.Interpret.Peripheral

import StdEnv

import Data.Func
import Data.List
import Data.Functor
import Data.Functor.Identity
import Data.Monoid
import Data.Either
import Control.Monad
import Control.Monad.State
import Control.Monad.Writer
import Control.Applicative
import qualified Data.Map as DM

import Text => qualified join

import mTask.Interpret.DSL
import mTask.Interpret.VoidPointer
import Data.UInt
import mTask.Language

import Data.GenType.CSerialise

derive class iTask BCPeripheral
derive gCSerialise BCPeripheral, DHTtype, DHTInfo, Pin, APin, DPin, LEDMatrixInfo, I2CAddr
derive gCDeserialise BCPeripheral, DHTtype, DHTInfo, Pin, APin, DPin, LEDMatrixInfo, I2CAddr

nextPeripheral st = length st.bcs_hardware

instance == BCPeripheral where (==) l r = l === r

addPerIfNotExist :: BCPeripheral -> BCInterpret Int
addPerIfNotExist per = getState
	>>= \s=:{bcs_hardware}->case findIndex ((==)per) bcs_hardware of
		?None = put {s & bcs_hardware=bcs_hardware ++ [per]} >>| pure (length bcs_hardware)
		?Just i = pure i

instance dht (StateT BCState (WriterT [BCInstr] Identity)) where
	DHT dhtinfo def = {main
		= addPerIfNotExist (BCDHT dhtinfo)
		>>= \pid->unmain (def (pure (Dht pid)))
		}
	temperature` ti dht
		= dht
		>>= \(Dht i)->tell` [BCMkTask $ BCDHTTemp $ fromInt i]
		>>| setRate True ti
	humidity` ti dht
		= dht
		>>= \(Dht i)->tell` [BCMkTask $ BCDHTHumid $ fromInt i]
		>>| setRate True ti

instance LEDMatrix (StateT BCState (WriterT [BCInstr] Identity)) where
	ledmatrix i def = {main
		= addPerIfNotExist (BCLEDMatrix i)
		>>= \pid->unmain (def (pure (LEDMatrix pid)))
		}
	LMDot m x y s = m >>= \(LEDMatrix i)->x >>| y >>| s >>| tell` [BCMkTask $ BCLEDMatrixDot $ fromInt i]
	LMIntensity m x = m >>= \(LEDMatrix i)->x >>| tell` [BCMkTask $ BCLEDMatrixIntensity $ fromInt i]
	LMClear m = m >>= \(LEDMatrix i)->tell` [BCMkTask $ BCLEDMatrixClear $ fromInt i]
	LMDisplay m = m >>= \(LEDMatrix i)->tell` [BCMkTask $ BCLEDMatrixDisplay $ fromInt i]

instance i2cbutton (StateT BCState (WriterT [BCInstr] Identity)) where
	i2cbutton addr def = {main
		= addPerIfNotExist (BCI2CButton addr)
		>>= \pid->unmain (def (pure (I2CButton pid)))
		}
	AButton` ti m
		= m
		>>= \(I2CButton i)->tell` [BCMkTask (BCAButton (fromInt i))]
		>>| setRate True ti
	BButton` ti m
		= m
		>>= \(I2CButton i)->tell` [BCMkTask (BCBButton (fromInt i))]
		>>| setRate True ti

instance LightSensor (StateT BCState (WriterT [BCInstr] Identity))
where
	lightsensor addr def = {main
		= addPerIfNotExist (BCLightSensor addr)
		>>= \pid->unmain (def (pure (LightSensor pid)))
		}
	light` ti ls
		= ls
		>>= \(LightSensor i) -> tell` [BCMkTask (BCGetLight (fromInt i))]
		>>| setRate True ti

instance AirQualitySensor (StateT BCState (WriterT [BCInstr] Identity))
where
	airqualitysensor addr def = {main
		= addPerIfNotExist (BCAirQualitySensor addr)
		>>= \pid->unmain (def (pure (AirQualitySensor pid)))
		}
	setEnvironmentalData aqs humid temp = humid >>| temp >>| aqs >>= \(AirQualitySensor i)->tell` [BCMkTask (BCSetEnvironmentalData (fromInt i))]
	tvoc` i aqs
		= aqs
		>>= \(AirQualitySensor s) -> tell` [BCMkTask (BCTVOC (fromInt s))]
		>>| setRate True i
	co2` i aqs
		= aqs
		>>= \(AirQualitySensor s) -> tell` [BCMkTask (BCCO2 (fromInt s))]
		>>| setRate True i

instance GestureSensor (StateT BCState (WriterT [BCInstr] Identity))
where
	gestureSensor addr def = {main
		=   addPerIfNotExist (BCGestureSensor addr)
		>>= \pid->unmain (def (pure (GestureSensor pid)))
		}
	gesture` i ges
		= ges
		>>= \(GestureSensor s) -> tell` [BCMkTask (BCGesture (fromInt s))]
		>>| setRate True i
