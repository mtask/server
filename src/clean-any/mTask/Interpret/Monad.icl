implementation module mTask.Interpret.Monad

import Control.Applicative
import Control.Monad
import Control.Monad.Fail
import Control.Monad.RWST => qualified tell, put, get, gets, modify
import Control.Monad.Trans
import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Monoid
import StdEnv

import mTask.Interpret.DSL

:: Interpret a = IntT (RWST () [BCInstr] BCState (MaybeError String) a)

runInterpret :: (Interpret a) BCState -> MaybeError String (BCState, [BCInstr])
runInterpret m st = execRWST (runIT m) () st

runIT :: (Interpret a) -> RWST () [BCInstr] BCState (MaybeError String) a
runIT (IntT m) = m

instance Functor Interpret where
	fmap f t = IntT (f <$> runIT t)
instance pure Interpret where
	pure a = IntT (pure a)
instance <*> Interpret where
	(<*>) fab fa = IntT (runIT fab <*> runIT fa)
instance Monad Interpret where
	bind ma a2mb = IntT (runIT ma >>= runIT o a2mb)
instance MonadFail Interpret where
	fail msg = IntT (fail msg)
instance <* Interpret
instance *> Interpret
instance <* (RWST r w s m) | pure m & Monoid w & Monad m
instance *> (RWST r w s m) | pure m & Monoid w & Monad m

tell :: [BCInstr] -> Interpret a
tell m = IntT ('Control.Monad.RWST'.tell m *> pure (abort "value of tell used during compilation"))

censorListen :: (Interpret a) -> Interpret [BCInstr]
censorListen m = IntT (snd <$> censor (\_->[]) (listen (runIT m)))

amend :: (BCState -> (a, BCState)) -> Interpret a
amend af = IntT
	$ 'Control.Monad.RWST'.get
	>>= \s->let (a, s`) = af s in 'Control.Monad.RWST'.put s`
	>>| pure a

get :: Interpret BCState
get = gets id

gets :: (BCState -> a) -> Interpret a
gets f = amend (\s->(f s, s))

put :: BCState -> Interpret ()
put s = amend (\_->((), s))

modify :: (BCState -> BCState) -> Interpret ()
modify f = amend (\s->((), f s))
