definition module mTask.Interpret.Message

/**
 * Types used for communication
 */

from StdOverloaded import class toString
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from Data.Either import :: Either
from iTasks.WF.Definition import class iTask
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenEq import generic gEq
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

from Data.UInt import :: UInt8, :: UInt16, :: UInt32
from iTasks.WF.Definition import :: TaskValue
from mTask.Interpret.Instructions import :: BCInstr, :: BCInstrs
from mTask.Interpret.Specification import :: MTDeviceSpec
from mTask.Interpret.Peripheral import :: BCPeripheral
from mTask.Interpret.Compile import :: BCShareSpec
from mTask.Interpret.String255 import :: String255
from mTask.Interpret.VoidPointer import :: VoidPointer

//** Messages that are sent to the device
:: MTMessageTo
	= MTTTask MTaskMeta
	//* Send a task
	| MTTTaskPrep MTaskPrepData
	//* Prepare the RTS for receiving a big task (make it wait for it so that the comm buffers won't overflow)
	| MTTTaskDel UInt8
	//** delete a task
	//** @param taskId
	| MTTSpecRequest
	//** Request a specification
	| MTTShutdown
	//** Request graceful shutdown
	| MTTSdsUpdate UInt8 UInt8 String255
	//** update an SDS
	//** @param taskId
	//** @param sdsId
	//** @param value

//** Preparation data
:: MTaskPrepData =
	{ taskid :: UInt8
	//** Task id
	, hash :: Int
	//** Hash value of the task to see if it was preloaded
	}

//** All data for a task
:: MTaskMeta =
	{ taskid        :: UInt8
	//* Task id
	, tree          :: VoidPointer
	//* Placeholder for a pointer to the tree (will be a void pointer in the C-code)
	, stability     :: MTaskValueState
	//* Stability
	, value         :: String255
	//* Buffer to hold the return value, this is a string with only spaces of the required length
	, shares        :: {BCShareSpec}
	//* SDS definitions
	, peripherals   :: {BCPeripheral}
	//* Peripheral definitions
	, instructions  :: BCInstrs
	//* Instructions
	, status        :: MTaskEvalStatus
	//* Evaluation status, this is only used in the RTS
	, execution_min :: UInt32
	//* Evaluation lower bound, this is only used in the RTS
	, execution_max :: UInt32
	//* Evaluation upper bound, this is only used in the RTS
	, lastrun       :: UInt32
	//* Last execution time, this is only used in the RTS
	}
//** Stability of the task value
:: MTaskValueState = MTNoValue | MTUnstable | MTStable

//** Status of the task
:: MTaskEvalStatus
	= MTEvaluated
	//* task tree is evaluated and the previous execution interval is still correct
	| MTPurged
	//* task tree is evaluated, but the execution interval needs to be recalculated
	| MTUnevaluated
	//* task tree contains unevaluated parts, the execution interval is [0,0]
	| MTRemoved
	//* task is removed
	| MTNeedsInit
	//* task doesn't have a tasktree yet

//** Messages that are sent from the device
:: MTMessageFro
	= MTFTaskAck UInt8
	//** Acknowledge a task
	//** @param taskid
	| MTFTaskPrepAck UInt8
	//** Acknowledge a task preparation request
	//** @param taskid
	| MTFTaskDelAck UInt8
	//** Acknowledge a task deletion request
	//** @param taskid
	| MTFTaskReturn UInt8 (TaskValue String255)
	//** Communicate a new return value
	//** @param taskid
	//** @param returnvalue
	| MTFSpec MTDeviceSpec
	//** Send the device specification
	//** @param specification
	| MTFSdsUpdate UInt8 UInt8 {UInt16}
	//** Communicate a new SDS value
	//** @param taskid
	//** @param sdsid
	//** @param value (this datatype is strange but an SDS is stored as 16 bit integers on the RTS due to the word width so this makes communicating this easy)
	| MTFException MTException
	//** Send an exception
	//** @param exception
	| MTFDebug String255
	//** Send a debug message
	//** @param message
	| MTFPing
	//** Send a ping signal (actually a pong)

//** All types of mTask exceptions
:: MTException
	= MTEOutOfMemory UInt8
	//** Memory has ran out
	//** @param taskid
	| MTEHeapUnderflow UInt8
	//** Heap has underflown
	//** @param taskid
	| MTEStackOverflow UInt8
	//** Stack has overflown
	//** @param taskid
	| MTESdsUnknown UInt8 UInt8
	//** SDS is not known
	//** @param taskid
	//** @param sdsid
	| MTEPeripheralUnknown UInt8 UInt8
	//** Peripheral is not known
	//** @param taskid
	//** @param peripheral id
	| MTEUnsupportedPeripheral UInt8 UInt8
	//** Peripheral is not supported
	//** @param taskid
	//** @param peripheral id
	| MTEUnsupportedInterrupt UInt16 UInt8
	//** Interrupt is not supported
	//** @param encoded pin
	//** @param interrupt mode
	| MTEFPException UInt8
	//** Floating point exception
	//** @param taskid
	| MTESyncException String255
	//** Not used TODO remove
	//** @param reason
	| MTERTSError
	//** Not used TODO remove
	| MTEUnexpectedDisconnect
	//** Unexpected disconnect

derive class iTask MTMessageFro, MTMessageTo, MTException, MTaskMeta, MTaskValueState, MTaskEvalStatus, MTaskPrepData
derive gCSerialise MTMessageFro, MTMessageTo, MTException, MTaskMeta, TaskValue, MTaskValueState, MTaskEvalStatus, MTaskPrepData
derive gCDeserialise MTMessageFro, MTMessageTo, MTException, MTaskMeta, TaskValue, MTaskValueState, MTaskEvalStatus, MTaskPrepData

instance toString MTException
