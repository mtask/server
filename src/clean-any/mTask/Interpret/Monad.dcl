definition module mTask.Interpret.Monad

from Control.Applicative import class Applicative, class *>, class <*, class <*>, class pure
from Control.Monad import class Monad
from Control.Monad.Fail import class MonadFail
from Data.Error import :: MaybeError
from Data.Functor import class Functor
from Data.Functor.Identity import :: Identity, instance Functor Identity

from mTask.Interpret.DSL import :: BCState, :: BCInstr

:: Interpret a

runInterpret :: (Interpret a) BCState -> MaybeError String (BCState, [BCInstr])

instance Functor Interpret
instance pure Interpret
instance <*> Interpret
instance <* Interpret
instance *> Interpret
instance Monad Interpret
instance MonadFail Interpret

tell :: [BCInstr] -> Interpret a

//* Execute an Interpret, do not write the instructions but save them.
censorListen :: (Interpret a) -> Interpret [BCInstr]

amend :: (BCState -> (a, BCState)) -> Interpret a
get :: Interpret BCState
gets :: (BCState -> a) -> Interpret a
put :: BCState -> Interpret ()
modify :: (BCState -> BCState) -> Interpret ()
