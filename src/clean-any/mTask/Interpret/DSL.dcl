definition module mTask.Interpret.DSL

/**
 * Bytecode compiler instances of the mTask DSL
 */

from Gast import class Gast, generic ggen, generic genShow, generic gPrint, :: GenState, class PrintOutput, :: PrintState
from StdOverloaded import class zero
from Control.Monad.State import :: StateT
from Control.Monad.Writer import :: WriterT
from Data.Functor.Identity import :: Identity
from StdOverloaded import class ^(^), class -(-)
from Data.Map import :: Map
import iTasks.WF.Derives

import mTask.Interpret.Peripheral
import mTask.Interpret.ByteCodeEncoding
import mTask.Interpret.Instructions
import Data.UInt
import mTask.Language
from iTasks.SDS.Definition import :: SDSLens

/**
 * Helper type representing a NULL pointer in the mTask RTS
 *
 * @type UInt16
 */
MT_NULL     :== UInt16 ((2^16) - 1)
/**
 * Helper type representing a task-is-removed state in the mTask RTS
 *
 * @type UInt16
 */
MT_REMOVE   :== UInt8  ((2^8)  - 1)

//** The datatype for the bytecode compiler that implements the mTask classes
:: BCInterpret a :== StateT BCState (WriterT [BCInstr] Identity) a

//** Monad.Writer's {{tell}} function with a more relaxed type
tell` :: [BCInstr] -> BCInterpret a
/**
 * Tweak the timing interval 
 *
 * @param TODO: ???
 * @param timinng interval
 * @result a {{BCInterpret}}
 */
setRate :: Bool (TimingInterval (StateT BCState (WriterT [BCInstr] Identity))) -> BCInterpret a
/**
 * Helper function to add stuff to a map iff it is not already there
 */
addMapIfNotExist :: k v (Map k v) -> Map k v | < k

//** State of the compiler
:: BCState =
	{ bcs_infun        :: JumpLabel
	//** Current function the compiler is in
	, bcs_mainexpr     :: [BCInstr]
	//** Main expression
	, bcs_context      :: [BCInstr]
	//** Context (i.e. the lambda lifted arguments that are in scope)
	, bcs_functions    :: Map JumpLabel BCFunction
	//** Functions
	, bcs_freshlabel   :: JumpLabel
	//** Next fresh label
	, bcs_sdses        :: Map Int (Either String255 MTLens)
	//** Map of SDSs
	, bcs_hardware     :: [BCPeripheral]
	//** Peripherals
	}
//** Synonym type for a lifted SDS that can be written to by mTask
:: MTLens :== SDSLens () String255 String255
//** An mTask function
:: BCFunction =
	{ bcf_instructions :: [BCInstr]
	//** the instructions
	, bcf_argwidth     :: UInt8
	//** The width of the argument in bytes
	, bcf_returnwidth  :: UInt8
	//** The width of the return value in bytes
	}

instance zero BCState

/**
 * Compile a {{Main}} program
 *
 * @param {{Main}} program
 * @param state
 * @result new state
 */
mainBC :: (Main (BCInterpret v)) BCState -> BCState

instance aio       (StateT BCState (WriterT [BCInstr] Identity))
instance expr      (StateT BCState (WriterT [BCInstr] Identity))
instance delay     (StateT BCState (WriterT [BCInstr] Identity))
instance dio p     (StateT BCState (WriterT [BCInstr] Identity))
instance pinMode   (StateT BCState (WriterT [BCInstr] Identity))
instance interrupt (StateT BCState (WriterT [BCInstr] Identity))
instance rpeat     (StateT BCState (WriterT [BCInstr] Identity))
//TODO instance lcd      (StateT BCState (WriterT [BCInstr] Identity))
instance rtrn      (StateT BCState (WriterT [BCInstr] Identity))
instance sds       (StateT BCState (WriterT [BCInstr] Identity))
instance liftsds   (StateT BCState (WriterT [BCInstr] Identity))
instance step      (StateT BCState (WriterT [BCInstr] Identity))
instance tupl      (StateT BCState (WriterT [BCInstr] Identity))
instance unstable  (StateT BCState (WriterT [BCInstr] Identity))
instance .&&.      (StateT BCState (WriterT [BCInstr] Identity))
instance .||.      (StateT BCState (WriterT [BCInstr] Identity))
instance fun ()
                   (StateT BCState (WriterT [BCInstr] Identity))
instance fun (StateT BCState (WriterT [BCInstr] Identity) a)
                   (StateT BCState (WriterT [BCInstr] Identity)) | type a
instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b)
                   (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b
instance fun (StateT BCState (WriterT [BCInstr] Identity) a, StateT BCState (WriterT [BCInstr] Identity) b, StateT BCState (WriterT [BCInstr] Identity) c)
                   (StateT BCState (WriterT [BCInstr] Identity)) | type a & type b & type c

instance int  (StateT BCState (WriterT [BCInstr] Identity)) Int
instance int  (StateT BCState (WriterT [BCInstr] Identity)) Real
instance int  (StateT BCState (WriterT [BCInstr] Identity)) Long
instance real (StateT BCState (WriterT [BCInstr] Identity)) Int
instance real (StateT BCState (WriterT [BCInstr] Identity)) Real
instance real (StateT BCState (WriterT [BCInstr] Identity)) Long
instance long (StateT BCState (WriterT [BCInstr] Identity)) Int
instance long (StateT BCState (WriterT [BCInstr] Identity)) Real
instance long (StateT BCState (WriterT [BCInstr] Identity)) Long
