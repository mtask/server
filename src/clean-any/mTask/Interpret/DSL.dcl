definition module mTask.Interpret.DSL

/**
 * Bytecode compiler instances of the mTask DSL
 */

from Data.Map import :: Map
from Gast import class Gast, generic ggen, generic genShow, generic gPrint, :: GenState, class PrintOutput, :: PrintState
from StdOverloaded import class ^(^), class -(-)
from StdOverloaded import class zero
import iTasks.WF.Derives
from iTasks.SDS.Definition import :: SDSLens

import Data.UInt
import mTask.Interpret.Instructions
import mTask.Interpret.Monad
import mTask.Interpret.Peripheral
import mTask.Language

/**
 * Helper type representing a NULL pointer in the mTask RTS
 *
 * @type UInt16
 */
MT_NULL :== UInt16 ((2^16) - 1)
/**
 * Helper type representing a task-is-removed state in the mTask RTS
 *
 * @type UInt16
 */
MT_REMOVE   :== UInt8  ((2^8)  - 1)

/**
 * Tweak the timing interval 
 *
 * @param TODO: ???
 * @param timinng interval
 * @result a {{Interpret}}
 */
setRate :: Bool (TimingInterval Interpret) -> Interpret a

//** State of the compiler
:: BCState =
	{ bcs_infun        :: JumpLabel
	//** Current function the compiler is in
	, bcs_mainexpr     :: [BCInstr]
	//** Main expression
	, bcs_context      :: [BCInstr]
	//** Context (i.e. the lambda lifted arguments that are in scope)
	, bcs_functions    :: Map JumpLabel BCFunction
	//** Functions
	, bcs_freshlabel   :: JumpLabel
	//** Next fresh jump label
	, bcs_sdses        :: Map UInt8 (Either String255 MTLens)
	//** Map of SDSs
	, bcs_freshsds     :: UInt8
	//** Next fresh sds label
	, bcs_peripherals  :: [BCPeripheralInfo]
	//** Peripherals
	}
//** Synonym type for a lifted SDS that can be written to by mTask
:: MTLens :== SDSLens () String255 String255
//** An mTask function
:: BCFunction =
	{ bcf_instructions :: [BCInstr]
	//** the instructions
	, bcf_argwidth     :: UInt8
	//** The width of the argument in bytes
	, bcf_returnwidth  :: UInt8
	//** The width of the return value in bytes
	}

instance zero BCState

/**
 * Compile a {{Main}} program
 *
 * @param {{Main}} program
 * @param state
 * @result new state
 */
mainBC :: (Main (Interpret v)) BCState -> MaybeError String BCState

instance aio       Interpret
instance expr      Interpret
instance delay     Interpret
instance dio p     Interpret
instance pinMode   Interpret
instance interrupt Interpret
instance rpeat     Interpret
//TODO instance lcd      Interpret
instance rtrn      Interpret
instance sds       Interpret
instance lowerSds  Interpret
instance step      Interpret
instance tupl      Interpret
instance unstable  Interpret
instance .&&.      Interpret
instance .||.      Interpret
instance reflect   Interpret
instance fun ()
                   Interpret
instance fun (Interpret a)
                   Interpret | type a
instance fun (Interpret a, Interpret b)
                   Interpret | type a & type b
instance fun (Interpret a, Interpret b, Interpret c)
                   Interpret | type a & type b & type c

instance int  Interpret Int
instance int  Interpret Real
instance int  Interpret Long
instance real Interpret Int
instance real Interpret Real
instance real Interpret Long
instance long Interpret Int
instance long Interpret Real
instance long Interpret Long
