definition module mTask.Interpret.Specification

/**
 * Device specification
 */

from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..), :: Task
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.Either import :: Either
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from StdClass import class zero

from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError
from mTask.Interpret.DSL import :: UInt8, :: UInt16

derive class iTask MTDeviceSpec
instance zero MTDeviceSpec

//* Device specification
:: MTDeviceSpec =
	{ memory  :: UInt16
	//* Size of the mTask memory in bytes
	, aPins   :: UInt8
	//* Number of analogue pins
	, dPins   :: UInt8
	//* Number of digital pins
	, haveDHT :: Bool
	//* Contains a DHT sensor
	, haveLM  :: Bool
	//* Contains a led matrix
	, haveI2B :: Bool
	//* Contains I²C buttons
	, haveLS  :: Bool
	//* Contains a light sensor
	, haveAQS :: Bool
	//* Contains an air quality sensor
	, haveGes :: Bool
	//* Contains a gesture sensor
	}

derive gCSerialise MTDeviceSpec
derive gCDeserialise MTDeviceSpec
