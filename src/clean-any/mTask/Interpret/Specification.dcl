definition module mTask.Interpret.Specification

/**
 * Device specification
 */

from Data.Either import :: Either
from Data.GenDefault import generic gDefault
from Data.GenEq import generic gEq
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: CDeserialiseError, :: CDeserialiser, :: StateT
from StdClass import class zero
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.WF.Definition import class iTask, :: Stability, :: TaskValue(..), :: Task

from mTask.Interpret.DSL import :: UInt8, :: UInt16
from mTask.SemVer import :: SemVer

derive class iTask MTDeviceSpec
instance zero MTDeviceSpec

//* Device specification
:: MTDeviceSpec =
	{ serverVersion :: SemVer
	//* mTask-server version used by the device
	, clientVersion :: SemVer
	//* mTask-client version used by the device
	, memory  :: UInt16
	//* Size of the mTask memory in bytes
	, aPins   :: UInt8
	//* Number of analogue pins
	, dPins   :: UInt8
	//* Number of digital pins
	, haveDHT :: Bool
	//* Contains a DHT sensor
	, haveLM  :: Bool
	//* Contains a led matrix
	, haveI2B :: Bool
	//* Contains I²C buttons
	, haveLS  :: Bool
	//* Contains a light sensor
	, haveAQS :: Bool
	//* Contains an air quality sensor
	, haveGes :: Bool
	//* Contains a gesture sensor
	}

derive gCSerialise MTDeviceSpec
derive gCDeserialise MTDeviceSpec
