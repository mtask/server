definition module mTask.Language

/**
 * The mTask language classes
 */

import Data.UInt

import mTask.Language.Expressions
import mTask.Language.Types
import mTask.Language.MTask
import mTask.Language.Tasks
import mTask.Language.Long
import mTask.Language.Shares
import mTask.Language.Interrupts

// Peripherals
import mTask.Language.pinIO
import mTask.Language.LEDMatrix
import mTask.Language.i2cbutton
import mTask.Language.DHT
import mTask.Language.LCD
import mTask.Language.Light
import mTask.Language.AirQuality
import mTask.Language.Gesture
import mTask.Language.PIR
import mTask.Language.Sound
