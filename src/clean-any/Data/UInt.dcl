definition module Data.UInt

/**
 * Bounded integers, signed and unsigned
 *
 * Note that the ints do not overflow but are capped
 */

from Data.GenHash import generic gHash
from Data.GenDefault import generic gDefault
from iTasks.UI.Editor.Generic import generic gEditor, :: Editor, :: EditorPurpose, :: EditorReport
from iTasks.Internal.Generic.Visualization import generic gText, :: TextFormat
from Text.GenJSON import generic JSONDecode, generic JSONEncode, :: JSONNode
from Data.GenEq import generic gEq
from StdOverloaded import class zero, class one, class -, class +, class *, class /, class rem, class <, class toInt, class ~, class fromInt, class ^
from Data.GenType import generic gType, :: Box, :: GType
from Data.GenType.CSerialise import generic gCSerialise, generic gCDeserialise, :: Either, :: CDeserialiseError

//** Unsigned 32-bit integer
:: UInt32 =: UInt32 Int
/**
 * The maximal value for an unsigned 32-bit integer
 * @type UInt32
 */
UINT32_MAX :== UInt32 0xffffffff
/**
 * The minimal value for an unsigned 32-bit integer
 * @type UInt32
 */
UINT32_MIN :== UInt32 0x00000000

//** Unsigned 16-bit integer
:: UInt16 =: UInt16 Int
/**
 * The maximal value for an unsigned 16-bit integer
 * @type UInt16
 */
UINT16_MAX :== UInt16 0xffff
/**
 * The minimal value for an unsigned 16-bit integer
 * @type UInt16
 */
UINT16_MIN :== UInt16 0x0000

//** Unsigned 8-bit integer
:: UInt8 =: UInt8 Int
/**
 * The minimal value for an unsigned 8-bit integer
 * @type UInt8
 */
UINT8_MAX :== UInt8 0xff
/**
 * The minimal value for an unsigned 8-bit integer
 * @type UInt8
 */
UINT8_MIN :== UInt8 0x00

//** Signed 32-bit integer
:: Int32 =: Int32 Int
/**
 * The maximal value for a signed 32-bit integer
 * @type Int32
 */
INT32_MAX :== Int32  0x7fffffff
/**
 * The minimal value for a signed 32-bit integer
 * @type Int32
 */
INT32_MIN :== Int32 -0x80000000

//** Signed 16-bit integer
:: Int16 =: Int16 Int
/**
 * The maximal value for a signed 16-bit integer
 * @type Int16
 */
INT16_MAX :== Int16  0x7fff
/**
 * The minimal value for a signed 16-bit integer
 * @type Int16
 */
INT16_MIN :== Int16 -0x8000

//** Signed 8-bit integer
:: Int8 =: Int8 Int
/**
 * The maximal value for a signed 8-bit integer
 * @type Int8
 */
INT8_MAX :== Int8  0x7f
/**
 * The minimal value for a signed 8-bit integer
 * @type Int8
 */
INT8_MIN :== Int8 -0x80

instance ~ UInt8, UInt16, UInt32, Int8, Int16, Int32
instance < UInt8, UInt16, UInt32, Int8, Int16, Int32
instance + UInt8, UInt16, UInt32, Int8, Int16, Int32
instance - UInt8, UInt16, UInt32, Int8, Int16, Int32
instance * UInt8, UInt16, UInt32, Int8, Int16, Int32
instance / UInt8, UInt16, UInt32, Int8, Int16, Int32
instance ^ UInt8, UInt16, UInt32, Int8, Int16, Int32
instance rem UInt8, UInt16, UInt32, Int8, Int16, Int32
instance == UInt8, UInt16, UInt32, Int8, Int16, Int32
instance one UInt8, UInt16, UInt32, Int8, Int16, Int32
instance zero UInt8, UInt16, UInt32, Int8, Int16, Int32
instance toInt UInt8, UInt16, UInt32, Int8, Int16, Int32
instance fromInt UInt8, UInt16, UInt32, Int8, Int16, Int32
instance toString UInt8, UInt16, UInt32, Int8, Int16, Int32

derive gEq UInt8, UInt16, UInt32, Int8, Int16, Int32
derive JSONEncode UInt8, UInt16, UInt32, Int8, Int16, Int32
derive JSONDecode UInt8, UInt16, UInt32, Int8, Int16, Int32

//iTask
derive gText UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gEditor UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gHash UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gCSerialise UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gCDeserialise UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gDefault UInt8, UInt16, UInt32, Int8, Int16, Int32

//gType
derive gType UInt8, UInt16, UInt32, Int8, Int16, Int32

//* Helper function for C-code generation of unsigned 8-bit integers (see {{gCSerialise}})
uint8gType :: (String, [String], String -> [String], String -> [String])
//* Helper function for C-code generation of unsigned 16-bit integers (see {{gCSerialise}})
uint16gType :: (String, [String], String -> [String], String -> [String])
//* Helper function for C-code generation of unsigned 32-bit integers (see {{gCSerialise}})
uint32gType :: (String, [String], String -> [String], String -> [String])
//* Helper function for C-code generation of signed 8-bit integers (see {{gCSerialise}})
int8gType :: (String, [String], String -> [String], String -> [String])
//* Helper function for C-code generation of signed 16-bit integers (see {{gCSerialise}})
int16gType :: (String, [String], String -> [String], String -> [String])
//* Helper function for C-code generation of signed 32-bit integers (see {{gCSerialise}})
int32gType :: (String, [String], String -> [String], String -> [String])
