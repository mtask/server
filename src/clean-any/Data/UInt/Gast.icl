implementation module Data.UInt.Gast

import Control.GenBimap
import Gast
import Gast.Gen
import StdEnv

import Data.UInt

derive gPrint UInt8, UInt16, UInt32, Int8, Int16, Int32
derive genShow UInt8, UInt16, UInt32, Int8, Int16, Int32

genI min max = [!min,max:interleave [!one..max - one] [!min+one..zero - one]]
ggen{|UInt8|} s = genI UINT8_MIN UINT8_MAX
ggen{|UInt16|} s = genI UINT16_MIN UINT16_MAX
ggen{|UInt32|} s = genI UINT32_MIN UINT32_MAX
ggen{|Int8|} s = genI INT8_MIN INT8_MAX
ggen{|Int16|} s = genI INT16_MIN INT16_MAX
ggen{|Int32|} s = genI INT32_MIN INT32_MAX
