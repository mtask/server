definition module Data.UInt.Gast

import Data.UInt
from Gast import class Gast, generic gPrint, generic genShow, generic ggen, class PrintOutput, :: PrintState, :: GenState

derive gPrint UInt8, UInt16, UInt32, Int8, Int16, Int32
derive genShow UInt8, UInt16, UInt32, Int8, Int16, Int32
derive ggen UInt8, UInt16, UInt32, Int8, Int16, Int32
