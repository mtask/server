implementation module Data.UInt

import Control.GenBimap
import Data.Func
import Data.Functor
import Data.GenEq
import Data.GenCons
import StdEnv
import Text.GenJSON
import iTasks

import Data.GenType
import Data.GenType.CSerialise

instance ~ UInt8 where ~ (UInt8 a) = norm (UInt8 a)
instance ~ Int8 where ~ (Int8 a) = norm (Int8 (~ a))
instance ~ UInt16 where ~ (UInt16 a) = norm (UInt16 a)
instance ~ Int16 where ~ (Int16 a) = norm (Int16 (~ a))
instance ~ UInt32 where ~ (UInt32 a) = norm (UInt32 a)
instance ~ Int32 where ~ (Int32 a) = norm (Int32 (~ a))
instance < UInt8 where (<) a b = on (<) toInt a b
instance < Int8 where (<) a b = on (<) toInt a b
instance < UInt16 where (<) a b = on (<) toInt a b
instance < Int16 where (<) a b = on (<) toInt a b
instance < UInt32 where (<) a b = on (<) toInt a b
instance < Int32 where (<) a b = on (<) toInt a b
instance + UInt8 where (+) a b = norm (UInt8 (on (+) toInt a b))
instance + Int8 where (+) a b = norm (Int8 (on (+) toInt a b))
instance + UInt16 where (+) a b = norm (UInt16 (on (+) toInt a b))
instance + Int16 where (+) a b = norm (Int16 (on (+) toInt a b))
instance + UInt32 where (+) a b = norm (UInt32 (on (+) toInt a b))
instance + Int32 where (+) a b = norm (Int32 (on (+) toInt a b))
instance - UInt8 where (-) a b = norm (UInt8 (on (-) toInt a b))
instance - Int8 where (-) a b = norm (Int8 (on (-) toInt a b))
instance - UInt16 where (-) a b = norm (UInt16 (on (-) toInt a b))
instance - Int16 where (-) a b = norm (Int16 (on (-) toInt a b))
instance - UInt32 where (-) a b = norm (UInt32 (on (-) toInt a b))
instance - Int32 where (-) a b = norm (Int32 (on (-) toInt a b))
instance * UInt8 where (*) a b = norm (UInt8 (on (*) toInt a b))
instance * Int8 where (*) a b = norm (Int8 (on (*) toInt a b))
instance * UInt16 where (*) a b = norm (UInt16 (on (*) toInt a b))
instance * Int16 where (*) a b = norm (Int16 (on (*) toInt a b))
instance * UInt32 where (*) a b = norm (UInt32 (on (*) toInt a b))
instance * Int32 where (*) a b = norm (Int32 (on (*) toInt a b))
instance / UInt8 where (/) a b = norm (UInt8 (on (/) toInt a b))
instance / Int8 where (/) a b = norm (Int8 (on (/) toInt a b))
instance / UInt16 where (/) a b = norm (UInt16 (on (/) toInt a b))
instance / Int16 where (/) a b = norm (Int16 (on (/) toInt a b))
instance / UInt32 where (/) a b = norm (UInt32 (on (/) toInt a b))
instance / Int32 where (/) a b = norm (Int32 (on (/) toInt a b))
instance ^ UInt8 where (^) a b = norm (UInt8 (on (^) toInt a b))
instance ^ Int8 where (^) a b = norm (Int8 (on (^) toInt a b))
instance ^ UInt16 where (^) a b = norm (UInt16 (on (^) toInt a b))
instance ^ Int16 where (^) a b = norm (Int16 (on (^) toInt a b))
instance ^ UInt32 where (^) a b = norm (UInt32 (on (^) toInt a b))
instance ^ Int32 where (^) a b = norm (Int32 (on (^) toInt a b))
instance rem UInt8 where (rem) a b = norm (UInt8 (on (rem) toInt a b))
instance rem Int8 where (rem) a b = norm (Int8 (on (rem) toInt a b))
instance rem UInt16 where (rem) a b = norm (UInt16 (on (rem) toInt a b))
instance rem Int16 where (rem) a b = norm (Int16 (on (rem) toInt a b))
instance rem UInt32 where (rem) a b = norm (UInt32 (on (rem) toInt a b))
instance rem Int32 where (rem) a b = norm (Int32 (on (rem) toInt a b))
instance == UInt8 where (==) a b = on (==) toInt a b
instance == Int8 where (==) a b = on (==) toInt a b
instance == UInt16 where (==) a b = on (==) toInt a b
instance == Int16 where (==) a b = on (==) toInt a b
instance == UInt32 where (==) a b = on (==) toInt a b
instance == Int32 where (==) a b = on (==) toInt a b
instance one UInt8 where one = UInt8 one
instance one Int8 where one = Int8 one
instance one UInt16 where one = UInt16 one
instance one Int16 where one = Int16 one
instance one UInt32 where one = UInt32 one
instance one Int32 where one = Int32 one
instance zero UInt8 where zero = UInt8 zero
instance zero Int8 where zero = Int8 zero
instance zero UInt16 where zero = UInt16 zero
instance zero Int16 where zero = Int16 zero
instance zero UInt32 where zero = UInt32 zero
instance zero Int32 where zero = Int32 zero
instance toInt UInt8 where toInt (UInt8 i) = i
instance toInt Int8 where toInt (Int8 i) = i
instance toInt UInt16 where toInt (UInt16 i) = i
instance toInt Int16 where toInt (Int16 i) = i
instance toInt UInt32 where toInt (UInt32 i) = i
instance toInt Int32 where toInt (Int32 i) = i
instance fromInt UInt8 where fromInt i = norm (UInt8 i)
instance fromInt Int8 where fromInt i = norm (Int8 i)
instance fromInt UInt16 where fromInt i = norm (UInt16 i)
instance fromInt Int16 where fromInt i = norm (Int16 i)
instance fromInt UInt32 where fromInt i = norm (UInt32 i)
instance fromInt Int32 where fromInt i = norm (Int32 i)
instance toString UInt8 where toString (UInt8 i) = toString i
instance toString Int8 where toString (Int8 i) = toString i
instance toString UInt16 where toString (UInt16 i) = toString i
instance toString Int16 where toString (Int16 i) = toString i
instance toString UInt32 where toString (UInt32 i) = toString i
instance toString Int32 where toString (Int32 i) = toString i

class norm a :: a -> a
nmm mn mx :== min mx o max mn

instance norm UInt8 where
	norm i = nmm UINT8_MIN UINT8_MAX i
instance norm UInt16 where
	norm i = nmm UINT16_MIN UINT16_MAX i
instance norm Int8 where
	norm i = nmm INT8_MIN INT8_MAX i
instance norm Int16 where
	norm i = nmm INT16_MIN INT16_MAX i
instance norm Int32 where
	norm i = IF_INT_64_OR_32 (nmm INT32_MIN INT32_MAX i) i
instance norm UInt32 where
	norm i = IF_INT_64_OR_32 (nmm UINT32_MIN UINT32_MAX i) i

derive gEq UInt8, UInt16, UInt32, Int8, Int16, Int32
derive JSONEncode UInt8, UInt16, UInt32, Int8, Int16, Int32
derive JSONDecode UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gText UInt8, UInt16, UInt32, Int8, Int16, Int32
gEditor{|UInt8|} ep = boundedIntEditor UINT8_MIN UINT8_MAX ep "Enter an 8-bit unsigned integer"
gEditor{|UInt16|} ep = boundedIntEditor UINT16_MIN UINT16_MAX ep "Enter an 8-bit unsigned integer"
gEditor{|UInt32|} ep = boundedIntEditor UINT32_MIN UINT32_MAX ep "Enter an 8-bit unsigned integer"
gEditor{|Int8|} ep = boundedIntEditor INT8_MIN INT8_MAX ep "Enter an 8-bit integer"
gEditor{|Int16|} ep = boundedIntEditor INT16_MIN INT16_MAX ep "Enter a 16-bit integer"
gEditor{|Int32|} ep = boundedIntEditor INT32_MIN INT32_MAX ep "Enter a 32-bit integer"

boundedIntEditor :: a a EditorPurpose String -> Editor a (EditorReport a) | fromInt, toInt, toString a
boundedIntEditor min max ViewValue hint
	= mapEditorWrite (const EmptyEditor)
	$ mapEditorRead toString textView
boundedIntEditor min max EditValue hint
	= mapEditorWrite (fmap fromInt)
	$ mapEditorRead toInt
	$ withDynamicHintAttributes hint
	$ integerField <<@ maxAttr (toInt max) <<@ minAttr (toInt min)

derive gHash UInt8, UInt16, UInt32, Int8, Int16, Int32

gCSerialise{|Int8|} i = (++) $ serialiseInt 1 True i
gCSerialise{|UInt8|} i = (++) $ serialiseInt 1 False i
gCSerialise{|Int16|} i = (++) $ serialiseInt 2 True i
gCSerialise{|UInt16|} i = (++) $ serialiseInt 2 False i
gCSerialise{|Int32|} i = (++) $ serialiseInt 4 True i
gCSerialise{|UInt32|} i = (++) $ serialiseInt 4 False i
gCDeserialise{|Int8|} top = deserialiseInt 1 True top
gCDeserialise{|UInt8|} top = deserialiseInt 1 False top
gCDeserialise{|Int16|} top = deserialiseInt 2 True top
gCDeserialise{|UInt16|} top = deserialiseInt 2 False top
gCDeserialise{|Int32|} top = deserialiseInt 4 True top
gCDeserialise{|UInt32|} top = deserialiseInt 4 False top

derive gDefault UInt8, UInt16, UInt32, Int8, Int16, Int32
derive gType UInt8, UInt16, UInt32, Int8, Int16, Int32
uint8gType :: (String, [String], String -> [String], String -> [String])
uint8gType =
	( gTypeName (gTypeForValue (UInt8 0))
	, ["typedef uint8_t UInt8;\n"]
	, \r->[r +++ " = get();\n"]
	, \r->["put(" +++ r +++ ");\n"])

uint16gType :: (String, [String], String -> [String], String -> [String])
uint16gType =
	( gTypeName (gTypeForValue (UInt16 0))
	, ["typedef uint16_t UInt16;\n"]
	, \r->[r +++ " = (uint16_t)get()<<8;\n", r +++ " += (uint16_t)get();\n"]
	, \r->["put(" +++ r +++ ">>8);\n", "put(" +++ r +++ " & 0xff);\n"])

uint32gType :: (String, [String], String -> [String], String -> [String])
uint32gType =
	( gTypeName (gTypeForValue (UInt32 0))
	, ["typedef uint32_t UInt32;\n"]
	, \r->
		[ r +++ " = (uint32_t)get()<<24;\n"
		, r +++ " = (uint32_t)get()<<16;\n"
		, r +++ " = (uint32_t)get()<<8;\n"
		, r +++ " += (uint32_t)get();\n"]
	, \r->
		[ "put(" +++ r +++ ">>24);\n"
		, "put(" +++ r +++ ">>16);\n"
		, "put(" +++ r +++ ">>8);\n"
		, "put(" +++ r +++ " & 0xff);\n"])

int8gType :: (String, [String], String -> [String], String -> [String])
int8gType =
	( gTypeName (gTypeForValue (Int8 0))
	, ["typedef int8_t Int8;\n"]
	, \r->[r +++ " = (int8_t)get();\n"]
	, \r->["put(" +++ r +++ ");\n"])
	
int16gType :: (String, [String], String -> [String], String -> [String])
int16gType =
	( gTypeName (gTypeForValue (Int16 0))
	, ["typedef int16_t Int16;\n"]
	, \r->[r +++ " = (int16_t)get()<<8;\n", r +++ " += (int16_t)get();\n"]
	, \r->["put(" +++ r +++ ">>8);\n", "put(" +++ r +++ " & 0xff);\n"])

int32gType :: (String, [String], String -> [String], String -> [String])
int32gType =
	( gTypeName (gTypeForValue (Int32 0))
	, ["typedef int32_t Int32;\n"]
	, \r->
		[ r +++ " = (int32_t)get()<<24;\n"
		, r +++ " = (int32_t)get()<<16;\n"
		, r +++ " = (int32_t)get()<<8;\n"
		, r +++ " += (int32_t)get();\n"]
	, \r->
		[ "put(" +++ r +++ ">>24);\n"
		, "put(" +++ r +++ ">>16);\n"
		, "put(" +++ r +++ ">>8);\n"
		, "put(" +++ r +++ " & 0xff);\n"])
