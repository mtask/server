#include <stdint.h>

/**
 * Union for interchanging floats and 32-bit integers without translation
 * This requires that floats are exactly 32-bit of size
 */
union floatint {
	/** float representation */
	float f;
	/** unsigned 32-bit int representation */
	uint32_t i;
};

//** Pack a real into a 32-bit integer as a float
uint32_t convert_real_to_float_in_int_32(double r)
{
	union floatint fi = { .f=(float) r };
	return fi.i;
}

//** Pack a real into a 64-bit integer as a float
uint64_t convert_real_to_float_in_int_64(double r)
{
	union floatint fi = { .f=(float) r };
	return (uint64_t) fi.i;
}

//** Unpack a float packed into a 32-bit integer as a real
double convert_float_in_int_to_real_32(uint32_t r)
{
	union floatint fi = { .i=(uint32_t) r };
	return (double) fi.f;
}

//** Unpack a float packed into a 34-bit integer as a real
double convert_float_in_int_to_real_64(uint64_t r)
{
	union floatint fi = { .i=(uint32_t) r };
	return (double) fi.f;
}
