# The mTask server library

For general mTask information please refer to the main repository:
https://gitlab.science.ru.nl/mtask/mtask

Automatically built packages can be found here:
[windows 64-bit](https://gitlab.science.ru.nl/mtask/server/builds/artifacts/master/raw/mtask-server-windows-x64.zip?job=windows-x64),
[linux 64-bit](https://gitlab.science.ru.nl/mtask/server/builds/artifacts/master/raw/mtask-server-linux-x64.tar.gz?job=linux-x64)

[[_TOC_]]

## How to compile

To compile the dependencies for linux, run `linux-compile.sh`.

To cross compile for windows on linux, run `windows-compile.sh`.

To create a package, run `package.sh`
