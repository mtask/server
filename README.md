# The mTask server library

This repository contains the server library.

For general mTask information please refer to the main repository:
https://gitlab.com/mtask/mtask

## Windows support

The little C code that is used (see `src/c-any`) is on windows crosscompiled using `mingw`.
See [nitrile.yml](nitrile.yml) for more details.

## Code style

Please try to adhere to the codestyle used elsewhere.
Most importantly, order imports (alphabetically, local imports separated).

## Maintainer

Mart Lubbers (mart@cs.ru.nl)

## License

`mtask-server` is licensed under the BSD 2-Clause "Simplified" License (see [LICENSE](LICENSE)).
