# Changelog

### 0.6

- Use VoidPointer from gentype
- Change `fail` from the bytecode decoder to `failBC` to not have a conflict
  with `MonadFail`.
- Adapt to base 3 and iTask 0.16

### 0.5

- Add version field to the device specification

#### 0.4.2

- Fix: default settings WEMOS D1 Mini LED shield

#### 0.4.1

- Fix: fix bug in SDS numbering.

### 0.4

- Change: Airquality tasks now returns longs instead of ints.
- Change: Refactor BCInterpret to Interpret and make type instead of type synonym.
- Change: Use `Port` type instead of `Int` in MQTT and TCP settings.
- Chore: Remove many unused bytecodeencoding exports.
- Chore: Support iTask 0.13.
- Feature: Add reflect combinator.

#### 0.3.1

- Add predefined gesture sensor object

#### 0.3.1

- Add some predefined info objects for peripherals

### 0.3

- Clean up peripherals and make uniform and more extendible
- Change: data layout of peripherals
- Add support for the SGP30 air quality sensor
- Add support for NeoPixel peripherals

#### 0.2.1

- Support iTask 0.7 also

### 0.2

- Fix fix storing of sdss
- rename bcs_hardware to bcs_peripherals
- Don't send specification (let the client send it on connect)

#### 0.1.3

- Fix storing of lowered SDSs (see #26)

#### 0.1.2

- Rename `liftsds` to `lowersds` and `pinIO`

#### 0.1.1

- Fix SDS numbering in case of multiple SDSes
- use INPUT_PULLUP for PIRs
- add fun (v Real, v Real) v to mTask class

### 0.1

- Initial nitrile version
